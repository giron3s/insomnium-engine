#include "mcv_platform.h"
#include "render_texture.h"
#include "texture_manager.h"
#include "d3d9.h"

CRenderTexture::CRenderTexture()
    : target_view(NULL)
    , xres(0)
    , yres(0)
    , trace("RT")
    , color_fmt(DXGI_FORMAT_UNKNOWN)
    , depth_fmt(DXGI_FORMAT_UNKNOWN)
{
    view_port.Height = view_port.Width = view_port.TopLeftX = view_port.TopLeftY = 0.0f;
    view_port.MinDepth = 0.0f; view_port.MaxDepth = 1.0f;
}

bool CRenderTexture::isValid() const {
    return mResourceView != NULL && target_view != NULL;
}

void CRenderTexture::destroy() {
    SAFE_RELEASE(target_view);
    SAFE_RELEASE(mResourceView);
    return;
}

// ---------------------------------------------------------------
void CRenderTexture::setResolution(int w, int h) {
    xres = w;
    yres = h;

    // Init viewport
    view_port.Width = static_cast<float>(w);
    view_port.Height = static_cast<float>(h);
    view_port.MinDepth = 0.0f;
    view_port.MaxDepth = 1.0f;
    view_port.TopLeftX = 0;
    view_port.TopLeftY = 0;
}

bool CRenderTexture::create(const char *aname, int axres, int ayres, DXGI_FORMAT acolor_fmt, DXGI_FORMAT adepth_fmt) {

    memset(name, 0x00, sizeof(name));
    strcpy(name, aname);
    trace.setName(name);

    setResolution(axres, ayres);
    color_fmt = acolor_fmt;
    depth_fmt = adepth_fmt;

    // Do we need to create a color buffer. (shadow maps don't)
    if (color_fmt != DXGI_FORMAT_UNKNOWN)
        if (!createColorBuffer(name))
            return false;

    // Do we need to create a depth buffer
    if (depth_fmt != DXGI_FORMAT_UNKNOWN)
        if (!createDepthStencil())
            return false;

    return true;
}

bool CRenderTexture::createColorBuffer(const char *name) {
    ID3D11Device* device = App.d3dDevice;

    D3D11_TEXTURE2D_DESC textureDesc;
    HRESULT result;

    // Initialize the render target texture description.
    ZeroMemory(&textureDesc, sizeof(textureDesc));

    // Setup the render target texture description.
    textureDesc.Width = xres;
    textureDesc.Height = yres;
    textureDesc.MipLevels = 1;
    textureDesc.ArraySize = 1;
    textureDesc.Format = color_fmt; // DXGI_FORMAT_R32G32B32A32_FLOAT;
    textureDesc.SampleDesc.Count = 1;
    textureDesc.Usage = D3D11_USAGE_DEFAULT;
    textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
    textureDesc.CPUAccessFlags = 0;
    textureDesc.MiscFlags = 0;

    // Create the render target texture.
    ID3D11Texture2D* texture;
    result = device->CreateTexture2D(&textureDesc, NULL, &texture);
    if (FAILED(result))
        return false;

    // Setup the description of the render target view.
    D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
    ZeroMemory(&renderTargetViewDesc, sizeof(renderTargetViewDesc));
    renderTargetViewDesc.Format = textureDesc.Format;
    renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
    renderTargetViewDesc.Texture2D.MipSlice = 0;

    // Create the render target view.
    result = device->CreateRenderTargetView(texture, &renderTargetViewDesc, &target_view);
    if (FAILED(result))
        return false;

    // Setup the description of the shader resource view.
    D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
    ZeroMemory(&shaderResourceViewDesc, sizeof(shaderResourceViewDesc));
    shaderResourceViewDesc.Format = textureDesc.Format;
    shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
    shaderResourceViewDesc.Texture2D.MipLevels = 1;

    // Create the shader resource view.
    result = device->CreateShaderResourceView(texture, &shaderResourceViewDesc, &mResourceView);
    if (FAILED(result))
        return false;

    texture_manager.registerNewTexture(this, name);

    SAFE_RELEASE(texture);
    return true;
}

// ---------------------------------------------------------------
bool CRenderTexture::createDepthStencil(bool bind_shader_resource) {
    ID3D11Device* device = App.d3dDevice;
    assert(ztexture.mResourceView == NULL);
    assert(depth_fmt != DXGI_FORMAT_UNKNOWN);
    assert(depth_fmt == DXGI_FORMAT_R32_TYPELESS
        || depth_fmt == DXGI_FORMAT_R24G8_TYPELESS
        || depth_fmt == DXGI_FORMAT_R16_TYPELESS
        || depth_fmt == DXGI_FORMAT_D24_UNORM_S8_UINT
        || depth_fmt == DXGI_FORMAT_R8_TYPELESS);

    D3D11_TEXTURE2D_DESC          depthBufferDesc;

    // Init depth and stencil buffer
    // Initialize the description of the depth buffer.
    ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

    // Set up the description of the depth buffer.
    depthBufferDesc.Width = xres;
    depthBufferDesc.Height = yres;
    depthBufferDesc.MipLevels = 1;
    depthBufferDesc.ArraySize = 1;
    depthBufferDesc.Format = depth_fmt;
    depthBufferDesc.SampleDesc.Count = 1;
    depthBufferDesc.SampleDesc.Quality = 0;
    depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    depthBufferDesc.CPUAccessFlags = 0;
    depthBufferDesc.MiscFlags = 0;

    //  if (bind_shader_resource)
    depthBufferDesc.BindFlags |= D3D11_BIND_SHADER_RESOURCE;

    DXGI_FORMAT texturefmt = DXGI_FORMAT_R32_TYPELESS;
    DXGI_FORMAT SRVfmt = DXGI_FORMAT_R32_FLOAT;
    DXGI_FORMAT DSVfmt = DXGI_FORMAT_D32_FLOAT;

    switch (depth_fmt) {
    case DXGI_FORMAT_R32_TYPELESS:
        SRVfmt = DXGI_FORMAT_R32_FLOAT;
        DSVfmt = DXGI_FORMAT_D32_FLOAT;
        break;
    case DXGI_FORMAT_R24G8_TYPELESS:
        SRVfmt = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
        DSVfmt = DXGI_FORMAT_D24_UNORM_S8_UINT;
        break;
    case DXGI_FORMAT_R16_TYPELESS:
        SRVfmt = DXGI_FORMAT_R16_UNORM;
        DSVfmt = DXGI_FORMAT_D16_UNORM;
        break;
    case DXGI_FORMAT_R8_TYPELESS:
        SRVfmt = DXGI_FORMAT_R8_UNORM;
        DSVfmt = DXGI_FORMAT_R8_UNORM;
        break;
    case DXGI_FORMAT_D24_UNORM_S8_UINT:
        SRVfmt = depthBufferDesc.Format;
        DSVfmt = depthBufferDesc.Format;
        break;
    }

    // Create the texture for the de  pth buffer using the filled out description.
    ID3D11Texture2D* ztexture2d;
    HRESULT hr = device->CreateTexture2D(&depthBufferDesc, NULL, &ztexture2d);
    if (FAILED(hr))
        return false;
    setDbgName(ztexture2d, "ZTexture2D");

    // Initialize the depth stencil view.
    D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
    ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

    // Set up the depth stencil view description.
    depthStencilViewDesc.Format = DSVfmt;
    depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    depthStencilViewDesc.Texture2D.MipSlice = 0;

    // Create the depth stencil view.
    hr = device->CreateDepthStencilView(ztexture2d, &depthStencilViewDesc, &depth_stencil_view);
    if (FAILED(hr))
        return false;
    setDbgName(depth_stencil_view, "ZTextureDSV");

    // Setup the description of the shader resource view.
    D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
    shaderResourceViewDesc.Format = SRVfmt;
    shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
    shaderResourceViewDesc.Texture2D.MipLevels = depthBufferDesc.MipLevels;

    // Create the shader resource view.
    hr = device->CreateShaderResourceView(ztexture2d, &shaderResourceViewDesc, &ztexture.mResourceView);
    if (FAILED(hr))
        return false;
    setDbgName(ztexture.mResourceView, "ZTextureRV");

    SAFE_RELEASE(ztexture2d);

    return true;
}

void CRenderTexture::clear(XMFLOAT4 color) {
    assert(target_view);
    App.immediateContext->ClearRenderTargetView(target_view, &color.x);
}

ID3D11DepthStencilView* CRenderTexture::getDepthStencilView() {
    if (depth_stencil_view)
        return depth_stencil_view;
    return App.depthStencilView;
}

void CRenderTexture::clearZBuffer() {
    App.immediateContext->ClearDepthStencilView(getDepthStencilView(), D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void CRenderTexture::begin() {
    trace.begin();
    // Bind the render target view and depth stencil buffer to the output render pipeline.
    ID3D11DeviceContext* devcon = App.immediateContext;
    if (target_view)
        devcon->OMSetRenderTargets(1, &target_view, getDepthStencilView());
    else
        devcon->OMSetRenderTargets(0, NULL, getDepthStencilView());
    activateViewport();
}

void CRenderTexture::beginWithZBuffer(ID3D11DepthStencilView *depth_view) {
    trace.begin();
    // Bind the render target view and depth stencil buffer to the output render pipeline.
    ID3D11DeviceContext* devcon = App.immediateContext;
    assert(target_view);
    devcon->OMSetRenderTargets(1, &target_view, depth_view);
    activateViewport();
}


void CRenderTexture::end() {

    // Activar de nuevo un viewport a full screen de la app
    D3D11_VIEWPORT app_viewport;
    app_viewport.TopLeftX = app_viewport.TopLeftY = 0.0f;
    app_viewport.MinDepth = 0.0f; app_viewport.MaxDepth = 1.0f;
    app_viewport.Width = App.xres;
    app_viewport.Height = App.yres;
    ID3D11DeviceContext* devcon = App.immediateContext;
    devcon->RSSetViewports(1, &app_viewport);

    App.immediateContext->OMSetRenderTargets(1, &App.renderTargetView, App.depthStencilView);
    trace.end();
}

void CRenderTexture::activateViewport() {
    assert(view_port.Width > 0);
    ID3D11DeviceContext* devcon = App.immediateContext;
    devcon->RSSetViewports(1, &view_port);
}
