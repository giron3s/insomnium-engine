#include "mcv_platform.h"
#include "debug_manager.h"
#include "input_manager.h"
#include "render_manager.h"
#include "render_constants.h"

namespace debug_manager
{
    //Vars
    bool        mDebug;                 //Is in debug or not
    bool        mDebugWireFrame;        //Wireframe render
    bool        mDebugAABB;             //AABB render
    bool        mDebugGrid;             //Grid render
    bool        mDebugAxis;             //AXis render of the objects

    int         mDebugFrameID;          //Render frame ID
    bool        mDebugDeferred;         //Render the deferred render textures

    bool        mDebugProfilerInfo;
    string      mDebugProfilerInfoStr;  //Shiny profiler info

    //-----------------------------------------------------------
    // Init
    //-----------------------------------------------------------
    void init()
    {
        mDebug                = true;        //Is in debug or not
        mDebugWireFrame       = false;        //Wireframe render
        mDebugAABB            = false;        //AABB render
        mDebugGrid            = false;        //Grid render
        mDebugAxis            = false;        //AXis render of the objects
        mDebugDeferred        = false;        //Render the deferred render textures
        mDebugFrameID         = 0;
                              
        mDebugProfilerInfo    = false;
        mDebugProfilerInfoStr = "";
    }


    //-----------------------------------------------------------
    // Update
    //-----------------------------------------------------------
    void Update(float delta)
    {
        if (InputManager.becomesPressed(CInputManager::F1_KEY))
        {
            mDebug = !mDebug;
            ShowCursor(mDebug);
        }

        if (InputManager.becomesPressed(CInputManager::F5_KEY))
        {
            if (mDebugFrameID - 1 < 0)
            {
                mDebugFrameID = RT_COUNT - 1;
            }
            else{
                mDebugFrameID -= 1;
            }
        }

        if (InputManager.becomesPressed(CInputManager::F6_KEY))
        {
            mDebugFrameID = (mDebugFrameID + 1) % (RT_COUNT - 1);
        }
    }

    //-----------------------------------------------------------
    // IsDebug
    //-----------------------------------------------------------
    const bool IsDebug()
    {
        return mDebug;
    }

    //-----------------------------------------------------------
    // IsDebugAABB
    //-----------------------------------------------------------
    const bool IsDebugAABB()
    {
        return mDebugAABB;
    }

    //-----------------------------------------------------------
    // IsDebugGrid
    //-----------------------------------------------------------
    const bool IsDebugGrid()
    {
        return mDebugGrid;
    }

    //-----------------------------------------------------------
    // IsDebugWireframe
    //-----------------------------------------------------------
    const bool IsDebugWireFrame()
    {
        return mDebugWireFrame;
    }

    //-----------------------------------------------------------
    // IsDebugDeferred
    //-----------------------------------------------------------
    const bool IsDebugDeferred()
    {
        return mDebugDeferred;
    }

    //-----------------------------------------------------------
    // IsDebugAxis
    //-----------------------------------------------------------
    const bool IsDebugAxis()
    {
        return mDebugAxis;
    }
    
    //-----------------------------------------------------------
    // GetDebugFrameID
    //-----------------------------------------------------------
    int GetDebugFrameID()
    {
        return mDebugFrameID;
    }


    //-----------------------------------------------------------
    // IsDebugProfiler
    //-----------------------------------------------------------
    const bool IsDebugProfilerInfo()
    {
        return mDebugProfilerInfo;
    }
    
    
    //-----------------------------------------------------------
    // GetProfilerInfo
    //-----------------------------------------------------------
    string GetProfilerInfo()
    {
        return mDebugProfilerInfoStr;
    }

    //-----------------------------------------------------------
    // SetDebugAABB
    //-----------------------------------------------------------
    void SetDebugAABB(bool aDebugAABB)
    {
        mDebugAABB = aDebugAABB;
    }

    //-----------------------------------------------------------
    // SetDebugGrid
    //-----------------------------------------------------------
    void SetDebugGrid(bool aDebugGrid)
    {
        mDebugGrid = aDebugGrid;
    }

    //-----------------------------------------------------------
    // SetDebugAxis
    //-----------------------------------------------------------
    void SetDebugAxis(bool aDebugAxis)
    {
        mDebugAxis = aDebugAxis;
    }

    //-----------------------------------------------------------
    // SetDebugWireFrame
    //-----------------------------------------------------------
    void SetDebugWireFrame(bool aDebugWireFrame)
    {
        mDebugWireFrame = aDebugWireFrame;
    }

    //-----------------------------------------------------------
    // SetDebugDeferred
    //-----------------------------------------------------------
    void SetDebugDeferred(bool aDebugDeferred)
    {
        mDebugDeferred = aDebugDeferred;
    }

    //-----------------------------------------------------------
    // SetDebugProfilerInfo
    //-----------------------------------------------------------
    void SetDebugProfilerInfo(bool aDebugProfilerInfo)
    {
        mDebugProfilerInfo = aDebugProfilerInfo;
    }

    //-----------------------------------------------------------
    // SetDebugProfilerInfoStr
    //-----------------------------------------------------------
    void SetDebugProfilerInfoStr(string aDebugProfilerInfoStr)
    {
        mDebugProfilerInfoStr = aDebugProfilerInfoStr;
    }
}
