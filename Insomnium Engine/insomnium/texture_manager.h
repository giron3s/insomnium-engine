#ifndef INC_TEXTURE_MANAGER_H_
#define INC_TEXTURE_MANAGER_H_

//--------------------------------------------------------------------------------------
#include "generic_manager.h"
#include "texture.h"

class CTextureManager : public CObjManager<CTexture> {
public:
  void registerNewTexture(CTexture *new_texture, const std::string &new_name);
};
extern CTextureManager texture_manager;

#endif
