#ifndef INC_TECHNIQUE_PARSER_H_
#define INC_TECHNIQUE_PARSER_H_

#include "XMLParser.h"
#include "components/components.h"

class CTechniqueParser : public CXMLParser {
    CRenderTechnique          *curr_technique;
    char*                      curr_technique_name;
    string                     curr_shader_name;
    string                     curr_vs_function;
    string                     curr_ps_function;
    CVertexDeclaration        *curr_vertex_declaration;
    int                        curr_blend_config;
    int                        curr_tech_group;

public:
    CTechniqueParser() : curr_technique(NULL){ }
    void onStartElement(const std::string &elem, MKeyValue &atts);
    void onEndElement(const std::string &elem);

    vector< CRenderTechnique* > techniques_loaded;
};

#endif
