#include "mcv_platform.h"
#include "rigid_animation_manager.h"

CRigidAnimationManager rigid_animation_manager;

bool CCoreRigidAnimation::load( CDataProvider &dp ) {
  
  dp.read( header );
  if( !header.isValid() )
    return false;

  names.resize( header.nobjs );
  for( size_t i=0; i<header.nobjs; ++i ) 
    dp.read( names[ i ] );

  keys.resize( header.nobjs * header.nsamples );
  dp.read( &keys[0], header.nobjs * header.nsamples * sizeof( TKey ) );

  return true;
}

void CCoreRigidAnimation::apply( float curr_time, VEntities &targets, XMMATRIX world ) const {

  assert( curr_time >= 0 && curr_time <= header.total_time );
  
  // [0..1]
  float unit_time = curr_time / header.total_time;
  float frame = unit_time * (header.nsamples - 1);  // 14.3
  unsigned prev_frame = static_cast<int>( frame );     // 14
  float    amount_of_next_frame = frame - prev_frame;  // 0.3
  unsigned next_frame = prev_frame + 1;

  if( next_frame > header.nsamples - 1 ) { // unit_time = 1.0f
    next_frame = header.nsamples - 1;
    amount_of_next_frame = 0.f;
  }
  
  assert( amount_of_next_frame >= 0 && amount_of_next_frame < 1.0f );
  assert( prev_frame >= 0 && prev_frame < header.nsamples );
  assert( next_frame >= 0 && next_frame < header.nsamples );
  assert( targets.size() == names.size() );
  assert( targets.size() == header.nobjs );

  unsigned nkeys_per_frame = header.nobjs;
  float    amount_of_prev_frame = 1.0f - amount_of_next_frame;

  // Prev and next keys
  const TKey *pkey = &keys[ prev_frame * nkeys_per_frame ];
  const TKey *nkey = &keys[ next_frame * nkeys_per_frame ];

  XMVECTOR xm_amount_of_next = XMVectorSet( amount_of_next_frame, amount_of_next_frame, amount_of_next_frame, amount_of_next_frame );
  XMVECTOR xm_amount_of_prev = XMVectorSet( amount_of_prev_frame, amount_of_prev_frame, amount_of_prev_frame, amount_of_prev_frame );

  for( size_t i=0; i<header.nobjs; ++i, ++pkey, ++nkey ) {

    XMVECTOR ptrans = XMVectorSet( pkey->trans.x, pkey->trans.y, pkey->trans.z, pkey->trans.w );
    XMVECTOR ntrans = XMVectorSet( nkey->trans.x, nkey->trans.y, nkey->trans.z, nkey->trans.w );
    XMVECTOR pquat = XMVectorSet( pkey->quat.x, pkey->quat.y, pkey->quat.z, pkey->quat.w );
    XMVECTOR nquat = XMVectorSet( nkey->quat.x, nkey->quat.y, nkey->quat.z, nkey->quat.w );

    XMVECTOR new_pos = (ptrans) * xm_amount_of_prev 
                     + (ntrans) * xm_amount_of_next;
    //XMQuaternionSlerp( 
    XMVECTOR new_quat = (pquat) * xm_amount_of_prev 
                      + (nquat) * xm_amount_of_next;

    if( targets[i] ) {
      CTransform *t = targets[ i ]->get<CTransform>();
      XMMATRIX new_world = XMMatrixRotationQuaternion( new_quat );
      t->getWorld() = new_world;
      t->setPosition( new_pos );
      t->getWorld() = t->getWorld() * world;
    }
  }
}

XMVECTOR CCoreRigidAnimation::getPositionAt(unsigned object_id, float base_time ) const {

  assert(base_time >= 0 && base_time <= header.total_time);

  // [0..1]
  float unit_time = base_time / header.total_time;
  float frame = unit_time * (header.nsamples - 1);  // 14.3
  unsigned prev_frame = static_cast<int>(frame);     // 14
  float    amount_of_next_frame = frame - prev_frame;  // 0.3
  unsigned next_frame = prev_frame + 1;

  if (next_frame > header.nsamples - 1) { // unit_time = 1.0f
    next_frame = header.nsamples - 1;
    amount_of_next_frame = 0.f;
  }

  assert(amount_of_next_frame >= 0 && amount_of_next_frame < 1.0f);
  assert(prev_frame >= 0 && prev_frame < header.nsamples);
  assert(next_frame >= 0 && next_frame < header.nsamples);
  assert(object_id < names.size());

  unsigned nkeys_per_frame = header.nobjs;
  float    amount_of_prev_frame = 1.0f - amount_of_next_frame;

  // Prev and next keys
  const TKey *pkey = &keys[prev_frame * nkeys_per_frame];
  const TKey *nkey = &keys[next_frame * nkeys_per_frame];

  pkey += object_id;
  nkey += object_id;

  XMVECTOR xm_amount_of_next = XMVectorSet(amount_of_next_frame, amount_of_next_frame, amount_of_next_frame, amount_of_next_frame);
  XMVECTOR xm_amount_of_prev = XMVectorSet(amount_of_prev_frame, amount_of_prev_frame, amount_of_prev_frame, amount_of_prev_frame);

  //
  XMVECTOR ptrans = XMVectorSet(pkey->trans.x, pkey->trans.y, pkey->trans.z, pkey->trans.w);
  XMVECTOR ntrans = XMVectorSet(nkey->trans.x, nkey->trans.y, nkey->trans.z, nkey->trans.w);

  XMVECTOR out_trans = (ptrans)* amount_of_prev_frame + (ntrans)* amount_of_next_frame;
  return out_trans;
}

XMVECTOR CCoreRigidAnimation::getTranslation(unsigned object_id, float base_time, float elapsed ) const {
  XMVECTOR trans0 = getPositionAt(object_id, base_time);
  float time_1 = base_time + elapsed;
  if (time_1 > header.total_time)
    time_1 = header.total_time;
  XMVECTOR trans1 = getPositionAt(object_id, time_1);
  XMVECTOR delta_trans = trans1 - trans0;
  return delta_trans;
}


bool initObjFromName( CCoreRigidAnimation &obj, const char *name ) {
  std::string full_name = "data/rigid_anims/" + std::string(name) + ".anim";
  CFileDataProvider fdp( full_name.c_str() );
  bool is_ok = obj.load( fdp );
  assert( is_ok );
  return is_ok;
}


