#ifndef INC_ENTITY_3D_H_
#define INC_ENTITY_3D_H_

#include "angular.h"
class CRenderMesh;

//--------------------------------------------------------------------------------------
class CEntity3D {
public:
  XMMATRIX     world;
  XMVECTOR     pos;
  float        yaw;
  CRenderMesh *mesh;

  void updateWorld();

  float getYaw() const { return yaw; }
  void setYaw( float new_yaw );
  void setWorldMatrix( XMMATRIX new_world );

  XMVECTOR getPosition( ) const { return pos; }
  void setPosition( XMVECTOR new_pos );

  CEntity3D( );
  XMVECTOR getFront() const { return getVectorFromYaw( yaw ); }
  XMVECTOR getLeft() const { return getVectorFromYaw( yaw + XM_PIDIV2 ); }

  float getAngleTo( XMVECTOR p) const;
  bool isInsideCone( XMVECTOR p, float cone_angle );

  bool isInFront( XMVECTOR p) const;
  bool isInLeft( XMVECTOR p) const;

};

#endif