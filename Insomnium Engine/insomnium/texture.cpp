#include "mcv_platform.h"
#include "texture.h"
#include "utils.h"

//----------------------------------------------------------
// Constructor
//----------------------------------------------------------
CTexture::CTexture() : mResourceView(NULL), mMemSize(0){}

//----------------------------------------------------------
// Load the texture from the memory
//----------------------------------------------------------
bool CTexture::Load( const char *aFilename ) {
    assert(mResourceView == NULL);
    assert(aFilename != 0);

    mFileName = aFilename;

    // Load shader resource view
    ID3D11Resource *ppTexture;
    D3DX11_IMAGE_LOAD_INFO loadInfo;
    ZeroMemory(&loadInfo, sizeof(D3DX11_IMAGE_LOAD_INFO));
    loadInfo.BindFlags = D3D10_BIND_SHADER_RESOURCE;
    D3DX11CreateShaderResourceViewFromFile(App.d3dDevice, aFilename, NULL, NULL, &mResourceView, NULL);
    setDbgName(mResourceView, aFilename);

    //Load shader resource description
    D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
    mResourceView->GetDesc(&srvDesc);
    mResourceView->GetResource(&ppTexture);
    D3D11_RESOURCE_DIMENSION type;
   
    //Load the texture description
    ppTexture->GetType(&type);
    switch (type)
    {
         case D3D11_RESOURCE_DIMENSION_TEXTURE2D:
             {
                 ID3D11Texture2D *pTexture2D = (ID3D11Texture2D*)ppTexture;
                 pTexture2D->GetDesc(&mTextureDesc);

                 srvDesc.Format = mTextureDesc.Format;
                 srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
                 srvDesc.Texture2D.MipLevels = mTextureDesc.MipLevels;
                 srvDesc.Texture2D.MostDetailedMip = mTextureDesc.MipLevels - 1;
             }
             break;
         default:
            assert(0); //Invalid texture format!!"
        break;
    }

    MemSize( );
    return true;
}

//----------------------------------------------------------
// Calculate the memory size of the texture
//----------------------------------------------------------
void CTexture::MemSize( )
{
    int lImgWidth = static_cast<int>(mTextureDesc.Width);
    int lImgHeight = static_cast<int>(mTextureDesc.Height);

    assert(lImgWidth <= 2048 || lImgHeight <= 2048);
    mMemSize = 0;
    for (uint32 i = 1, lMipLevels = mTextureDesc.MipLevels; i <= lMipLevels; ++i)
    {
        mMemSize += lImgWidth * lImgHeight;
        lImgWidth  = static_cast<int>(utils::numeric::Max(static_cast<int>(mTextureDesc.Width) >> i, static_cast<int>(1)));
        lImgHeight = static_cast<int>(utils::numeric::Max(static_cast<int>(mTextureDesc.Width) >> i, static_cast<int>(1)));
    }
    mMemSize *= GetFormatsize(mTextureDesc.Format);
    mMemSize /= 1024.f;     //Kilobyte
    mMemSize /= 1024.f;     //Megabyte
    assert( mMemSize );
}

//----------------------------------------------------------
// Get the size in byte 
//----------------------------------------------------------
int CTexture::GetFormatsize( DXGI_FORMAT aFormat )
{
    uint32 lFormatSize = 0;
    switch (aFormat)
    {
        case DXGI_FORMAT_R8G8B8A8_UNORM:     //32-bit unsigned-normalized-integer
            lFormatSize = 32;
            break;
        case DXGI_FORMAT_BC3_UNORM:          //Bytes per 4x4 pixel block 16
            lFormatSize = 8;
            break;
        case DXGI_FORMAT_R16_UNORM:          //16-bit unsigned-integer
            lFormatSize = 16;
            break;
        case DXGI_FORMAT_R16G16B16A16_FLOAT: //64-bit floating-point format that supports
            lFormatSize = 64;
            break;
        default:
            assert(0);                      //Invalid format
    }
    return lFormatSize / 8.f;               //Convert to byte
}

//----------------------------------------------------------
// Destroy
//----------------------------------------------------------
void CTexture::Destroy( ) {
    if (mResourceView)
    {
        SAFE_RELEASE( mResourceView );
    }
}
