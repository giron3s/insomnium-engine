#ifndef INC_WORLD_SYSTEM_H_
#define INC_WORLD_SYSTEM_H_

#include "mcv_platform.h"
#include "render_texture.h"
#include "shader.h"

class CWorldSystem {
private:
    CRenderTechnique *tech_gbuffer;
    //CRenderTechnique *tech_reflection;
    CRenderTechnique *tech_composite;

  void renderGBuffer();
  void renderReflection();
 

public:
  CWorldSystem();
  bool init();
  void render();
  void renderComposite();
  void destroy();
};

#endif
