#include "mcv_platform.h"
#include "skeleton_manager.h"
#include "mesh_manager.h"
#include "utils.h"
#include "vertex_declarations.h"
#include "rigid_animation_manager.h"

CSkeletonManager skeleton_manager;

template<>
bool initObjFromName( CCoreSkeleton &obj, const char *c_name ) {
  std::string name( c_name );
  CalLoader::setLoadingMode( LOADER_ROTATE_X_AXIS );

  // setName( name );

  std::string root_path = "data/characters/" + name + "/";

  std::string csf = root_path + name + ".csf";
  bool is_ok = obj.loadCoreSkeleton( csf );
  assert( is_ok );

  obj.idle_id = obj.loadCoreAnimation( root_path + "idle.caf", "idle" );
  assert( obj.idle_id >= 0 );
  obj.walk_id = obj.loadCoreAnimation( root_path + "impact.caf", "impact" );
  assert( obj.walk_id >= 0 );

  std::string cmf = root_path + name + ".cmf";
  obj.mesh_id = obj.loadCoreMesh( cmf );
  assert( obj.mesh_id >= 0 );

  // Registrar la malla en el mesh_manager. 
  // Primero confirmar que no este ya antes.
  assert( !mesh_manager.exists( name.c_str() ) );

  CalCoreMesh *core_mesh = obj.getCoreMesh( obj.mesh_id );
  assert( core_mesh );

  assert( core_mesh->getCoreSubmeshCount( ) >= 1 );
  CalCoreSubmesh *core_submesh = core_mesh->getCoreSubmesh( 0 );
  size_t nfaces = core_submesh->getFaceCount( );

  // Get a copy
  std::vector< CalCoreSubmesh::Face > faces = core_submesh->getVectorFace( );
  for( size_t i=0; i<faces.size(); ++i )
    std::swap( faces[ i ].vertexId[1], faces[ i ].vertexId[2] );

  size_t nvertexs = core_submesh->getVertexCount( );

  dbg("cal->mesh with %ld vtxs and %ld faces and has %ld submeshes\n", nvertexs, faces.size(), core_mesh->getCoreSubmeshCount());

  // Get access to the texture coords of cal3d
  // vector de vector de coords, pq cal soporta mas de 1 juego de coords
  typedef std::vector< CalCoreSubmesh::TextureCoordinate > VCalCoords;
  const std::vector< VCalCoords > &cal_all_coords = core_submesh->getVectorVectorTextureCoordinate( );
  assert( cal_all_coords.size() > 0 );
  // Me quedo solo con el primer juego de coords
  const VCalCoords &cal_coords = cal_all_coords[ 0 ];
  
  const std::vector< CalCoreSubmesh::Vertex > &cal_vtxs = core_submesh->getVectorVertex();

  // Calcular los bones que se estan usando de verdad
  typedef std::map< int, int > MUsedBones;
  MUsedBones used_bones;
  for (size_t i = 0; i < cal_vtxs.size(); ++i) {
    const CalCoreSubmesh::Vertex *c = &cal_vtxs[i];
    for (size_t j = 0; j < c->vectorInfluence.size(); ++j) {
      const CalCoreSubmesh::Influence &f = c->vectorInfluence[j];
      used_bones[f.boneId]++;
    }
  }
  MUsedBones remap_bones;
  MUsedBones::iterator it = used_bones.begin();
  while (it != used_bones.end()) {
    remap_bones[it->first] = remap_bones.size();
    obj.used_bones_ids.push_back(it->first);
    ++it;
  }
  dbg("We are using %ld bones from the total %ld\n", used_bones.size(), obj.getCoreSkeleton()->getVectorCoreBone().size());

  // Do a copy of the cal vertexs
  std::vector< CVertexSkin > vtxs;
  for( size_t i=0; i<cal_vtxs.size(); ++i ) {
    const CalCoreSubmesh::Vertex *c = &cal_vtxs[ i ];
    CVertexSkin p;
    memset( &p, 0x00, sizeof( CVertexSkin ));
    p.Pos.x = c->position.x;
    p.Pos.y = c->position.y;
    p.Pos.z = c->position.z;
    p.Nor.x = c->normal.x;
    p.Nor.y = c->normal.y;
    p.Nor.z = c->normal.z;
    p.Tex.x = cal_coords[ i ].u;
    p.Tex.y = cal_coords[ i ].v;
    
    // Para todas las influencias de cal3d
    int total_weight = 0;
    assert( c->vectorInfluence.size() <= 4 );
    for( size_t j=0; j<c->vectorInfluence.size(); ++j ) {
      const CalCoreSubmesh::Influence &f = c->vectorInfluence[ j ];
      p.weights[ j ] = (f.weight * 255);
      p.indices[j] = remap_bones[ f.boneId ];
      total_weight += p.weights[ j ];
    }
    assert( total_weight <= 255 );
    int err_weight = 255 - total_weight;
    p.weights[ 0 ] += err_weight;

    vtxs.push_back( p );
  }

  CRenderMesh *mesh = new CRenderMesh;
  mesh->createMesh<CVertexSkin>( &vtxs[0], nvertexs
    , &faces[0].vertexId[0]
    , nfaces * 3      // # de indices 
    , CRenderMesh::TRIANGLE_LIST
    , false );      // Ya no hace falta que sea dinamica

  mesh_manager.registerNewMesh( mesh, name );

  obj.configLookat( );

  // 
  obj.rigid_anim_of_impact = NULL;
  obj.rigid_anim_of_impact = rigid_animation_manager.getByName("cavernicola_impact");

  return true;
}

void CCoreSkeleton::configLookat( ) {

  CalCoreSkeleton *core_skel = getCoreSkeleton();
  lookat_steps.clear();

  TLookAtStep s;
  s.bone_id = core_skel->getCoreBoneId( "Bip01 Spine" );
  s.axis.set( 0,1,0 );
  s.amount = 0.2f;
  lookat_steps.push_back( s );

  s.bone_id = core_skel->getCoreBoneId( "Bip01 Spine1" );
  s.axis.set( 0,1,0 );
  s.amount = 0.2f;
  lookat_steps.push_back( s );

  s.bone_id = core_skel->getCoreBoneId( "Bip01 Neck" );
  s.axis.set( 0,1,0 );
  s.amount = 0.3f;
  lookat_steps.push_back( s );

  s.bone_id = core_skel->getCoreBoneId( "Bip01 Head" );
  s.axis.set( 0,1,0 );
  s.amount = 1.0f;
  lookat_steps.push_back( s );
}