#ifndef INC_RENDER_TECHNIQUES_MANAGER_H_
#define INC_RENDER_TECHNIQUES_MANAGER_H_

//--------------------------------------------------------------------------------------
#include "generic_manager.h"
#include "data_provider.h"
#include "shader.h"

class CRenderTechniqueManager : public CObjManager < CRenderTechnique > {
public:
    void registerTechnique(CRenderTechnique *tech);
    CRenderTechnique* GetTechniqueByGroup(TRenderTechniqueGroup aGroupId, const CVertexDeclaration *aVertexDeclation);

};
extern CRenderTechniqueManager render_techniques_manager;

#endif
