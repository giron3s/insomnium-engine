#include "mcv_platform.h"
#include "draw_utils.h"
#include "mesh.h"
#include "vertex_declarations.h"
#include "camera.h"
#include "aabb.h"
#include "texture.h"
#include "angular.h"

#include "render_techniques_manager.h"

ID3D11Buffer*            g_pConstantBufferCamera = NULL;
ID3D11Buffer*            g_pConstantBufferObject = NULL;
ID3D11Buffer*            g_pConstantBufferLight  = NULL;


void fillCubeCoords(CVertexSolid *v, XMVECTOR vmin, XMVECTOR vmax, XMFLOAT4 color) {
  CVertexSolid *vertices = v;
  v->Pos = XMFLOAT3(XMVectorGetX(vmin), XMVectorGetY(vmin), XMVectorGetZ(vmin)); ++v;
  v->Pos = XMFLOAT3(XMVectorGetX(vmax), XMVectorGetY(vmin), XMVectorGetZ(vmin)); ++v;
  v->Pos = XMFLOAT3(XMVectorGetX(vmin), XMVectorGetY(vmax), XMVectorGetZ(vmin)); ++v;
  v->Pos = XMFLOAT3(XMVectorGetX(vmax), XMVectorGetY(vmax), XMVectorGetZ(vmin)); ++v;
  v->Pos = XMFLOAT3(XMVectorGetX(vmin), XMVectorGetY(vmin), XMVectorGetZ(vmax)); ++v;
  v->Pos = XMFLOAT3(XMVectorGetX(vmax), XMVectorGetY(vmin), XMVectorGetZ(vmax)); ++v;
  v->Pos = XMFLOAT3(XMVectorGetX(vmin), XMVectorGetY(vmax), XMVectorGetZ(vmax)); ++v;
  v->Pos = XMFLOAT3(XMVectorGetX(vmax), XMVectorGetY(vmax), XMVectorGetZ(vmax)); ++v;
  for (int i = 0; i<8; ++i)
    vertices[i].Color = color;
}

bool createWiredCube(CRenderMesh &m, XMVECTOR vmin, XMVECTOR vmax, bool updateable) {
  CVertexSolid vertices[ 8 ];
  fillCubeCoords(vertices, vmin, vmax, XMFLOAT4(0.5f, 0.5f, 0.5f, 0.5f));

  WORD indices[] = { 0, 1
                   , 2, 3
                   , 4, 5
                   , 6, 7
                   
                   , 0, 2
                   , 1, 3
                   , 4, 6
                   , 5, 7

                   , 0, 4
                   , 1, 5
                   , 2, 6
                   , 3, 7
  };
  if (!m.createMesh(vertices, 8, indices, 24, CRenderMesh::LINE_LIST, updateable))
    return false;
  return true;
}


// 2, 5
bool createGrid( CRenderMesh &m, int nbigs, int nsmalls ) {
  
  // Calcular # de vertices que voy a necesitar
  int n_per_half_side = nbigs * nsmalls;
  int n_per_side = n_per_half_side * 2 + 1;
  int nvertexs = n_per_side * 4;
  int nindices = nvertexs;

  // Pedir la memoria
  CVertexSolid *vs = new CVertexSolid[ nvertexs ];

  XMFLOAT4 Color2( 0.3f, 0.3f, 0.3f, 0.3f );
  XMFLOAT4 Color1( 0.5f, 0.5f, 0.5f, 0.5f );

  CVertexSolid *v = vs;
  // Generar lineas cada 2 vertices
  for( int i=-n_per_half_side; i<=n_per_half_side; ++i ) {
    XMFLOAT4 clr = ( i % nsmalls ) == 0 ? Color1 : Color2;
    v->Color = clr; v->Pos = XMFLOAT3( i, 0,  n_per_half_side ); ++v;
    v->Color = clr; v->Pos = XMFLOAT3( i, 0, -n_per_half_side ); ++v;
    v->Color = clr; v->Pos = XMFLOAT3(  n_per_half_side, 0, i ); ++v;
    v->Color = clr; v->Pos = XMFLOAT3( -n_per_half_side, 0, i ); ++v;
  }

  // Crear un index buffer con datos 0,1,2,3... nvtxs
  CRenderMesh::TIndex *idxs = new CRenderMesh::TIndex[ nindices ];
  for( int i=0; i<nindices; ++i )
    idxs[ i ] = i;

  // Llamar a m.createVertex( .. )
  bool is_ok = m.createMesh( vs, nvertexs, idxs, nindices, CRenderMesh::LINE_LIST );

  // Liberar la memoria de c++
  delete [] idxs;
  delete [] vs;

  return is_ok;
}

// ----------------------------------------------------------
bool createIcosahedron(CRenderMesh &m) {
#define X (.525731112119133606f*1.2584085f)
#define Z (.850650808352039932f*1.2584085f)

  static const float vs[12][3] = {
      { -X, 0.0, Z }, {  X, 0.0, Z }, { -X, 0.0, -Z }, {  X, 0.0, -Z },
      {  0.0, Z, X }, { 0.0, Z, -X }, {  0.0, -Z, X }, { 0.0, -Z, -X },
      {  Z, X, 0.0 }, { -Z, X, 0.0 }, {  Z, -X, 0.0 }, { -Z, -X, 0.0 }
  };
  CVertexPosition vpos[12];
  memcpy(vpos, vs, 12 * sizeof(CVertexPosition));

#undef X
#undef Y
  static const CRenderMesh::TIndex idxs[20][3] = {
      { 0, 4, 1 }, { 0, 9, 4 }, { 9, 5, 4 }, { 4, 5, 8 }, { 4, 8, 1 },
      { 8, 10, 1 }, { 8, 3, 10 }, { 5, 3, 8 }, { 5, 2, 3 }, { 2, 7, 3 },
      { 7, 10, 3 }, { 7, 6, 10 }, { 7, 11, 6 }, { 11, 0, 6 }, { 0, 1, 6 },
      { 6, 1, 10 }, { 9, 0, 11 }, { 9, 11, 2 }, { 9, 2, 5 }, { 7, 2, 11 } };
  return m.createMesh(vpos, 12, &idxs[0][0], 20 * 3, CRenderMesh::TRIANGLE_LIST);
}

// ----------------------------------------------------------
bool createCubeTextured( CRenderMesh &m ) {
  
  // Axis -----------------------
  CVertexTextured vertices[] = {
        { XMFLOAT3( -1.0f, 1.0f, -1.0f ), XMFLOAT2( 0.0f, 0.0f ) },
        { XMFLOAT3( 1.0f, 1.0f, -1.0f ), XMFLOAT2( 1.0f, 0.0f ) },
        { XMFLOAT3( 1.0f, 1.0f, 1.0f ), XMFLOAT2( 1.0f, 1.0f ) },
        { XMFLOAT3( -1.0f, 1.0f, 1.0f ), XMFLOAT2( 0.0f, 1.0f ) },

        { XMFLOAT3( -1.0f, -1.0f, -1.0f ), XMFLOAT2( 0.0f, 0.0f ) },
        { XMFLOAT3( 1.0f, -1.0f, -1.0f ), XMFLOAT2( 1.0f, 0.0f ) },
        { XMFLOAT3( 1.0f, -1.0f, 1.0f ), XMFLOAT2( 1.0f, 1.0f ) },
        { XMFLOAT3( -1.0f, -1.0f, 1.0f ), XMFLOAT2( 0.0f, 1.0f ) },

        { XMFLOAT3( -1.0f, -1.0f, 1.0f ), XMFLOAT2( 0.0f, 0.0f ) },
        { XMFLOAT3( -1.0f, -1.0f, -1.0f ), XMFLOAT2( 1.0f, 0.0f ) },
        { XMFLOAT3( -1.0f, 1.0f, -1.0f ), XMFLOAT2( 1.0f, 1.0f ) },
        { XMFLOAT3( -1.0f, 1.0f, 1.0f ), XMFLOAT2( 0.0f, 1.0f ) },

        { XMFLOAT3( 1.0f, -1.0f, 1.0f ), XMFLOAT2( 0.0f, 0.0f ) },
        { XMFLOAT3( 1.0f, -1.0f, -1.0f ), XMFLOAT2( 1.0f, 0.0f ) },
        { XMFLOAT3( 1.0f, 1.0f, -1.0f ), XMFLOAT2( 1.0f, 1.0f ) },
        { XMFLOAT3( 1.0f, 1.0f, 1.0f ), XMFLOAT2( 0.0f, 1.0f ) },

        { XMFLOAT3( -1.0f, -1.0f, -1.0f ), XMFLOAT2( 0.0f, 0.0f ) },
        { XMFLOAT3( 1.0f, -1.0f, -1.0f ), XMFLOAT2( 1.0f, 0.0f ) },
        { XMFLOAT3( 1.0f, 1.0f, -1.0f ), XMFLOAT2( 1.0f, 1.0f ) },
        { XMFLOAT3( -1.0f, 1.0f, -1.0f ), XMFLOAT2( 0.0f, 1.0f ) },

        { XMFLOAT3( -1.0f, -1.0f, 1.0f ), XMFLOAT2( 0.0f, 0.0f ) },
        { XMFLOAT3( 1.0f, -1.0f, 1.0f ), XMFLOAT2( 1.0f, 0.0f ) },
        { XMFLOAT3( 1.0f, 1.0f, 1.0f ), XMFLOAT2( 1.0f, 1.0f ) },
        { XMFLOAT3( -1.0f, 1.0f, 1.0f ), XMFLOAT2( 0.0f, 1.0f ) },
    }; 
  WORD indices[] = {  
         1,3, 0,
         1,2, 3,

         4,6, 5,
         4,7, 6,

         9,11,8,
         9,10,11,

        12,14,13,
        12,15,14,

        17,19,16,
        17,18,19,

        20,22,21,
        20,23,22 
  };
  if( !m.createMesh( vertices, 24, indices, 36, CRenderMesh::TRIANGLE_LIST ))
    return false;
  return true;
}

bool createConstantBuffer(size_t nbytes, ID3D11Buffer** buffer, const char *name) {
  HRESULT hr;
  // Create the constant buffers
  // One for the camera parameters
  D3D11_BUFFER_DESC bd;
  ZeroMemory(&bd, sizeof(bd));
  bd.Usage = D3D11_USAGE_DEFAULT;
  bd.ByteWidth = nbytes;
  bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
  bd.CPUAccessFlags = 0;
  hr = App.d3dDevice->CreateBuffer(&bd, NULL, buffer);
  if (FAILED(hr))
    return false;
  setDbgName( *buffer, name );
  return true;
}

bool createConstantBuffers()  {
  return createConstantBuffer(sizeof(ConstantBufferCamera), &g_pConstantBufferCamera, "BufCamera")
    && createConstantBuffer(sizeof(ConstantBufferObject), &g_pConstantBufferObject, "BufObject")
    && createConstantBuffer(sizeof(ConstantBufferLight), &g_pConstantBufferLight, "BufLight")
    ;

}

namespace DrawUtils {

  TFont       dbg_font;
  CRenderMesh mesh_axis;
  CRenderMesh mesh_grid;
  CRenderMesh mesh_line;
  CRenderMesh mesh_aabb;
  CRenderMesh mesh_cube_textured;
  CRenderMesh mesh_frustum;
  CRenderMesh mesh_texture2d;
  CRenderMesh mesh_icosahedron;
  
  // ------------------------------------------
  bool createDrawUtils() {
      XMVECTOR v = getVectorFromYawPitch( deg2rad(0.1), deg2rad(90));
      XMVECTOR v2 = getVectorFromYaw(deg2rad(155));

      float yaw = getYawFromVector(v2); 
    // Axis -----------------------
    CVertexSolid vertices[] = {
      { XMFLOAT3( 0.0f, 0.0f, 0.0f ), XMFLOAT4( 1.0f, 0.0f, 0.0f, 1.0f ) },
      { XMFLOAT3( 10.0f, 0.0f, 0.0f ), XMFLOAT4( 1.0f, 0.0f, 0.0f, 1.0f ) },

      { XMFLOAT3( 0.0f, 0.0f, 0.0f ), XMFLOAT4( 0.0f, 1.0f, 0.0f, 1.0f ) },
      { XMFLOAT3( 0.0f, 20.0f, 0.0f ), XMFLOAT4( 0.0f, 1.0f, 0.0f, 1.0f ) },

      { XMFLOAT3( 0.0f, 0.0f, 0.0f ), XMFLOAT4( 0.0f, 0.0f, 1.0f, 1.0f ) },
      { XMFLOAT3( 0.0f, 0.0f, 30.0f ), XMFLOAT4( 0.0f, 0.0f, 1.0f, 1.0f ) },

      { XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f) },
      { XMFLOAT3(XMVectorGetX(v) * 10, XMVectorGetY(v) * 10, XMVectorGetZ(v) * 10), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f) },
    };
    WORD indices[] = { 0,1,2, 3,4,5, 6,7 };
    if( !mesh_axis.createMesh( vertices, 8, indices, 8, CRenderMesh::LINE_LIST ))
      return false;

    if( !mesh_line.createMesh<CVertexSolid>( NULL, 2, NULL, 0, CRenderMesh::LINE_LIST, true ))
      return false;

    // Grid -----------------------
    if( !createGrid( mesh_grid, 40, 5 ) )
      return false;

    // Cube textured ----------------------
    if (!createCubeTextured(mesh_cube_textured))
      return false;

    // Icosahedron for deferred ----------------------
    if (!createIcosahedron(mesh_icosahedron))
      return false;

    // Para el drawTexture2D ----------------------
    if (!mesh_texture2d.createMesh<CVertexTextured>(NULL, 4, NULL, 0, CRenderMesh::TRIANGLE_STRIP, true))
      return false;

    // Frustum -------------------
    if (!createWiredCube(mesh_frustum, XMVectorSet(-1, -1, 0, 0), XMVectorSet(1, 1, 1, 0), false))
      return false;    
    
    if (!createWiredCube(mesh_aabb, XMVectorSet(-1, -1, 0, 0), XMVectorSet(1, 1, 1, 0), true))
      return false;

    if( !createConstantBuffers() )
      return false;

    dbg_font.size = 16.f;
    dbg_font.create( );

    return true;
  }

  // -------------------------------------------
  void destroyDrawUtils() {
    if (g_pConstantBufferCamera) g_pConstantBufferCamera->Release();
    if (g_pConstantBufferObject) g_pConstantBufferObject->Release();
    if (g_pConstantBufferLight) g_pConstantBufferLight->Release();
    mesh_icosahedron.destroy();
    mesh_aabb.destroy();
    mesh_frustum.destroy();
    mesh_cube_textured.destroy();
    mesh_grid.destroy( );
    mesh_line.destroy( );
    mesh_axis.destroy();
    dbg_font.destroy();
  }

  void drawAxis() {
    mesh_axis.render( );
  }
  
  void drawGrid() {
    mesh_grid.render( );
  }

  void drawIcosahedron() {
    mesh_icosahedron.render();
  }

  void drawCubeTextured() {
    mesh_cube_textured.render( );
  }

  void drawLine( XMVECTOR a, XMVECTOR b, XMFLOAT4 clr ) {
    CVertexSolid vertices[2];
    vertices[ 0 ].Pos.x = XMVectorGetX( a );
    vertices[ 0 ].Pos.y = XMVectorGetY( a );
    vertices[ 0 ].Pos.z = XMVectorGetZ( a );
    vertices[ 0 ].Color = clr;
    vertices[ 1 ].Pos.x = XMVectorGetX( b );
    vertices[ 1 ].Pos.y = XMVectorGetY( b );
    vertices[ 1 ].Pos.z = XMVectorGetZ( b );
    vertices[ 1 ].Color = clr;
    mesh_line.updateFromCPU( vertices, 2 * sizeof( CVertexSolid ) );
    mesh_line.render();
  }


  void draw(const CAABB &aabb, XMFLOAT4 color) {
    CVertexSolid vertices[8];
    fillCubeCoords(vertices, aabb.GetMinCorner(), aabb.GetMaxCorner(), color);
    mesh_aabb.updateFromCPU(vertices, 8 * sizeof(CVertexSolid));
    mesh_aabb.render();
  }

  // --------------------------------------------------
  void drawCameraFrustum( const CCamera &frustum_to_render
                        //, const CCamera &from_camera 
                        ) {

    // La world sera la inversa de la view_proj de la camera
    // de la cual queremos pintar su frustum
    XMVECTOR determinant;
    XMMATRIX world = XMMatrixInverse( &determinant, frustum_to_render.getViewProjection() );

    // El transpose lo pide el shader!!
    setWorldMatrix( world );
    
    // draw unit cube +/-1 +/-1 0..1
    mesh_frustum.render( );
  }

  void activateCamera( const CCamera &camera ) {
    ConstantBufferCamera cb;
    cb.View = camera.getView();
    cb.Projection = camera.getProjection();
    cb.ViewProjection = camera.getViewProjection();

    XMVECTOR determinant;
    cb.inv_view = XMMatrixInverse(&determinant, camera.getView());

    XMVECTOR loc = camera.getPosition();
    cb.WorldCameraPos.x = XMVectorGetX(loc);
    cb.WorldCameraPos.y = XMVectorGetY(loc);
    cb.WorldCameraPos.z = XMVectorGetZ(loc);
    cb.WorldCameraPos.w = 1.0f;
    App.immediateContext->UpdateSubresource(g_pConstantBufferCamera, 0, NULL, &cb, 0, 0);
  }


  void activateReflection(CRenderTechnique *tech, XMMATRIX reflection, const CCamera &camera){
      bool is_ok;
      ConstantBufferReflection cb;

      cb.Reflection = reflection;
      cb.Reflection_View = camera.getView();
      cb.Reflection_ViewProj = camera.getView() * camera.getProjection();
      is_ok = tech->setParam("ConstantBufferReflection", cb);
      assert(is_ok);
  }

  void activateLight(const CCamera &light) {
    ConstantBufferLight cb;
    //cb.View = light.getView();
    //cb.Projection = light.getProjection();
    cb.LightViewProjection = light.getViewProjection();

    XMVECTOR loc = light.getPosition();
    cb.WorldLightPos.x = XMVectorGetX(loc);
    cb.WorldLightPos.y = XMVectorGetY(loc);
    cb.WorldLightPos.z = XMVectorGetZ(loc);
    cb.WorldLightPos.w = 1.0f;

    // matrix to transform from world space to light view-proj space
    // including the conversion from -1..1 to 0..1
    XMMATRIX offset_mtx = XMMatrixIdentity();
    offset_mtx(0, 0) = 0.5f;
    offset_mtx(3, 0) = 0.5f;
    offset_mtx(1, 1) = -0.5f;
    offset_mtx(3, 1) = 0.5f;
    cb.LightViewProjectionOffset = cb.LightViewProjection * offset_mtx;

    App.immediateContext->UpdateSubresource(g_pConstantBufferLight, 0, NULL, &cb, 0, 0);
  }

  void activateFullScreenOrthoCamera() {
    CCamera camera;
    camera.loadOrtho(0, App.xres, 0, App.yres, -1, 1);
    activateCamera(camera);
    setWorldMatrix(XMMatrixIdentity());
  }

  void setWorldMatrix( const XMMATRIX &new_world ) {
    ConstantBufferObject cb;
    //cb.World = XMMatrixTranspose( new_world );
    cb.World = new_world;
    App.immediateContext->UpdateSubresource( g_pConstantBufferObject, 0, NULL, &cb, 0, 0 );
  }

  void drawTextured2D(int xmin, int ymin, int w, int h, CTexture *t, CRenderTechnique *tech) {
      //assert(t);
      //assert(t->resource_view);
      CVertexTextured vtxs[4];
      CVertexTextured *v = vtxs;
      float z = 1.f;
      v->Pos = XMFLOAT3(xmin, ymin, z);     v->Tex = XMFLOAT2(0, 0); ++v;
      v->Pos = XMFLOAT3(xmin + w, ymin, z); v->Tex = XMFLOAT2(1, 0); ++v;
      v->Pos = XMFLOAT3(xmin, ymin + h, z); v->Tex = XMFLOAT2(0, 1); ++v;
      v->Pos = XMFLOAT3(xmin + w, ymin + h, z); v->Tex = XMFLOAT2(1, 1); ++v;
      // activar el shader de 'solo textura'
      if (tech == NULL){
          const CRenderTechnique *ctech = render_techniques_manager.getByName("textured");
          tech = const_cast<CRenderTechnique *>(ctech);
      }

      //Bind the texture only if exist
        if (t != NULL){
            tech->setTexture("txDiffuse", t);
        }

        tech->activate();
        mesh_texture2d.updateFromCPU(vtxs, 4 * sizeof(CVertexTextured));
        mesh_texture2d.render();
  }
}

