#include "mcv_platform.h"
#include "render_techniques_manager.h"
#include "utils.h"

CRenderTechniqueManager render_techniques_manager;

template<>
bool initObjFromName(CRenderTechnique &obj, const char *name) {
    utils::logger::fatal("Failed at init a render_tech from a name!!!");
    return false;
}

void CRenderTechniqueManager::registerTechnique(CRenderTechnique *tech) {
    assert(tech && tech->getName());
    assert(!exists(tech->getName()));
    objs[tech->getName()] = tech;
}

CRenderTechnique* CRenderTechniqueManager::GetTechniqueByGroup(TRenderTechniqueGroup aGroupId, const CVertexDeclaration *aVertexDeclation){
    bool isFind = false;
    CRenderTechnique *lSearched = NULL;
    typedef std::map< std::string, CRenderTechnique *>::iterator lIT; // CRenderTechnique
    for (lIT lIter = objs.begin(); lIter != objs.end() && !isFind; lIter++){
        CRenderTechnique * lCurrentTechnique = lIter->second;
        if (lCurrentTechnique->GetVertexDeclaration() == aVertexDeclation && lCurrentTechnique->GetRenderGroup() == aGroupId){
            isFind = true;
            lSearched = lIter->second;
        }
    }
    return lSearched;
}
