#include "mcv_platform.h"
#include "texture_manager.h"
#include "utils.h"

CTextureManager texture_manager;

bool initObjFromName( CTexture &obj, const char *name ) {
  std::string full_name = "data/textures/" + std::string(name);
  bool is_ok = obj.Load( full_name.c_str() );
  assert( is_ok );
  return is_ok;
}

void CTextureManager::registerNewTexture(CTexture *new_texture, const std::string &new_name) {
  assert(!exists(new_name.c_str()));
  objs[new_name] = new_texture;
  setDbgName(new_texture->mResourceView, new_name.c_str());
}



