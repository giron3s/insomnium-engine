#include "mcv_platform.h"
#include "entity_controllers.h"
#include "angular.h"
#include "utils.h"
#include "components/components.h"
#include "components/comp_transform.h"
#include "input_manager.h"

CWalkEntityController::CWalkEntityController( ) 
  : linear_speed( 2.0f )
  , angular_speed( deg2rad( 90.f ) )
{ }

void CWalkEntityController::updateEntity( CEntity *e, float delta ) {

  float amount_moved = linear_speed * delta;
  float amount_rotated = angular_speed * delta;

  CTransform *t = e->get<CTransform>( );
  assert( t );

  if( !keyIsPressed( VK_SHIFT ) )
    return;

  // Adelante y atras
  if( keyIsPressed( 'W' ) ) 
    t->setPosition( t->getPosition() + t->getFront() * amount_moved );
  else if( keyIsPressed( 'S' ) ) 
    t->setPosition( t->getPosition() - t->getFront() * amount_moved );

  // strafe left/right
  if( keyIsPressed( 'A' ) ) 
    t->setPosition( t->getPosition() + t->getLeft() * amount_moved );
  else if( keyIsPressed( 'D' ) ) 
    t->setPosition( t->getPosition() - t->getLeft() * amount_moved );

  // rotate left/right
  if( keyIsPressed( 'Q' ) ) {
    XMMATRIX delta_rot_y = XMMatrixRotationAxis( XMVectorSet( 0, 1, 0, 0 ), amount_rotated );
    XMVECTOR abs_pos = t->getPosition();
    t->setPosition( XMVectorSet( 0,0,0,0 ) );
    t->getWorld() = t->getWorld() * delta_rot_y;
    t->setPosition( abs_pos );
  }
  else if( keyIsPressed( 'E' ) )  {
    XMMATRIX delta_rot_y = XMMatrixRotationAxis( XMVectorSet( 0, 1, 0, 0 ), -amount_rotated );
    XMVECTOR abs_pos = t->getPosition();
    t->setPosition( XMVectorSet( 0,0,0,0 ) );
    t->getWorld() = t->getWorld() * delta_rot_y;
    t->setPosition( abs_pos );
  }
}

CAimToTargetEntityController::CAimToTargetEntityController( ) 
  : target( NULL ) 
  , angular_speed( deg2rad( 15.0f ) )   // low rotation speed
{ }

void CAimToTargetEntityController::updateEntity( CEntity *e, float delta ) {
  //float err_angle = e->getAngleTo( target->getPosition() );
  
  // rotate based on err_angle and angular_speed
  // ...

  //e->setYaw( e->getYaw() + err_angle );
}


// --------------------------------------------------
CFreeCameraController::CFreeCameraController( )
: angular_speed( deg2rad( 90.0f ) )
, pan_speed( 180.0f )
, zoom_speed( 10.0f )
{ }
  
void CFreeCameraController::updateEntity( CEntity *e, float delta ) {
  CTransform *t = e->get<CTransform>();

  if( keyIsPressed( VK_SHIFT ) )
    return;

  // Where is looking the object now?
  XMVECTOR front = t->getFront();
  XMVECTOR source = t->getPosition( );
  XMVECTOR target = source + front;
  
  // Convert to polar coords
  float yaw,pitch;
  getYawPitchFromVector( front, &yaw, &pitch );

  // Get mouse
  const CInputManager::TMouse &m = InputManager.getMouse();

  if( keyIsPressed( 'W' ) ) {
    source += front * delta * pan_speed;
    target += front * delta * pan_speed;
  }  
  if( keyIsPressed( 'S' ) ) {
    source -= front * delta * pan_speed;
    target -= front * delta * pan_speed;
  }

  // Pan if left button is pressed
  if (InputManager.isPressed(CInputManager::MOUSE_LEFT)) {
    XMVECTOR world_delta = t->getLeft() * delta * m.dx;
    source += world_delta;
    target += world_delta;

  // else Change look target in polars 
  } else {
    yaw -= angular_speed * delta * m.dx;
    //yaw += angular_speed * delta;
    pitch -= angular_speed * delta * m.dy;
    if( pitch < -deg2rad( 89.9f ) )
      pitch = -deg2rad( 89.9f );

    front = getVectorFromYawPitch( yaw, pitch );
    target = source + front;
  }

  t->lookAt( source, target );
}