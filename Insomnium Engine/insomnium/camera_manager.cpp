#include "mcv_platform.h"
#include "camera_manager.h"
#include "input_manager.h"

#include "angular.h"
#include "shader.h"
#include "components\components.h"
#include "components\comp_transform.h"

CCameraManager camera_manager;

bool CCameraManager::init(){
    XMVECTOR eye = XMVectorSet(6.0f, 5.0f, 7.0f, 0.0f);
    XMVECTOR target = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
    free_camera.lookAt(eye, target);
    free_camera.updatePerspective(deg2rad(75), App.xres / (float)App.yres, 1.0f, 5000.0f);

    eye = XMVectorSet(50.0f, 50.0f, 0.0f, 0.0f);
    target = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
    light_camera.lookAt(eye, target);
    light_camera.updatePerspective(deg2rad(45.f), 1.0f, 10.0f, 100.0f);

    free_camera_entity = new CEntity;
    CTransform *t = free_camera_entity->add< CTransform >();
    t->lookAt(XMVectorSet(50.f, 50.f, 50.0f, 0.f), XMVectorSet(0.f, 0.f, 0.f, 0.f));
    
    light_camera_entity = new CEntity;
    t = light_camera_entity->add< CTransform >();
    t->lookAt(XMVectorSet(40.f, 60.f, 0.0f, 0.f), XMVectorSet(0.f, 0.f, 0.f, 0.f));

    return true;
}

void CCameraManager::update( float delta ){
    if (InputManager.isPressed(CInputManager::Z_KEY))
        free_cam_controller.updateEntity(light_camera_entity, delta);
    else
        free_cam_controller.updateEntity(free_camera_entity, delta);

    CTransform *t;
    t = free_camera_entity->get<CTransform>();
    free_camera.lookAt(t->getPosition(), t->getPosition() + t->getFront());
    t = light_camera_entity->get<CTransform>();
    light_camera.lookAt(t->getPosition(), t->getPosition() + t->getFront());
}

void CCameraManager::uploadCameraToGPU( const CCamera* aCamera ){
    ConstantBufferCamera cb;
    cb.View             = aCamera->getView();
    cb.Projection       = aCamera->getProjection();
    cb.ViewProjection   = aCamera->getViewProjection();

    XMVECTOR determinant;
    cb.inv_view = XMMatrixInverse(&determinant, aCamera->getView());

    XMVECTOR loc        = aCamera->getPosition();
    cb.WorldCameraPos.x = XMVectorGetX(loc);
    cb.WorldCameraPos.y = XMVectorGetY(loc);
    cb.WorldCameraPos.z = XMVectorGetZ(loc);
    cb.WorldCameraPos.w = 1.0f;
    App.immediateContext->UpdateSubresource(g_pConstantBufferCamera, 0, NULL, &cb, 0, 0);
}

void CCameraManager::uploadCameraParamsToGPU(CRenderTechnique *aRenderTech){
    ConstantBufferCameraParams cb;
    cb.camera_aspect_ratio = free_camera.getAspectRatio();
    cb.camera_zfar         = free_camera.getZFar();
    cb.camera_znear        = free_camera.getZNear();
    cb.tan_half_fov        = tan(free_camera.getFov() * 0.5f);
    aRenderTech->setParam("ConstantBufferCameraParams", cb);
}


void CCameraManager::destroy(){

}
