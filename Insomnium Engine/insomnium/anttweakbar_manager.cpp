#include "mcv_platform.h"
#include "profiler_settings.h"
#include "anttweakbar_manager.h"
#include "render_settings.h"
CAntTweakBarManager anttweakbar_manager;



bool CAntTweakBarManager::init(){

    bool is_ok = true;
    is_ok = is_ok & TwInit(TW_DIRECT3D11, App.d3dDevice); // for Direct3D 11
    TwWindowSize(App.xres, App.yres);

    //set the bar size
    int barSize[2] = { 250, App.yres };
    
    // Create a tweak bar called 'Main' and change its refresh rate, position, size and transparency
    mMainBar = TwNewBar("Main");
    TwDefine(" Main label='Main TweakBar' position='16 16' size='260 320'");    
    TwSetParam(mMainBar, NULL, "size", TW_PARAM_INT32, 2, barSize);
    is_ok = is_ok & TwAddButton(mMainBar, "Camera", NULL, NULL, " label='Camera configurations' ");
    is_ok = is_ok & TwAddButton(mMainBar, "Render", CreateRenderBarCB, NULL, " label='Render configurations' ");
    is_ok = is_ok & TwAddButton(mMainBar, "Profiler", CreateProfilerBarCB, NULL, " label='Profiler' visible=true");
    assert(is_ok);
    return is_ok;
}

void CAntTweakBarManager::update( float delta ){
    TwRefreshBar(mMainBar);
}

void CAntTweakBarManager::render(){
    TwDraw();
}

void CAntTweakBarManager::destroy(){
    TwTerminate();
}