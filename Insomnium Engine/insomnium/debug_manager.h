#ifndef _DEBUG_MANAGER_
#define _DEBUG_MANAGER_

namespace debug_manager
{
    extern          int                     mDebugFrameID;

    void            Init                    ();
    void            Update                  (float delta);

    const bool      IsDebug                 ();
    const bool      IsDebugAABB             ();
    const bool      IsDebugGrid             ();
    const bool      IsDebugAxis             ();
    const bool      IsDebugWireFrame        ();
    const bool      IsDebugDeferred         ();
    int             GetDebugFrameID         ();

    const bool      IsDebugProfilerInfo     ();
    string          GetProfilerInfo         ();

    void            SetDebugAABB            ( bool aDebugAABB );
    void            SetDebugGrid            ( bool aDebugGrid );
    void            SetDebugAxis            ( bool aDebugAxis );
    void            SetDebugWireFrame       ( bool aDebugWireFrame );
    void            SetDebugDeferred        ( bool aDebugDeferred );
    void            SetDebugProfilerInfo    ( bool aDebugProfilerInfo );
    void            SetDebugProfilerInfoStr ( string aDebugProfilerInfoStr);

}
#endif