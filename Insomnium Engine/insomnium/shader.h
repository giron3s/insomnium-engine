#ifndef INC_SHADER_H_
#define INC_SHADER_H_

#include "mcv_platform.h"
#include <vector>
#include "render_constants.h"

class CVertexDeclaration;
class CTexture;

// ------------------------------------------
class CShader {

public:
  CShader( ) : reflector( NULL ) { }
  static bool createSamplers( );
  static void destroySamplers( );

  // ----------------------------
  struct TParam {
    char        name[32];
    size_t      nbytes;
    unsigned    slot;
    ID3D11Buffer* dx11_cte_buffer;
    bool          owned_by_shader;
    TParam( );
    // type float/cte/textura/...
  };

  typedef std::vector< TParam > VParams;
  VParams params;

  struct TTextureParam {
    char            name[32];
    unsigned        slot;
    const CTexture* current_value;
  };
  typedef std::vector< TTextureParam > VTextureParams;
  VTextureParams texture_params;

  static ID3D11SamplerState* sampler_linear_wrap;
  static ID3D11SamplerState* sampler_linear_clamp;

  const TParam *getParamByName( const char *name ) const;
  TTextureParam *getTextureParamByName(const char *name);

protected:
  
  ID3D11ShaderReflection*         reflector;
  void scanParams( ID3DBlob* pBlob );

};

// ------------------------------------------
class CVertexShader : public CShader {
public:
  ID3D11VertexShader*        vs;
  ID3D11InputLayout*         vertex_layout;
  const CVertexDeclaration*  vertex_decl;

  CVertexShader( );
  void destroy( );
  void activate( ) const;
  bool compile( const char *filename
              , const char *entry_point
              , const CVertexDeclaration *vtx_decl
              );
  const CVertexDeclaration* GetVertexDeclaration(){ return vertex_decl; };
  static const CVertexShader *current;
};

// ------------------------------------------
class CPixelShader : public CShader {

public:
  ID3D11PixelShader*   ps;
  CPixelShader( );
  void destroy( );
  void activate( ) const;
  bool compile( const char *filename
              , const char *entry_point
              );
};


// ------------------------------------------------
class CRenderTechnique {
  const char             *name;
  
  CVertexShader          vs;
  CPixelShader           ps;
  TRenderTechniqueGroup  group;
  TBlendConfig           blend_config;
  bool                   uses_bones;

  bool setParamDataBytes( const char *name, const void *data, size_t nbytes ) const ;

public:

  CRenderTechnique( ) : name( NULL ), uses_bones( false ) { }

  bool create( const char *name
             , const char *shader
             , const char *vs_function
             , const char *ps_function
             , const CVertexDeclaration *avtx_decl
             , TRenderTechniqueGroup tech_group
             , TBlendConfig blend_config = BLEND_DISABLED );
  void activate( ) const;

  template< class TCteDef >
  bool setParam( const char *name, const TCteDef &block_data ) const {
    return setParamDataBytes( name, &block_data, sizeof( TCteDef ) );
  }

  bool setTexture(const char *name, const CTexture *texture);
    
  const char *getName() const { return name; }
  bool usesBones() const { return uses_bones; }
  const CVertexDeclaration* GetVertexDeclaration(){ return vs.GetVertexDeclaration(); }
  const TRenderTechniqueGroup GetRenderGroup(){ return group; }

};

#endif
