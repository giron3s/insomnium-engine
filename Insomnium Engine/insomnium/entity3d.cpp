#include "mcv_platform.h"
#include "entity3d.h"

void CEntity3D::updateWorld() {
  world = XMMatrixRotationY( yaw ) * XMMatrixTranslationFromVector( pos );
}

void CEntity3D::setYaw( float new_yaw ) {
  yaw = new_yaw;
  updateWorld( );
}

void CEntity3D::setWorldMatrix( XMMATRIX new_world ) {
  world = new_world;
}

void CEntity3D::setPosition( XMVECTOR new_pos ) {
  pos = new_pos;
  updateWorld( );
}

CEntity3D::CEntity3D( ) : yaw( 0.0f ), mesh( NULL ) { 
  pos = XMVectorSet( 0,0,0,0 );
}

float CEntity3D::getAngleTo( XMVECTOR p) const {
  return getAngleDifferenceTo( pos, yaw, p );
}

bool CEntity3D::isInsideCone( XMVECTOR p, float cone_angle ) {
  // hacedlo vosotros!!
  return false;
}

bool CEntity3D::isInFront( XMVECTOR p) const {
  XMVECTOR dir = p - pos;
  XMVECTOR dot = XMVector3Dot( dir, getFront() );
  return XMVectorGetX( dot ) > 0.0f;
}

bool CEntity3D::isInLeft( XMVECTOR p) const {
  XMVECTOR dir = p - pos;
  XMVECTOR dot = XMVector3Dot( dir, getLeft() );
  return XMVectorGetX( dot ) > 0.0f;
}
