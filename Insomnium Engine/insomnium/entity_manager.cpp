#include "mcv_platform.h"
#include "entity_manager.h"

CEntityManager entity_manager;

CEntity *CEntityManager::get( const std::string &name ) {
  MEntitiesByName::iterator i = entities_by_name.find( name );
  if( i == entities_by_name.end() )
    return NULL;
  return i->second;
}

void CEntityManager::add( CEntity *e, const std::string &name ) {
  e->setName( name.c_str() );
  entities_by_name[ name ] = e;
}

void CEntityManager::add( CEntity *e ) {
  entities_by_name[ e->getName() ] = e;
}


