#ifndef _LOGGER_
#define _LOGGER_

namespace utils{
    namespace logger{

        void dbg(const char *fmt, ...);
        bool fatal(const char *fmt, ...);
    }
};
#endif 