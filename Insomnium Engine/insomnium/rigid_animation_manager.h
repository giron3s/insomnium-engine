#ifndef INC_RIGID_ANIMATION_MANAGER_H_
#define INC_RIGID_ANIMATION_MANAGER_H_

#include "components/components.h"
#include "generic_manager.h"
#include "data_provider.h"
#include "components/comp_transform.h"
#include <vector>

struct CCoreRigidAnimation {

  struct THeader {
    unsigned magic;
    unsigned version;
    unsigned nobjs;
    unsigned nsamples;
    unsigned bytes_per_name;
    float    total_time;
    unsigned dummy1;
    unsigned dummy2;
    static const unsigned valid_magic = 0x6a6a6a6a;
    bool isValid() const { return magic == valid_magic; }
  };

  struct TName {
    char name[32];
  };

  struct TKey {
    XMFLOAT4 trans;   // 4 floats
    XMFLOAT4 quat;    // 4 floats
  };

  typedef std::vector< TName > VNames;
  typedef std::vector< TKey > VKeys;
  THeader header;
  VNames  names;
  VKeys   keys;

  bool load(CDataProvider &dp);
  void apply(float curr_time, VEntities &targets, XMMATRIX world) const;

  // Me devuelve, del objecto obj_id, donde esta en base_time
  XMVECTOR getPositionAt(unsigned object_id, float base_time) const;
  XMVECTOR getTranslation(unsigned object_id, float base_time, float elapsed) const;
};

class CRigidAnimationManager : public CObjManager<CCoreRigidAnimation> {
public:
};
extern CRigidAnimationManager rigid_animation_manager;

#endif
