#ifndef INC_ANGULAR_H_
#define INC_ANGULAR_H_

XMVECTOR getVectorFromYaw( float yaw );
float    getYawFromVector( XMVECTOR v );
XMVECTOR getVectorFromYawPitch( float yaw, float pitch );
void     getYawPitchFromVector( XMVECTOR v, float *yaw, float *pitch );
float    getAngleDifferenceTo( XMVECTOR src, float yaw, XMVECTOR p);

float    deg2rad( float degrees );
float    rad2deg( float radians );

#endif