#include "mcv_platform.h"
#include "mesh_manager.h"

CMeshManager mesh_manager;

template<>
bool initObjFromName( CRenderMesh &obj, const char *name ) {
  std::string full_name = "data/meshes/" + std::string(name) + ".mesh";
  CFileDataProvider fdp( full_name.c_str() );
  bool is_ok = obj.load( fdp );
  assert( is_ok );
  return is_ok;
}

void CMeshManager::registerNewMesh( CRenderMesh *new_mesh, const std::string &new_name ) {
  assert( !exists( new_name.c_str() ) );
  objs[ new_name ] = new_mesh;
}
