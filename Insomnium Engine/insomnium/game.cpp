#include "mcv_platform.h"
#include "render_constants.h"
#include "draw_utils.h"
#include "utils.h"
#include "font.h"

#include "entity_manager.h"
#include "components\components.h"
#include "components\comp_renderable.h"
#include "components\comp_transform.h"
#include "render_manager.h"
#include "render_texture.h"
#include "camera_manager.h"

#include "entity_controllers.h"
#include "render_techniques_manager.h"
#include "anttweakbar_manager.h"
#include "entity_parser.h"
#include "input_manager.h"
#include "debug_manager.h"

TFont                   font;
CRenderTexture          rt0;

//--------------------------------------------------------------------------------------
bool CApplication::onDeviceCreated() {
    utils::Init(); 
    initComponents();

    bool is_ok = true;
    //Camera manager
    is_ok &= camera_manager.init();

    //Render manager
    is_ok = is_ok & render_manager.init(App.xres, App.yres);
    is_ok = is_ok & rt0.create("rt0", App.xres, App.yres, DXGI_FORMAT_R32G32B32A32_FLOAT, DXGI_FORMAT_UNKNOWN);

    //Tools manager
    is_ok = is_ok & anttweakbar_manager.init();

    font.create();
    font.camera = &camera_manager.free_camera;

    CShader::createSamplers();

    //Load the entities
    CEntityParser parser;
    is_ok = is_ok & parser.xmlParseFile("data/scene.xml");
    //is_ok = is_ok & parser.xmlParseFile("data/scene_small.xml");
    is_ok = is_ok & parser.xmlParseFile("data/lights.xml");
    return true;
}

//--------------------------------------------------------------------------------------
void CApplication::onDeviceDestroyed() {
    CShader::destroySamplers();

    font.destroy();

    DrawUtils::destroyDrawUtils();
    render_manager.destroy();
}

//--------------------------------------------------------------------------------------
void CApplication::onUpdate(float delta) {
    utils::Update();
    anttweakbar_manager.update(delta);

    InputManager.update(delta);

    debug_manager::Update(delta);
 
    camera_manager.update( delta );

    CCompManagersRegistry::get().updateAll(delta);
}



//--------------------------------------------------------------------------------------
void CApplication::onRender() {
    utils::profiler::graphics::Init();

    //Do render
    render_manager.render(rt0);
    
    //Render the result
    CTraceScoped t1("FinalImg");
    DrawUtils::drawTextured2D(0, 0, App.xres, App.yres, &rt0);

#ifndef NDEBUG
    PROFILE_BEGIN(DEBUG_RENDER);
#endif

  //Debug render
  renderDebug();

#ifndef NDEBUG
    PROFILE_END();
#endif

    if (debug_manager::IsDebug()){
        anttweakbar_manager.render();
    }
}

void CApplication::renderDebug(){

    render_manager.setDepthConfig(TZConfig::Z_TEST_ON_WRITE_OFF);
    if (debug_manager::IsDebugAABB()){
        renderDebugAABB();
    }

    if (debug_manager::IsDebugGrid()){
        renderDebugGrid();
    }

    if (debug_manager::IsDebugAxis()){
        renderDebugAxis();
    }

    render_manager.setDepthConfig(TZConfig::Z_DEFAULT);

    if (debug_manager::IsDebugWireFrame()){
        renderDebugWireframe();
    }

    if (debug_manager::IsDebugDeferred()){
        renderDebugDeferred();
    }

    if (debug_manager::IsDebugProfilerInfo())
    {
        font.size = 13;
        font.print( 0, 100, debug_manager::GetProfilerInfo().c_str());
    }
}

void CApplication::renderDebugDeferred() {
    render_manager.setDepthConfig(Z_DISABLE_ALL);
    DrawUtils::activateFullScreenOrthoCamera();

    string str_frame_name = "";
    switch (debug_manager::GetDebugFrameID())
    {
        case RT_ALBEDO:
            str_frame_name = "ALBEDO";
            break;
        case RT_NORMAL:
            str_frame_name = "NORMAL";
            break;
        case RT_SPECULAR:
            str_frame_name = "SPECULAR";
            break;
        case RT_DISPLACEMENT:
            str_frame_name = "DISPLACEMENT";
            break;
        case RT_REFLECTION:
            str_frame_name = "REFLECTION";
            break;
        case RT_ZBUFFER:
            str_frame_name = "ZBUFFER";
            break;
        case RT_ACC_LIGHT:
            str_frame_name = "ACC LIGHT";
            break;
    }

    //Render debug frame
    DrawUtils::drawTextured2D(0.f, 0.f, App.xres, App.yres, &render_manager.rts[ debug_manager::GetDebugFrameID()]);

    //Other render-to-textures
    float w = (float)App.xres / 5;
    float h = (float)App.yres / 5;
    DrawUtils::drawTextured2D(0, 0 * h, w, h, &render_manager.rts[RT_ALBEDO]);
    DrawUtils::drawTextured2D(0, 1 * h, w, h, &render_manager.rts[RT_NORMAL]);
    DrawUtils::drawTextured2D(0, 2 * h, w, h, &render_manager.rts[RT_ZBUFFER]);
    DrawUtils::drawTextured2D(0, 3 * h, w, h, &render_manager.rts[RT_ACC_LIGHT]);
    DrawUtils::drawTextured2D(0, 4 * h, w, h, &render_manager.rts[RT_REFLECTION]);
}


void CApplication::renderDebugGrid(){
    DrawUtils::activateCamera(camera_manager.free_camera);
    CRenderTechnique tech_solid = *((CRenderTechnique *)render_techniques_manager.getByName("solid"));
    tech_solid.activate();

    DrawUtils::setWorldMatrix(XMMatrixScaling(10., 10., 10.));

    //Render the grid
    DrawUtils::drawGrid();
}

void CApplication::renderDebugAxis(){
    DrawUtils::activateCamera(camera_manager.free_camera);
    CRenderTechnique tech_solid = *((CRenderTechnique *)render_techniques_manager.getByName("solid"));
    tech_solid.activate();

    CCompManager<CRenderable> &rm = *getCompManager< CRenderable >();
    for (size_t i = 0; i < rm.size(); ++i) {
        CRenderable *r = rm.getByIndex(i);
        DrawUtils::setWorldMatrix(r->getOwner()->get< CTransform >()->getWorld());
        DrawUtils::drawAxis();
    }
}

void CApplication::renderDebugAABB(){
    DrawUtils::activateCamera(camera_manager.free_camera);
    CRenderTechnique tech_solid = *((CRenderTechnique *)render_techniques_manager.getByName("solid"));
    tech_solid.activate();

    //Render the aabb
    render_manager.calculateSceneAABB();

    //Render the camera rustum
    DrawUtils::drawCameraFrustum(camera_manager.light_camera);
    DrawUtils::setWorldMatrix(XMMatrixIdentity());
}


void CApplication::renderDebugWireframe() {

    //Change the raster state
    ID3D11RasterizerState *prev_raster_state;
    App.immediateContext->RSGetState(&prev_raster_state);
    render_manager.setRasterizerStates(RASTER_FRONT_WIREFRAME);
    

    const CRenderTechnique *technique = render_techniques_manager.getByName("deferred.gbuffer");
    technique->activate();
    DrawUtils::activateCamera( camera_manager.free_camera );
    render_manager.renderSolidObjects();


    //Restore the raster state
    App.immediateContext->RSSetState(prev_raster_state);



    // Render debug objects
   /* tech_solid.activate();
    DrawUtils::drawGrid();


    frustum.initFromViewProjection(camera.getViewProjection());
    if (frustum.isVisible(XMVectorSet(0, 0, 0, 1)))
        font.printf(10, 30, "Inside!");
        */
    //CCompManager<CRenderable> &rm = *getCompManager< CRenderable >();
    //for (size_t i = 0; i < rm.size(); ++i) {
    //  CRenderable *r = rm.getByIndex(i);
    //  CAABB aabb = r->getAABB();
    //  if (frustum.isVisible(aabb))
    //    DrawUtils::draw(aabb, XMFLOAT4(0, 1, 0, 1));
    //  else
    //    DrawUtils::draw(aabb, XMFLOAT4(0.5f, 0.5f, 0.5f, 1));
    //}

    //tech_solid.activate();
    //DrawUtils::setWorldMatrix(XMMatrixIdentity());
    //CCompManagersRegistry::get().renderDebug3DAll();

    //float y = 10.0f;
    //y += font.printf(10, y, "Mouse Delta: %d %d", InputManager.getMouse().dx, InputManager.getMouse().dy);

    //DrawUtils::drawCameraFrustum(camera_user);
    

}
