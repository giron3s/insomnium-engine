#ifndef INC_MCV_PLATFORM_H_
#define INC_MCV_PLATFORM_H_

#define _WINERROR_
#define MAKE_HRESULT(sev,fac,code) \
  ((HRESULT)(((unsigned long)(sev) << 31) | ((unsigned long)(fac) << 16) | ((unsigned long)(code))))
#define FAILED(hr) (((HRESULT)(hr)) < 0)
#define SUCCEEDED(hr) (((HRESULT)(hr)) >= 0)
#define S_OK                                   ((HRESULT)0L)

#define SAFE_RELEASE(x) if(x) x->Release( ), x = NULL

#define uint16 unsigned short int
#define uint32 unsigned int

#define int16 short int
#define int32 int

#define NOMINMAX
#include <windows.h>
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dcompiler.h>
#include <xnamath.h>
#include <d3d9.h>         // D3DPERF_*
#include <algorithm>

#include <cstdio>
#include <cstring>
#include <vector>
#include <string>
#include <cassert>
#include <iostream>     // std::cout
#include <limits>       // std::numeric_limits
#include "app.h"
#include <iostream>
#include <fstream>
#include <sstream>

//TOOLS
#include <AntTweakBar.h>
#include "Shiny.h"

using namespace std;

#define setDbgName(obj,name) (obj)->SetPrivateData(WKPDID_D3DDebugObjectName, (UINT) strlen( name ), name );

struct CDbgTrace {
  wchar_t wname[64];
  CDbgTrace(const char *name) {
    setName(name);
  }
  inline void setName(const char *name) {
    mbstowcs(wname, name, 64);
  }
  inline void begin() {
    D3DPERF_BeginEvent(D3DCOLOR_XRGB(255, 0, 255), wname);
  }
  inline void end() {
    D3DPERF_EndEvent();
  }
};


struct CTraceScoped : public CDbgTrace {
  CTraceScoped(const char *name) : CDbgTrace( name ) {
    begin();
  }
  ~CTraceScoped() {
    end();
  }
};


#endif
