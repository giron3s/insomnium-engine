#ifndef _COMPOSITE_
#define _COMPOSITE_

#include "Effect.h"
#include "shader.h"
#include "render_texture.h"
class CComposite : public CEffect{
private:
    CRenderTechnique *tech;

public:
    bool init();
    void render(CRenderTexture &target);
    void destoy();


};
#endif