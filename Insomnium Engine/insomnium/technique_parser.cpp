#include "mcv_platform.h"
#include "utils.h"
#include "technique_parser.h"

#include "shader.h"
#include "render_constants.h"
#include "vertex_declarations.h"

void CTechniqueParser::onStartElement(const std::string &elem, MKeyValue &atts) {
    if (elem == "techniques") {
        techniques_loaded.clear();
    }
    else if (elem == "technique") {
        assert(curr_technique == NULL);
        curr_technique = new CRenderTechnique;

        //Technique name
        string str = atts.getString("name", "none").c_str();
        curr_technique_name = new char[str.size() + 1];
        std::copy(str.begin(), str.end(), curr_technique_name);
        curr_technique_name[str.size()] = '\0'; // don't forget the terminating 0

        curr_blend_config = atts.getInt("blend_config", 0);
        curr_tech_group = atts.getInt("group", 0);
        string vtx_dcl = atts.getString("vertex_declaration", "none");
        
        if (!std::strcmp(vtx_dcl.c_str(), "vtx_dcl_solid")) {
            curr_vertex_declaration = &vtx_dcl_solid;
        }
        else if (!std::strcmp(vtx_dcl.c_str(), "vtx_dcl_textured")) {
            curr_vertex_declaration = &vtx_dcl_textured;
        }
        else if (!std::strcmp(vtx_dcl.c_str(), "vtx_dcl_textured2"))	{
            curr_vertex_declaration = &vtx_dcl_textured2;
        }
        else if (!std::strcmp(vtx_dcl.c_str(), "vtx_dcl_skin")){
            curr_vertex_declaration = &vtx_dcl_skin;
        }
        else if (!std::strcmp(vtx_dcl.c_str(), "vtx_dcl_normalmap")) {
            curr_vertex_declaration = &vtx_dcl_normalmap;
        }
        else if (!std::strcmp(vtx_dcl.c_str(), "vtx_dcl_pos")){
            curr_vertex_declaration = &vtx_dcl_pos;
        }
        else{
            utils::logger::fatal("Invalid vertex declaration!!");
        }
    
    }
    else if (elem == "shader"){
        assert(curr_technique != NULL);

        curr_shader_name = PATH_SHADERS + atts.getString("file", "none");
        curr_vs_function = atts.getString("vs", "none");
        curr_ps_function = atts.getString("ps", "none");
    }
    else {
        utils::logger::dbg("Unknown technique tag %s\n", elem.c_str());
    }
}
void CTechniqueParser::onEndElement(const std::string &elem) {
    if (elem == "technique") {
        assert(curr_technique != NULL);

        //Regist the new technique
        curr_technique->create( curr_technique_name, curr_shader_name.c_str(), curr_vs_function.c_str(),
                                curr_ps_function.c_str(), curr_vertex_declaration,
                                static_cast<TRenderTechniqueGroup>(curr_tech_group), 
                                static_cast<TBlendConfig>(curr_blend_config));

        techniques_loaded.push_back(curr_technique);
        curr_technique = NULL;
    }
    else {
    }
}
