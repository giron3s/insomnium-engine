#ifndef INC_MESH_H_
#define INC_MESH_H_

#include "mcv_platform.h"
#include "aabb.h"
#include "render_constants.h"
class CVertexDeclaration;
class CDataProvider;

class CRenderMesh {

public:

    CRenderMesh();
    void destroy(); 

    enum ePrimitiveType {
        POINT_LIST
        , LINE_LIST
        , TRIANGLE_LIST
        , TRIANGLE_STRIP
    };

    typedef unsigned short TIndex;

    // Create a mesh given a vertex type
    template< class TVertex >
    bool createMesh(const TVertex* new_vertexs
        , unsigned       new_nvertexs
        , const TIndex * new_indices
        , unsigned       new_nindices
        , ePrimitiveType new_type
        , bool           is_updateable = false
        )
    {
        return createRawMesh(new_vertexs
            , new_nvertexs
            , sizeof(TVertex)
            , new_indices
            , new_nindices
            , new_type
            , TVertex::getDecl()
            , is_updateable
            );
    }

    // Implemented in .cpp
    bool createRawMesh( const void *  new_vertexs, unsigned new_nvertexs, unsigned new_nbytes_per_vertex
                        , const TIndex * new_indices, unsigned new_nindices, ePrimitiveType new_type
                        , const CVertexDeclaration *avertex_decl, bool is_updateable );

    void render() const;
    bool load(CDataProvider &dp);
    unsigned getGroupsCount() const { return ngroups_used; }
    void renderGroup(unsigned group_idx) const;

    bool updateFromCPU(const void *data, unsigned nbytes);
    unsigned getNVertexs() const { return nvertexs; }
    const CVertexDeclaration* GetVertexDeclaration()        { return mVertexDeclaration; }
    const CVertexDeclaration* GetVertexDeclaration() const  { return mVertexDeclaration; }
    const TRenderTechniqueGroup GetTechniqueGroup()         { return mRenderGroup; }
    const TRenderTechniqueGroup GetTechniqueGroup() const   { return mRenderGroup; }
    const CAABB &getAABB() const { return mAABB; }      //Return AABB in local space

private:
    // DirectX11 buffers
    ID3D11Buffer*             vb;
    ID3D11Buffer*             ib;
    D3D_PRIMITIVE_TOPOLOGY    primitive_type;
    const CVertexDeclaration* mVertexDeclaration;

    unsigned                  nvertexs;
    unsigned                  nindices;
    unsigned                  bytes_per_vertex;
    unsigned                  ngroups_used;
    TRenderTechniqueGroup     mRenderGroup;

    CAABB                     mAABB;

    // Declarado private para que no se pueda usar
    CRenderMesh(const CRenderMesh &);

    static const unsigned max_groups_supported = 16;
    struct TGroup {
        unsigned first_index;
        unsigned nindices;
    };
    TGroup groups[max_groups_supported];

    // -----------------------
    struct THeader {
        unsigned magic_begin;
        unsigned version;
        unsigned nvertexs;
        unsigned nfaces;
        unsigned nindices;
        unsigned vertex_type;
        unsigned bytes_per_vertex;
        unsigned bytes_per_index;
        unsigned render_group;
        float    aabb_center[4];
        float    aabb_half_size[4];
        

        unsigned magic_end;

        static const unsigned valid_magic = 0x33887755;
        static const unsigned current_version = 4;

        bool isValid() const {
            return magic_begin == valid_magic
                && magic_end == valid_magic
                && version == current_version;
        }
    };

};


#endif