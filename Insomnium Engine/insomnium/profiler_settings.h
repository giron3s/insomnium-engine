#include "mcv_platform.h"
#include "utils.h"
#include "debug_manager.h"


void TW_CALL getFPS(void *value, void *clientData){
    *static_cast<int *>(value) = utils::profiler::fps::GetFPS();
}

void TW_CALL getProfilerInfo(void *value, void *clientData){
    *static_cast<bool *>(value) = debug_manager::IsDebugProfilerInfo();
}

void TW_CALL setProfilerInfo(const void *value, void *clientData){
    debug_manager::SetDebugProfilerInfo( *static_cast<const bool *>(value) );
}

void TW_CALL getDrawCalls(void *value, void *clientData){
    *static_cast<int *>(value) = utils::profiler::graphics::mDrawCalls;
}

void TW_CALL getRenderedObjectsCount(void *value, void *clientData){
    *static_cast<int *>(value) = utils::profiler::graphics::mRenderedObjectsCount;
}

void TW_CALL getCulledObjectsCount(void *value, void *clientData){
    *static_cast<int *>(value) = utils::profiler::graphics::mCulledObjectsCount;
}

void TW_CALL getVertexCount(void *value, void *clientData){
    *static_cast<int *>(value) = utils::profiler::graphics::mVertexCount;
}

void TW_CALL getTextureSize(void *value, void *clientData){
    *static_cast<float *>(value) = utils::profiler::graphics::mTextureSize;
}

void TW_CALL getTextureCount(void *value, void *clientData){
    *static_cast<int *>(value) =  utils::profiler::graphics::mTextureCount;
}

void TW_CALL getRenderTextureSize(void *value, void *clientData){
    *static_cast<float *>(value) = utils::profiler::graphics::mRenderTextureSize;
}

void TW_CALL getRenderTextureCount(void *value, void *clientData){
    *static_cast<int *>(value) = utils::profiler::graphics::mRenderTextureCount;
}


// Callback function to create a bar with a given title
void TW_CALL CreateProfilerBarCB(void *clientData){
    TwBar *bar = TwNewBar("Profiler");
    TwDefine("label='Profiler TweakBar' position='16 16' size='260 600'");

    //Profiler vars
    TwAddVarCB(bar, "FPS",                  TW_TYPE_INT32,    NULL,            getFPS,                 NULL,   " label='FPS' ");
#ifndef NDEBUG
    TwAddVarCB(bar, "Profiler Info",        TW_TYPE_BOOL8,    setProfilerInfo, getProfilerInfo,        NULL,   " label='Profiler info' ");
#endif
    TwAddVarCB(bar, "Drawcalls",            TW_TYPE_INT32,    NULL,            getDrawCalls,           NULL,   " label='Drawcalls' ");
    TwAddVarCB(bar, "Culled Objects",       TW_TYPE_INT32,    NULL,            getCulledObjectsCount,  NULL,   " label='Culled Objects Count' ");
    TwAddVarCB(bar, "Rendered Objects",     TW_TYPE_INT32,    NULL,            getRenderedObjectsCount,NULL,   " label='Rendered Objects Count' ");
    TwAddVarCB(bar, "Vertex Count",         TW_TYPE_INT32,    NULL,            getVertexCount,         NULL,   " label='Vertex Count' ");
                                                              
    TwAddVarCB(bar, "Texture Size",         TW_TYPE_FLOAT,    NULL,            getTextureSize,         NULL,   " label='Texture Size(MB)' ");
    TwAddVarCB(bar, "Texture Count",        TW_TYPE_INT32,    NULL,            getTextureCount,        NULL,   " label='Texture Count'");
    TwAddVarCB(bar, "Render Texture Size",  TW_TYPE_FLOAT,    NULL,            getRenderTextureSize,   NULL,   " label='Render Texture Size(MB)' ");
    TwAddVarCB(bar, "Render Texture Count", TW_TYPE_INT32,    NULL,            getRenderTextureCount,  NULL,   " label='Render Texture Count'");
}