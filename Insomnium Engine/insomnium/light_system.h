#ifndef _LIGHT_SYSTEM_H_
#define _LIGHT_SYSTEM_H_

#include "mcv_platform.h"
#include "render_texture.h"
#include "shader.h"

#include "components\comp_pointlight.h"
#include "components\comp_skylight.h"
#include "components\comp_dirlight.h"
class CCamera;

class CLightSystem{

private:
    unsigned  seq_unique_id;
    CRenderTechnique *tech_pointlight;
    CRenderTechnique *tech_directionallight;

    void renderDirectionalLight();
    void renderPointLights();

public:
    CSkylight *skylight;
    vector<CDirlight *> dirlights;
    vector<CPointlight *> pointlights;

    unsigned     registerPointlightToRender(CPointlight *pointlight);
    unsigned     registerDirlightToRender(CDirlight   *dirlight);
    unsigned     registerSkylightToRender(CSkylight   *skylight);
    void         unregisterFromRender(unsigned aunique_id);

    bool init();
    void update(float delta);
    void render();
    void destroy();

    void exportEntities();
};
#endif