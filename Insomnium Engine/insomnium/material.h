#ifndef INC_MATERIAL_H_
#define INC_MATERIAL_H_

#include "mcv_platform.h"
#include "texture.h"
#include "utils.h"

class CMaterial {

public:
    string                 mName;
    //const CRenderTechnique *tech;
    const CTexture         *diffuse;
    const CTexture         *normal;
    const CTexture         *lightmap;
    const CTexture         *specular;
    const CTexture         *displacement;

    bool                    cast_shadows;

    //----------------------------------------------------------
    // Constructur of material
    //----------------------------------------------------------
    CMaterial()                  : mName(""), diffuse(NULL), normal(NULL), lightmap(NULL), specular(NULL), displacement(NULL), cast_shadows(true) {}
    CMaterial(const char *aName) : diffuse(NULL), normal(NULL), lightmap(NULL), specular(NULL), displacement(NULL), cast_shadows(true)
    {
        mName = aName;
    }

    //----------------------------------------------------------
    // Activate the material
    //----------------------------------------------------------
    void activate() const {
        if (diffuse)
        {
            utils::profiler::graphics::mTextureCount++;
            utils::profiler::graphics::mTextureSize += diffuse->GetMemSize();
            App.immediateContext->PSSetShaderResources(0, 1, &diffuse->mResourceView);
        }
        if (lightmap)
        {
            utils::profiler::graphics::mTextureCount++;
            utils::profiler::graphics::mTextureSize += lightmap->GetMemSize();
            App.immediateContext->PSSetShaderResources(1, 1, &lightmap->mResourceView);
        }
        if (normal)
        {
            utils::profiler::graphics::mTextureCount++;
            utils::profiler::graphics::mTextureSize += normal->GetMemSize();
            App.immediateContext->PSSetShaderResources(2, 1, &normal->mResourceView);
        }
        if (specular)
        {
            utils::profiler::graphics::mTextureCount++;
            utils::profiler::graphics::mTextureSize += specular->GetMemSize();
            App.immediateContext->PSSetShaderResources(3, 1, &specular->mResourceView);
        }
        if (displacement)
        {
            utils::profiler::graphics::mTextureCount++;
            utils::profiler::graphics::mTextureSize += displacement->GetMemSize();
            App.immediateContext->PSSetShaderResources(4, 1, &displacement->mResourceView);
        }
    }
};

#endif