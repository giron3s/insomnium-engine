#ifndef _POSTPROCESS_SYSTEM_
#define _POSTPROCESS_SYSTEM_

#include "blur.h"
#include "composite.h"
#include "render_texture.h"

class CPostProcessSystem{
private:
    CBlur blur;
    CComposite composite;

public:
    bool init();
    void render(CRenderTexture &target);
    void update(float delta);
    void destroy();
};
#endif