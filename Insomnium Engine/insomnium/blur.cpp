#include "mcv_platform.h"
#include "blur.h"
#include "vertex_declarations.h"
#include "draw_utils.h"
#include "render_manager.h"
#include "camera.h"

#include "data/shaders/constants/buffer_blur.h"
#include "render_constants.h"

CRenderTechnique tech_blur;

CBlur::CBlur(){
    setEnable(false);
}

bool CBlur::init(int axres, int ayres) {
    xres = axres;
    yres = ayres;

    tech_blur.create("blur", string(PATH_SHADERS + "postprocess.fx").c_str(), "VS_Blur", "PS_Blur", &vtx_dcl_textured, TECH_POST_PROCESS);

    bool is_ok = true;
    is_ok &= rt_hor.create("blur.horizontal", xres, yres / 2, DXGI_FORMAT_B8G8R8A8_UNORM, DXGI_FORMAT_UNKNOWN);
    is_ok &= rt_blured.create("blur.blured", xres / 2, yres / 2, DXGI_FORMAT_B8G8R8A8_UNORM, DXGI_FORMAT_UNKNOWN);
    assert(is_ok);

    return is_ok;
}

void CBlur::doBlur(CTexture &input) {
    CTraceScoped t0("renderComposite");

    render_manager.setDepthConfig(Z_DISABLE_ALL);

    ConstantBufferBlur bp;

    rt_hor.begin();
    DrawUtils::activateFullScreenOrthoCamera();
    bp.blur_delta_uv.x = 0.f;
    bp.blur_delta_uv.y = 5.f / (float)yres;
    tech_blur.setParam("ConstantBufferBlur", bp);
    DrawUtils::drawTextured2D(0, 0, App.xres, App.yres, &input, &tech_blur);
    rt_hor.end();

    rt_blured.begin();
    DrawUtils::activateFullScreenOrthoCamera();
    bp.blur_delta_uv.x = 5.f / (float)xres;
    bp.blur_delta_uv.y = 0.f;
    tech_blur.setParam("ConstantBufferBlur", bp);
    DrawUtils::drawTextured2D(0, 0, App.xres, App.yres, &rt_hor, &tech_blur);
    rt_blured.end();

}


void CBlur::destroy() {
    rt_hor.destroy();
    rt_blured.destroy();
}