#ifndef INC_IO_STATUS_H_
#define INC_IO_STATUS_H_

class CInputManager {

public:

    struct TButton {
        bool  is_pressed;
        bool  was_pressed;
        float time_pressed;
        float time_released;
        int   key;
        TButton() : is_pressed(false)
            , was_pressed(false)
            , time_pressed(0.f)
            , time_released(0.f)
        { }
        void setPressed(bool how, float elapsed);
    };

    enum TButtonID {
          W_KEY
        , A_KEY
        , S_KEY
        , D_KEY
        , Q_KEY
        , E_KEY
        , SPACE_KEY
        , ENTER_KEY
        , V_KEY
        , R_KEY
        , T_KEY
        , Z_KEY
        , H_KEY
        , F_KEY

        //NUMERIC KEYS
        , ONE_KEY
        , TWO_KEY
        , THREE_KEY
        , FIVE_KEY
        , EIGHT_KEY
        , NINE_KEY
        , SHIFT_KEY
        , CTRL_KEY
        , TAB_KEY

        //DBG KEYS
        , F1_KEY
        , F2_KEY
        , F3_KEY
        , F4_KEY
        , F5_KEY
        , F6_KEY
        , F7_KEY
        , F8_KEY
        , F9_KEY
        , F10_KEY
        , F11_KEY
        , F12_KEY

        //MOUSE
        , MOUSE_LEFT
        , MOUSE_RIGHT
        , MOUSE_MIDDLE
        , MOUSE_WHEEL_UP
        , MOUSE_WHEEL_DOWN
        , BUTTONS_COUNT
    };

    bool isPressed(TButtonID button_id) const { return buttons[button_id].is_pressed; }
    bool isReleased(TButtonID button_id) const { return !buttons[button_id].is_pressed; }
    bool becomesPressed(TButtonID button_id) const {
        return buttons[button_id].is_pressed && !buttons[button_id].was_pressed;
    }
    bool becomesReleased(TButtonID button_id) const {
        return !buttons[button_id].is_pressed && buttons[button_id].was_pressed;
    }
    float getTimePressed(TButtonID button_id) const { return buttons[button_id].time_pressed; }
    float getTimeReleased(TButtonID button_id) const { return buttons[button_id].time_released; }

    // ---------------------------
    // Analog
    enum TJoystickID {
        LEFT_ANALOG
        , RIGHT_ANALOG
        , JOYSTICKS_COUNT
    };

    struct TJoystick {
        float x, y;
        TJoystick() : x(0.f), y(0.f) { }
    };

    TJoystick getJoystick(TJoystickID joy_id) const;

    // ----------------------
    struct TMouse {
        int   screen_x;     // as given by windows in client space
        int   screen_y;
        float normalized_x;       // Vars normalized to screen -1..1
        float normalized_y;
        int   dx;
        int   dy;

        bool  is_left_pressed;
        bool  is_middle_pressed;
        bool  is_right_pressed;
        bool  is_wheel_up_pressed;
        bool  is_wheel_down_pressed;

        int   prev_x;
        int   prev_y;
        void update();
    };
    const TMouse &getMouse() const { return mouse; }

    void notifyMouseButtonState(TButtonID button, bool new_state);

    CInputManager();
    void update(float elapsed);

protected:
    TButton   buttons[BUTTONS_COUNT];
    TJoystick joys[JOYSTICKS_COUNT];
    TMouse    mouse;
};

extern CInputManager InputManager;

#endif