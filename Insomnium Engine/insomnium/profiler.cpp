#include "mcv_platform.h"
#include "profiler.h"


namespace utils{
    namespace profiler {
        namespace fps {

            int           m_fps;
            int           m_count;
            unsigned long m_startTime;

            void Init()
            {
                m_fps       = 0;
                m_count     = 0;
                m_startTime = GetTickCount();
            }

            void Update()
            {
                m_count++;
                if (GetTickCount() >= (m_startTime + 1000)){
                    m_fps       = m_count;
                    m_count     = 0;
                    m_startTime = GetTickCount();
                }
            }

            //Return with the frame rate
            const int GetFPS()
            {
                return m_fps;
            }
        }
    }
};

namespace utils{
    namespace profiler {
        namespace graphics{

            uint32 mDrawCalls;              //N� of the drawcalls
            uint32 mVertexCount;            //N� of the vertex rendered

            uint32 mRenderedObjectsCount;   //N� of the renderes objects
            uint32 mCulledObjectsCount;     //N� of the culled object
            float  mTextureSize;            //Binded textures in MB
            uint32 mTextureCount;           //N� of the texture binded
            float  mRenderTextureSize;      //Binded render textures in MB
            float  mRenderTextureCount;     //N� of the render texture binded

            float  mParamSize;
            uint32 mParamCount;


            void Init()
            {
                mTextureSize            = 0;
                mTextureCount           = 0;
                mRenderTextureSize      = 0;
                mRenderTextureCount     = 0;

                mParamSize              = 0;
                mParamCount             = 0;
                mVertexCount            = 0; 
                mDrawCalls              = 0;

                mRenderedObjectsCount   = 0;
                mCulledObjectsCount     = 0;
            }
        }
    }
};