#ifndef INC_RENDER_TEXTURE_H_
#define INC_RENDER_TEXTURE_H_

#include "texture.h"

class CRenderTexture : public CTexture {

  ID3D11RenderTargetView*   target_view;
  int xres;
  int yres;
  char      name[64];
  CDbgTrace trace;

  DXGI_FORMAT               color_fmt;
  DXGI_FORMAT               depth_fmt;
  D3D11_VIEWPORT            view_port;

  // ZInformation
  CTexture                  ztexture;
  ID3D11DepthStencilView*   depth_stencil_view;

  void setResolution(int w, int h);
  bool createDepthStencil(bool bind_shader_resource = true);
  bool createColorBuffer(const char *name);
public:

  CRenderTexture( );
  bool create(const char *name, int xres, int height, DXGI_FORMAT color_fmt, DXGI_FORMAT depth_fmt );
  void destroy( );
  bool isValid() const;
  void clear(XMFLOAT4 color);
  void begin();
  void end();
  void beginWithZBuffer(ID3D11DepthStencilView *depth_view);

  void clearZBuffer();
  CTexture& getZTexture() {
    return ztexture;
  }
  ID3D11DepthStencilView* getDepthStencilView();

  // 
  void activateViewport();
  ID3D11RenderTargetView* getTargetView() {
    return target_view;
  }
};



#endif
