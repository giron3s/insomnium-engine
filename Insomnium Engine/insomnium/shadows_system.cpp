#include "mcv_platform.h"
#include "shadows_system.h"
#include "draw_utils.h"
#include "numeric.h"
#include "vertex_declarations.h"

#include "render_manager.h"



CRenderTechnique tech_shadow_gen;

CShadowsSystem::CShadowsSystem() : sample_state(NULL), rasterize_state(NULL){ }

bool CShadowsSystem::init(int dimension) {

    this->dimension = dimension;

    // La declaration se puede cambiar si quereis optimizar
    tech_shadow_gen.create("shadow_gen", string(PATH_SHADERS + "shadows.fx").c_str(), "VS", NULL, &vtx_dcl_normalmap, TRenderTechniqueGroup::TECH_SHADOWS);

    // PCF sampling
    D3D11_SAMPLER_DESC sampler_desc = {
        D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT,// D3D11_FILTER Filter;
        D3D11_TEXTURE_ADDRESS_BORDER, //D3D11_TEXTURE_ADDRESS_MODE AddressU;
        D3D11_TEXTURE_ADDRESS_BORDER, //D3D11_TEXTURE_ADDRESS_MODE AddressV;
        D3D11_TEXTURE_ADDRESS_BORDER, //D3D11_TEXTURE_ADDRESS_MODE AddressW;
        0,//FLOAT MipLODBias;
        0,//UINT MaxAnisotropy;
        D3D11_COMPARISON_LESS, //D3D11_COMPARISON_FUNC ComparisonFunc;
        0.0, 0.0, 0.0, 0.0,//FLOAT BorderColor[ 4 ];
        0,//FLOAT MinLOD;
        0//FLOAT MaxLOD;   
    };
    if (FAILED(App.d3dDevice->CreateSamplerState(&sampler_desc, &sample_state)))
        return false;

    // Depth bias options when rendering the shadows
    D3D11_RASTERIZER_DESC raster_desc = {
        D3D11_FILL_SOLID, // D3D11_FILL_MODE FillMode;
        D3D11_CULL_BACK,  // D3D11_CULL_MODE CullMode;
        FALSE,             // BOOL FrontCounterClockwise;
        13,               // INT DepthBias;
        0.0f,             // FLOAT DepthBiasClamp;
        2.0,              // FLOAT SlopeScaledDepthBias;
        TRUE,             // BOOL DepthClipEnable;
        FALSE,            // BOOL ScissorEnable;
        FALSE,            // BOOL MultisampleEnable;
        FALSE,            // BOOL AntialiasedLineEnable;
    };
    if (FAILED(App.d3dDevice->CreateRasterizerState(&raster_desc, &rasterize_state)))
        return false;

    //Create the render textures
    for (int i = 0; i < SHADOW_MAP_CASCADE_COUNT; i++){
        string str("CSM_%d", i);
        rt_zmap[i].create(str.c_str(), dimension, dimension, DXGI_FORMAT_UNKNOWN, DXGI_FORMAT_R32_TYPELESS);
    }
    return true;
}


void CShadowsSystem::uploadParamsShadowToGPU(CRenderTechnique &tech) {
    ConstantBufferCascadeShadow cb;
    cb.cascade_resolution = dimension;
    for (int i = 0; i < SHADOW_MAP_CASCADE_COUNT; i++){
        //View, projection params
        cb.cascade_view[i] = cascadeView[i];
        cb.cascade_projs[i] = cascadeProj[i];
        cb.cascade_view_projs[i] = cascadeView[i] * cascadeProj[i];
    }

    //Ztexture
    tech.setTexture("zMapCascade0", &getRT(0).getZTexture());
    tech.setTexture("zMapCascade1", &getRT(1).getZTexture());
    tech.setTexture("zMapCascade2", &getRT(2).getZTexture());

    bool is_ok = tech.setParam("ConstantBufferCascadeShadow", cb);
    //assert(is_ok);
}

void CShadowsSystem::uploadParamsLightToGPU(const XMMATRIX view, const XMMATRIX proj) {
    ConstantBufferCamera cb;
    cb.View = view;
    cb.Projection = proj;
    cb.ViewProjection = view * proj;
    App.immediateContext->UpdateSubresource(g_pConstantBufferCamera, 0, NULL, &cb, 0, 0);
}


void CShadowsSystem::generate(const CCamera &camera, const CCamera& light) {
    XMFLOAT3 lightDirection;
    XMStoreFloat3(&lightDirection, XMVector3Normalize( light.getTarget() - light.getPosition()) );

    //TODO: replace to the init, necessary publicate the camera
    //Camera frustum partititon
    cameraProj[0] = XMMatrixPerspectiveFovRH(camera.getFov(), camera.getAspectRatio(), camera.getZNear(), camera.getZFar() * 0.40f);
    cameraProj[1] = XMMatrixPerspectiveFovRH(camera.getFov(), camera.getAspectRatio(), camera.getZFar() * 0.40f, camera.getZFar() * 0.60f);
    cameraProj[2] = XMMatrixPerspectiveFovRH(camera.getFov(), camera.getAspectRatio(), camera.getZFar() * 0.60f, camera.getZFar());


    //Create the render textures
    for (size_t i = 0; i < SHADOW_MAP_CASCADE_COUNT; i++){

        XMFLOAT3 frustumCorner[8] = { XMFLOAT3(-1., 1., 0.),
            XMFLOAT3(1., 1., 0.),
            XMFLOAT3(1., -1., 0.),
            XMFLOAT3(-1., -1., 0.),

            XMFLOAT3(-1., 1., 1.),
            XMFLOAT3(1., 1., 1.),
            XMFLOAT3(1., -1., 1.),
            XMFLOAT3(-1., -1., 1.)
        };

        XMMATRIX cameraViewProj = camera.getView();;
        cameraViewProj *= cameraProj[i];
        XMVECTOR determinant;
        XMMATRIX invViewProj = XMMatrixInverse(&determinant, cameraViewProj);

        for (size_t i = 0; i < 8; i++){
            XMVECTOR aux = XMVector3TransformCoord(XMVectorSet(frustumCorner[i].x, frustumCorner[i].y, frustumCorner[i].z, 1), invViewProj);
            XMStoreFloat3(&frustumCorner[i], aux);
        }

        //Get the center of the split
        XMFLOAT3 frustumCenter(0.f, 0.f, 0.f);
        for (size_t i = 0; i < 8; i++){
            frustumCenter = utils::numeric::XMFloat3Sum(frustumCenter, frustumCorner[i]);
        }

        frustumCenter.x *= (1. / 8.);
        frustumCenter.y *= (1. / 8.);
        frustumCenter.z *= (1. / 8.);


        //2. Incorporating texel snapping (to avoid shadow shimmering or shadow crawling)
        XMFLOAT3 sub = utils::numeric::XMFloat3Sub(frustumCorner[0], frustumCorner[6]);
        float radius = XMVectorGetX(XMVector3Length(XMLoadFloat3(&sub))) / 2.f;

        //Get the texels per unit in the world
        float texelsPerUnit = dimension / (radius * 2.f);
        XMMATRIX scale = XMMatrixScaling(texelsPerUnit, texelsPerUnit, texelsPerUnit);

        //Lookat our light direction with scale !!!
        XMVECTOR zero = XMVectorSet(0.f, 0.f, 0.f, 0.f);
        XMVECTOR up = XMVectorSet(0.f, 1.f, 0.f, 0.f);
        XMMATRIX lookat, lookatInv;

        lookat = XMMatrixLookAtRH(zero, XMLoadFloat3(&lightDirection) * -1, up);
        lookat *= scale;
        lookatInv = XMMatrixInverse(&determinant, lookat);

        // we use this to move our frustum center in texel-sized increments, then get it back into its original space useing the inverse
        XMStoreFloat3(&frustumCenter, XMVector3TransformCoord(XMLoadFloat3(&frustumCenter), lookat));
        frustumCenter.x = (float)floor(frustumCenter.x);
        frustumCenter.y = (float)floor(frustumCenter.y);
        XMStoreFloat3(&frustumCenter, XMVector3TransformCoord(XMLoadFloat3(&frustumCenter), lookatInv));

        //vector 
        XMFLOAT3 aux;
        aux.x = lightDirection.x * radius * 2.f;
        aux.y = lightDirection.y * radius * 2.f;
        aux.z = lightDirection.z * radius * 2.f;
        XMFLOAT3 eye = utils::numeric::XMFloat3Sub(frustumCenter, aux);

        //Save the view and projection matrix
        cascadeView[i] = XMMatrixLookAtRH(XMLoadFloat3(&eye), XMLoadFloat3(&frustumCenter), up);
        cascadeProj[i] = XMMatrixOrthographicOffCenterRH(-radius, radius, -radius, radius, -radius * 6.f, radius * 6.);

        generate(cascadeView[i], cascadeProj[i], i);
    }
}

void CShadowsSystem::generate(const XMMATRIX view, const XMMATRIX proj, int cascade_i) {
    //Activar nuestro estado de rasterizacion
    ID3D11RasterizerState *prev_rs_state;
    App.immediateContext->RSGetState(&prev_rs_state);
    App.immediateContext->RSSetState(rasterize_state);

    uploadParamsLightToGPU(view, proj);
    rt_zmap[cascade_i].clearZBuffer();
    rt_zmap[cascade_i].begin();
        tech_shadow_gen.activate();
        render_manager.renderShadowCasters();
    rt_zmap[cascade_i].end();

    // Restaurar el raster state
    App.immediateContext->RSSetState(prev_rs_state);
    if (prev_rs_state) prev_rs_state->Release();

    App.immediateContext->PSSetSamplers(5, 1, &sample_state);
}



void CShadowsSystem::destroy() {
    for (int i = 0; i < SHADOW_MAP_CASCADE_COUNT; i++){
        rt_zmap[i].destroy();
    }

    if (sample_state)
        sample_state->Release(), sample_state = NULL;
}