#ifndef INC_UTILS_H_
#define INC_UTILS_H_

#include "logger.h"
#include "numeric.h"
#include "profiler.h"

#define LOG_MSG(LOG_TYPE, MSG) debug( LOG_TYPE << ": " << __FILE__  << "(@" __TIME__ << ")" << MSG )

#define LOG_ERROR(MSG)   LOG_MSG("ERROR", MSG)
#define LOG_WARNING(MSG) LOG_MSG("WARNING", MSG)
#define LOG_INFO(MSG)    LOG_MSG("INFO", MSG)

namespace utils{
    void Init();
    void Update();
}


// TODO remove the next functions
bool keyIsPressed( int key );
bool keyBecomesPressed( int key );
float randomFloat( float vmin, float vmax );
float unitRandom( );

struct CCronTimer {
  LARGE_INTEGER start;
  CCronTimer() {
    ::QueryPerformanceCounter(&start);
  }
  float elapsed() {
    LARGE_INTEGER now;
    ::QueryPerformanceCounter(&now);
    now.QuadPart -= start.QuadPart;

    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);
    float elapsed = (float)now.LowPart / (float)freq.LowPart;
    return elapsed;
  }
};


typedef unsigned char u8;

#endif
