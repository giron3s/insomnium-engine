#include "mcv_platform.h"
#include "vertex_declarations.h"

CVertexDeclaration::CVertexDeclaration( const D3D11_INPUT_ELEMENT_DESC *aelems,unsigned int n) 
: elems( aelems )
, nelems( n )
{ }

// Esta macro declara una vertex declaration con el nombre 'v'
// y REQUIERE que exista una variable de tipo D3D11_INPUT_ELEMENT_DESC 
// con el nombre {v}_elems
#define DEF_DECL(v) CVertexDeclaration v( v##_elems, ARRAYSIZE(v##_elems))

// ------------------------------------------------------
// Define the input layout
static D3D11_INPUT_ELEMENT_DESC vtx_dcl_solid_elems[] =
{
  { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
  { "COLOR",    0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
};
DEF_DECL( vtx_dcl_solid );

static D3D11_INPUT_ELEMENT_DESC vtx_dcl_pos_elems[] =
{
  { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
};
DEF_DECL( vtx_dcl_pos );

// Define the input layout
D3D11_INPUT_ELEMENT_DESC vtx_dcl_textured_elems[] =
{
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
};
DEF_DECL( vtx_dcl_textured );

// Define the input layout
D3D11_INPUT_ELEMENT_DESC vtx_dcl_textured2_elems[] =
{
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TEXCOORD", 1, DXGI_FORMAT_R32G32_FLOAT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
};
DEF_DECL( vtx_dcl_textured2 );


// Define the input layout
D3D11_INPUT_ELEMENT_DESC vtx_dcl_normalmap_elems[] =
{
  { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
  { "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
  { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
  { "TANGENT",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
};
DEF_DECL(vtx_dcl_normalmap);

// Define the input layout for skin vertexs
D3D11_INPUT_ELEMENT_DESC vtx_dcl_skin_elems[] =
{
    { "POSITION",     0, DXGI_FORMAT_R32G32B32_FLOAT,    0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "NORMAL",       0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TEXCOORD",     0, DXGI_FORMAT_R32G32_FLOAT,       0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "BLENDWEIGHTS", 0, DXGI_FORMAT_R8G8B8A8_UNORM,     0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "BLENDINDICES", 0, DXGI_FORMAT_R8G8B8A8_UINT,      0, 36, D3D11_INPUT_PER_VERTEX_DATA, 0 },
};
DEF_DECL( vtx_dcl_skin );
