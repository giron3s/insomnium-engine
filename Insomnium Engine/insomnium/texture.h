#ifndef INC_TEXTURE_H_
#define INC_TEXTURE_H_

class CTexture 
{

private:
    string                      mFileName;
    float                       mMemSize;
    D3D11_TEXTURE2D_DESC        mTextureDesc;

    void                        MemSize         ( );
    int                         GetFormatsize   (DXGI_FORMAT aFormat);
public:
    ID3D11ShaderResourceView    *mResourceView;

                                CTexture           ( );
    void                        Destroy            ( );
    bool                        Load               ( const char *filename );
    const char*                 GetFileName()      { return mFileName.c_str(); }
    const float                 GetMemSize()       { return mMemSize; }
    const float                 GetMemSize() const { return mMemSize; }
};

#endif
