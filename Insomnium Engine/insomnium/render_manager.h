#ifndef INC_RENDER_MANAGER_H_
#define INC_RENDER_MANAGER_H_

#include "render_constants.h"
#include "mcv_platform.h"
#include "render_texture.h"

#include "world_system.h"
#include "light_system.h"
#include "shadows_system.h"
#include "postprocess_system.h"

class CRenderTechnique;
class CMaterial;
class CRenderMesh;
class CRenderable;
class CEntity;

class CRenderManager {
public:
    struct TRenderKey {
        unsigned           unique_id;
        unsigned           sub_mesh;
        const CRenderMesh* mesh;
        const CMaterial*   mat;
        CEntity*           owner;
        CRenderTechnique*  gbuffer_tech;

        bool operator==(unsigned aid) const { return unique_id == aid; }
    };

private:
    unsigned  seq_unique_id;
    typedef std::vector< TRenderKey > VRenderKeys;
    VRenderKeys keys;

    ID3D11BlendState*        blend_cfs[BLEND_CONFIG_COUNT];
    ID3D11DepthStencilState* z_cfs[Z_CONFIG_COUNT];
    ID3D11RasterizerState*   raster_cfs[RASTER_CONFIG_COUNT];

public:

    CRenderTexture           rts[RT_COUNT];
    CWorldSystem             world_system;
    CLightSystem             light_system;
    CShadowsSystem           shadows_system;
    CPostProcessSystem       postprocess_system;

    CRenderManager();
    bool      createRasterizerStates();
    bool      createBlendingStates();
    bool      createDepthStencilStates();

    void      setDepthConfig(TZConfig config_id);
    void      setBlendConfig(TBlendConfig config_id);
    void      setRasterizerStates(TRasterizerConfig config_id);

    bool         init( int xres, int yres);
    void         render(CRenderTexture &target);
    //void         render(CRenderTechnique *tech, const CCamera &camera);
    void         destroy();

    void         renderShadowCasters();
    void         renderSolidObjects( );
    //void         renderSolidObjects2(CRenderTechnique *tech, const CCamera &camera, XMMATRIX reflection);

    unsigned     registerToRender(CRenderable *r);
    void         unregisterFromRender(unsigned aunique_id);
    void         calculateSceneAABB();
};

extern CRenderManager render_manager;

#endif
