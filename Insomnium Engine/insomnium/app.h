#ifndef INC_MCV_APPLICATION_H_
#define INC_MCV_APPLICATION_H_

class CCamera;

class CApplication {

public:
  int                     xres;
  int                     yres;
  //bool                    full_screen;
  bool                    has_focus;
  HWND                    hWnd;
  HINSTANCE               hInst;
  D3D_DRIVER_TYPE         driverType;
  D3D_FEATURE_LEVEL       featureLevel;
  ID3D11Device*           d3dDevice;
  ID3D11DeviceContext*    immediateContext;
  IDXGISwapChain*         swapChain;
  ID3D11RenderTargetView* renderTargetView;
  ID3D11DepthStencilView* depthStencilView;
  ID3D11Texture2D*        depthStencil;

  LARGE_INTEGER           prev_counter;

  CApplication();
  virtual ~CApplication() { }

  void loadConfig();
  bool createDevice();
  void destroyDevice();
  void mainLoop();
  
  void renderFrame();
  void updateFrame( );

  bool onDeviceCreated();
  void onUpdate( float delta );
  void onDeviceDestroyed();

  void onRender();

  void renderDebug();
  void renderDebugAABB();
  void renderDebugAxis();
  void renderDebugGrid();
  void renderDebugDeferred();
  void renderDebugWireframe();
};

extern CApplication App;

#endif
