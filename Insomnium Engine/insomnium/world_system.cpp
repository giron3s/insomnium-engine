#include "mcv_platform.h"
#include "world_system.h"
#include "vertex_declarations.h"
#include "draw_utils.h"
#include "render_manager.h"
#include "camera.h"
#include "entity_manager.h"
#include "components\comp_transform.h"
#include "components\comp_renderable.h"

#include "render_techniques_manager.h"
#include "render_constants.h"
#include "material.h"
#include "angular.h"

#include "camera_manager.h"


CWorldSystem world_system;

CWorldSystem::CWorldSystem(){}

bool CWorldSystem::init() {
    bool is_ok = true;

    //Load the techniques
    tech_gbuffer = (CRenderTechnique *)render_techniques_manager.getByName("deferred.gbuffer");
    //tech_reflection = (CRenderTechnique *)render_techniques_manager.getByName("reflection3");
    tech_composite = (CRenderTechnique *)render_techniques_manager.getByName("deferred.composite");

    //Bind the texture
    //is_ok &= tech_reflection->setTexture("txReflection", &render_manager.rts[RT_REFLECTION]);
    is_ok &= tech_composite->setTexture("txAccLight", &render_manager.rts[RT_ACC_LIGHT]);
    return is_ok;
}

void CWorldSystem::renderGBuffer() {
    CTraceScoped t0("G-Buffer");

    ID3D11DeviceContext* devcon = App.immediateContext;

    // Enable multiple render targets ( MRT )
    ID3D11RenderTargetView *targets[6] = {        render_manager.rts[RT_ALBEDO].getTargetView()
                                                , render_manager.rts[RT_NORMAL].getTargetView()
                                                , render_manager.rts[RT_SPECULAR].getTargetView()
                                                , render_manager.rts[RT_DISPLACEMENT].getTargetView()
                                                , render_manager.rts[RT_EXTRA].getTargetView()
                                                , render_manager.rts[RT_ZBUFFER].getTargetView() };
    devcon->OMSetRenderTargets(6, targets, App.depthStencilView);
    render_manager.rts[RT_ALBEDO].activateViewport();
    
    // Clear multiple render targets and ZBuffer
    render_manager.rts[RT_ALBEDO].clear(XMFLOAT4(0, 0, 0, 0));
    render_manager.rts[RT_NORMAL].clear(XMFLOAT4(0, 0, 0, 0));
    render_manager.rts[RT_SPECULAR].clear(XMFLOAT4(0, 0, 0, 0));
    render_manager.rts[RT_DISPLACEMENT].clear(XMFLOAT4(0, 0, 0, 0));
    render_manager.rts[RT_REFLECTION].clear(XMFLOAT4(0, 0, 0, 0));
    render_manager.rts[RT_EXTRA].clear(XMFLOAT4(0, 0, 0, 0));
    render_manager.rts[RT_ZBUFFER].clear(XMFLOAT4(1, 1, 1, 1));
    render_manager.rts[RT_ZBUFFER].clearZBuffer();

    render_manager.renderSolidObjects();
}


// ---------------------------------------------
void CWorldSystem::renderReflection(){
    //XMMATRIX reflection;
    ////Render the reflected object
    //{
    //    CTraceScoped t0("reflected");
    //    render_manager.setRasterizerStates(TRasterizerConfig::RASTER_CULL_CLOCKWISE);
    //
    //    CEntity *mirror_entity = entity_manager.get("Plane001");
    //    CTransform *mirror_transform = mirror_entity->get<CTransform>();
    //    reflection = mirror_transform->getReflection();
    //    CRenderTechnique *tech_normal = (CRenderTechnique *)render_techniques_manager.getByName("normalmap");
    //    tech_normal->activate();
    //
    //    render_manager.rts[RT_REFLECTION].begin();
    //        DrawUtils::activateCamera( camera_manager.free_camera);
    //        render_manager.renderSolidObjects2(tech_normal, camera_manager.free_camera, reflection);
    //    render_manager.rts[RT_REFLECTION].end();
    //
    //    //Restore default states
    //    App.immediateContext->RSSetState(0);
    //    App.immediateContext->OMSetDepthStencilState(0, 0);
    //}
    //
    ////Blend it with the floor surface
    //{
    //
    //    CTraceScoped t0("floor");
    //    //tech_reflection->activate();
    //    ID3D11DeviceContext* devcon = App.immediateContext;
    //
    //    // Enable multiple render targets ( MRT )
    //    ID3D11RenderTargetView *targets[6] = {    render_manager.rts[RT_ALBEDO].getTargetView()
    //                                            , render_manager.rts[RT_NORMAL].getTargetView()
    //                                            , render_manager.rts[RT_SPECULAR].getTargetView()
    //                                            , render_manager.rts[RT_DISPLACEMENT].getTargetView()
    //                                            , render_manager.rts[RT_EXTRA].getTargetView()
    //                                            , render_manager.rts[RT_ZBUFFER].getTargetView() };
    //    devcon->OMSetRenderTargets(6, targets, App.depthStencilView);
    //    render_manager.rts[RT_ALBEDO].activateViewport();
    //
    //
    //    //tech_reflection->activate();
    //    DrawUtils::activateCamera(camera_manager.free_camera);
    //    //DrawUtils::activateReflection(tech_reflection, reflection, camera_manager.free_camera);
    //    //render_manager.render(tech_reflection, camera_manager.free_camera);
    //}
}

// ---------------------------------------------
void CWorldSystem::render() {
    camera_manager.uploadCameraToGPU( &camera_manager.free_camera );

    renderGBuffer();
    //renderReflection();
}

void CWorldSystem::destroy() {

}
