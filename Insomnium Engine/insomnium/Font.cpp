#include "mcv_platform.h"
#include "font.h"
#include "camera.h"

#pragma comment(lib, "FW1FontWrapper.lib") 

TFont::TFont()
  : font(NULL)
  , FW1Factory(NULL)
  , color(0xffffffff)   // Text color default to white: 0xAaBbGgRr
  , size(20.f)
  , camera( NULL )
{}

void TFont::create() {
  assert(App.d3dDevice);
  assert(font == NULL);
  HRESULT hResult = FW1CreateFactory(FW1_VERSION, &FW1Factory);
  hResult = FW1Factory->CreateFontWrapper(App.d3dDevice, L"Lucida Console", &font);
}

void TFont::destroy() {
  font->Release();
  font = NULL;
  FW1Factory->Release();
  FW1Factory = NULL;
}

float TFont::print(float x, float y, const char *text) const {
  WCHAR utf16[2048];
  memset(utf16, 0x80, 2048 * 2);
  size_t n = mbstowcs(utf16, text, strlen(text));
  utf16[n] = 0x00;
  font->DrawString(
    App.immediateContext,
    utf16,
    size,
    x, y,
    color,
    FW1_RESTORESTATE// Flags (for example FW1_RESTORESTATE to keep context states unchanged)
    );
  return size;
}

float TFont::printf(float x, float y, const char *fmt, ... ) const {
  va_list args;
  va_start(args, fmt);

  char buf[ 2048 ];
  int n = vsnprintf( buf, sizeof( buf ), fmt, args );

  // Confirm the msg fits in the given buffer
  if( n < 0 )
    buf[ sizeof( buf )-1 ] = 0x00;
  
  return print( x, y, buf );
}

float TFont::print3D( XMVECTOR world_p3d, const char *text ) const {
  assert( camera );
  float x,y;
  if( camera->getScreenCoords( world_p3d, &x, &y ) )
    return print( x, y, text );
  return 0.f;
}


float TFont::printf3D(XMVECTOR world_p3d, const char *fmt, ... ) const {
  va_list args;
  va_start(args, fmt);

  char buf[ 2048 ];
  int n = vsnprintf( buf, sizeof( buf ), fmt, args );

  // Confirm the msg fits in the given buffer
  if( n < 0 )
    buf[ sizeof( buf )-1 ] = 0x00;

  return print3D( world_p3d, buf );
}