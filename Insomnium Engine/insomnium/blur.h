#ifndef INC_BLUR_H_
#define INC_BLUR_H_

#include "Effect.h"
#include "render_texture.h"
#include "shader.h"

class CBlur : public CEffect{
  
public:
  CRenderTexture         rt_hor;
  CRenderTexture         rt_blured;
  int                    xres, yres;

  CBlur();
  bool init(int xres, int yres);
  void destroy();
  void doBlur(CTexture &input);
};
#endif
