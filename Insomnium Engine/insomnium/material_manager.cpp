#include "mcv_platform.h"
#include "material_manager.h"
#include "texture_manager.h"
#include "utils.h"
#include "render_techniques_manager.h"

CMaterialManager material_manager;

template<>
bool initObjFromName(CMaterial &obj, const char *name) {
    utils::logger::fatal("Can't init a material from a name!!!");
    return false;
}

void CMaterialManager::onStartElement(const std::string &elem, MKeyValue &atts) {
    assert(elem == "material_def");
    string lName = atts.getString("name", "");
    assert(!lName.empty());

    // Material already registered. Skipping
    if (exists(lName.c_str()))
        return;

    // Material name duplicated!
    assert(!exists(lName.c_str()));

    CMaterial *m    = new CMaterial(lName.c_str());
    m->diffuse      = texture_manager.getByName(atts["diffuse"]);
    m->normal       = texture_manager.getByName(atts["normal"]);
    m->specular     = texture_manager.getByName(atts["specular"]);

    if (!atts.getString("displacement", "").empty()){
        m->displacement = texture_manager.getByName(atts["displacement"]);
    }
    m->cast_shadows = atts.getBool("cast_shadows", true);
    
    //Save the material
    objs[lName] = m;
}

