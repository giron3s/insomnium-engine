#ifndef _NUMERIC_H_
#define _NUMERIC_H_

namespace utils{
    namespace numeric {

        template <typename T>
        inline const T& Max(const T& a, const T& b) {
            return (a<b) ? b : a;     // or: return comp(a,b)?b:a; for version (2)
        }
        
        template <typename T>
        inline const T& Min(const T& a, const T& b) {
            return (a>b) ? b : a;     // or: return comp(a,b)?b:a; for version (2)
        }

        float    Clamp(float value, float min, float max); 
        float    Lerp(float from, float to, float time);

        XMFLOAT3 XMFloat3Sum(const XMFLOAT3 a, const XMFLOAT3 b);
        XMFLOAT3 XMFloat3Sub(const XMFLOAT3 a, const XMFLOAT3 b);
        XMFLOAT3 XMTransform(const XMVECTOR point, const XMMATRIX matrix);
    }
};
#endif
