#ifndef INC_ENTITY_CONTROLLERS_H_
#define INC_ENTITY_CONTROLLERS_H_

class CEntity;

class IEntityController {
public:
  virtual ~IEntityController( ) { }
  virtual void updateEntity( CEntity *e, float delta ) = 0;
};

class CWalkEntityController : public IEntityController {
  float linear_speed;   // in units/sec
  float angular_speed;  // in rads/sec
public:
  CWalkEntityController( );
  void updateEntity( CEntity *e, float delta );
};

class CAimToTargetEntityController : public IEntityController {
  float angular_speed;  // in rads/sec
public:
  CEntity *target;
  CAimToTargetEntityController( ) ;
  void updateEntity( CEntity *e, float delta );
};

class CFreeCameraController : public IEntityController {
  float angular_speed;
  float pan_speed;
  float zoom_speed;
public:
  CFreeCameraController( ) ;
  void updateEntity( CEntity *e, float delta );
};

#endif
