#ifndef INC_MESH_MANAGER_H_
#define INC_MESH_MANAGER_H_

//--------------------------------------------------------------------------------------
#include "generic_manager.h"
#include "data_provider.h"
#include "mesh.h"

class CMeshManager : public CObjManager<CRenderMesh> {
public:
  void registerNewMesh( CRenderMesh *new_mesh, const std::string &new_name );
};

extern CMeshManager mesh_manager;

#endif
