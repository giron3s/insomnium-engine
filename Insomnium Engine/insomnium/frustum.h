#ifndef _FRUSTRUM_PLANES_
#define _FRUSTRUM_PLANES_

#include "mcv_platform.h"

class CAABB;

static const int INSIDE = 1;
static const int OUTSIDE = 2;
static const int INTERSECT = 3;

class CFrustum{

private:
    enum TPlane {
        PLANE_LEFT
        , PLANE_RIGHT
        , PLANE_UP
        , PLANE_DOWN
        , PLANE_NEAR
        , PLANE_FAR
        , PLANES_COUNT
    };

    XMVECTOR planes[PLANES_COUNT];

    float Fast_fabsf(float f);
    bool  SegmentPlaneIntersection(XMVECTOR plane, XMVECTOR segA, XMVECTOR segB, XMVECTOR &interPoint);

public:
    CFrustum(){ }

    void Init(XMMATRIX aViewProjection);

    //point
    bool IsVisible(XMVECTOR aPoint) const;

    //AABB
    bool IsVisible(const CAABB &aAABB);

    //segment
    bool IsVisible(XMVECTOR p1, XMVECTOR p2);

    //triangle
    bool IsVisible(XMVECTOR aPoint1, XMVECTOR aPoint2, XMVECTOR aPoint3);
};

#endif