#include "mcv_platform.h"
#include "input_manager.h"
#include "debug_manager.h"
#include "utils.h"

// XInput for the XBOX controller
// http://msdn.microsoft.com/en-us/library/windows/desktop/ee417001(v=vs.85).aspx

CInputManager InputManager;

CInputManager::CInputManager(){
    buttons[W_KEY].key = 'W';
    buttons[A_KEY].key = 'A';
    buttons[S_KEY].key = 'S';
    buttons[D_KEY].key = 'D';
    buttons[V_KEY].key = 'V';
    buttons[R_KEY].key = 'R';
    buttons[T_KEY].key = 'T';
    buttons[Z_KEY].key = 'Z';
    buttons[F_KEY].key = 'F';
    buttons[SPACE_KEY].key = ' ';

    buttons[ONE_KEY].key = '1';
    buttons[TWO_KEY].key = '2';
    buttons[THREE_KEY].key = '3';
    buttons[FIVE_KEY].key = '5';
    buttons[EIGHT_KEY].key = '8';
    buttons[NINE_KEY].key = '9';
    buttons[Q_KEY].key = 'Q';
    buttons[E_KEY].key = 'E';
    buttons[H_KEY].key = 'H';
    buttons[SHIFT_KEY].key = VK_SHIFT;
    buttons[CTRL_KEY].key = VK_CONTROL;

    buttons[MOUSE_LEFT].key = VK_LBUTTON;
    buttons[MOUSE_RIGHT].key = VK_RBUTTON;
    buttons[MOUSE_MIDDLE].key = VK_MBUTTON;

    buttons[ENTER_KEY].key = VK_RETURN;
    buttons[F1_KEY].key = VK_F1;
    buttons[F2_KEY].key = VK_F2;
    buttons[F3_KEY].key = VK_F3;
    buttons[F4_KEY].key = VK_F4;
    buttons[F5_KEY].key = VK_F5;
    buttons[F6_KEY].key = VK_F6;
    buttons[F7_KEY].key = VK_F7;
    buttons[F8_KEY].key = VK_F8;
    buttons[F9_KEY].key = VK_F9;
    buttons[F10_KEY].key = VK_F10;
    buttons[F11_KEY].key = VK_F11;
    buttons[F12_KEY].key = VK_F12;

    buttons[TAB_KEY].key = VK_TAB;

}


void CInputManager::notifyMouseButtonState(TButtonID button, bool new_state) {
    if (button == MOUSE_RIGHT)
        mouse.is_right_pressed = new_state;
    else if (button == MOUSE_LEFT)
        mouse.is_left_pressed = new_state;
    else if (button == MOUSE_MIDDLE)
        mouse.is_middle_pressed = new_state;
    else if (button == MOUSE_WHEEL_UP)
        mouse.is_wheel_up_pressed = new_state;
    else if (button == MOUSE_WHEEL_DOWN)
        mouse.is_wheel_down_pressed = new_state;
}



void CInputManager::TButton::setPressed(bool how, float elapsed) {

    is_pressed = how;

    // The key was and is pressed
    if (was_pressed && is_pressed)
        time_pressed += elapsed;

    // The key was and is NOT pressed anymore -> has been released
    if (was_pressed && !is_pressed)
        time_released = 0.f;

    // ... 
    if (!was_pressed && !is_pressed)
        time_released += elapsed;

    // The key was NOT pressed and now IS pressed -> has been pressed
    if (!was_pressed && is_pressed)
        time_pressed = 0.f;

}

void CInputManager::update(float elapsed) {

    // update buttons
    for (int i = 0; i < BUTTONS_COUNT; ++i) {
        TButton &b = buttons[i];

        // 
        b.was_pressed = b.is_pressed;

        bool now_is_pressed = false;
        // Is the key right now pressed?
        if (b.key)
            now_is_pressed = (::GetAsyncKeyState(b.key) & 0x8000) != 0;

        if (i == MOUSE_LEFT)
            now_is_pressed |= mouse.is_left_pressed;
        else if (i == MOUSE_RIGHT)
            now_is_pressed |= mouse.is_right_pressed;
        else if (i == MOUSE_MIDDLE)
            now_is_pressed |= mouse.is_middle_pressed;
        else if (i == MOUSE_WHEEL_DOWN)
            now_is_pressed |= mouse.is_wheel_down_pressed;
        else if (i == MOUSE_WHEEL_UP)
            now_is_pressed |= mouse.is_wheel_up_pressed;

        // if( xinput_state.buttons & b.xflag ) now_is_pressed |= true;

        b.setPressed(now_is_pressed, elapsed);
    }

    // mouse
    if (!debug_manager::IsDebug()){
        mouse.update();
    }

    // update joysticks. Check the XInput lib
}

void CInputManager::TMouse::update() {

    if (!App.has_focus) {
        dx = dy = 0;
        return;
    }

    POINT mid_screen = { App.xres / 2, App.yres / 2 };
    ::ClientToScreen(App.hWnd, &mid_screen);

    POINT cursor_screen;
    ::GetCursorPos(&cursor_screen);

    dx = cursor_screen.x - mid_screen.x;
    dy = cursor_screen.y - mid_screen.y;

    ::SetCursorPos(mid_screen.x, mid_screen.y);

    // Clear the wheel up/down after each update
    // Those are trigger only events, you can't have the 
    // wheel down pressed
    is_wheel_down_pressed = false;
    is_wheel_up_pressed = false;
}

