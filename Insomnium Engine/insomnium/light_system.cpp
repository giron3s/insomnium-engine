#include "mcv_platform.h"
#include "light_system.h"
#include "draw_utils.h"
#include "shader.h"
#include "entity_parser.h"
#include "camera.h"
#include "camera_manager.h"
#include "render_manager.h"
#include "render_techniques_manager.h"

#include "components\comp_transform.h"

bool CLightSystem::init(){
    tech_pointlight = (CRenderTechnique *)render_techniques_manager.getByName("deferred.pointlight");
    tech_directionallight = (CRenderTechnique *)render_techniques_manager.getByName("deferred.directionallight");
    tech_pointlight->setTexture("txZBuffer", &render_manager.rts[RT_ZBUFFER]);
    tech_pointlight->setTexture("txSpecular", &render_manager.rts[RT_SPECULAR]);
    tech_pointlight->setTexture("txNormal",  &render_manager.rts[RT_NORMAL]);

    tech_directionallight->setTexture("txSpecular", &render_manager.rts[RT_SPECULAR]);
    tech_directionallight->setTexture("txNormal", &render_manager.rts[RT_NORMAL]);
    tech_directionallight->setTexture("txExtra", &render_manager.rts[RT_EXTRA]);
    return true;
}

unsigned CLightSystem::registerPointlightToRender( CPointlight *pointlight ){
    pointlights.push_back( pointlight );
    return seq_unique_id++;
}

unsigned CLightSystem::registerDirlightToRender(CDirlight *dirlight){
    dirlights.push_back( dirlight );
    return seq_unique_id++;
}
unsigned CLightSystem::registerSkylightToRender(CSkylight *skylight){
    this->skylight = skylight;
    return seq_unique_id++;
}
void CLightSystem::unregisterFromRender(unsigned aunique_id){

}


void CLightSystem::update(float delta){

}

void CLightSystem::render(){
    CTraceScoped t0("renderLightBuffer");
    camera_manager.uploadCameraParamsToGPU( tech_pointlight );

    float ambient_light = 0.02f;
    render_manager.rts[RT_ACC_LIGHT].begin();
    render_manager.rts[RT_ACC_LIGHT].clear(XMFLOAT4(0., 0., 0., 0));
        renderDirectionalLight();
        renderPointLights();
    render_manager.rts[RT_ACC_LIGHT].end();
}

/**
 * Render the directional light
 */
void CLightSystem::renderDirectionalLight(){
    DrawUtils::activateFullScreenOrthoCamera();
    render_manager.setDepthConfig(Z_DISABLE_ALL);
    tech_directionallight->activate();


    typedef vector<CDirlight *>::const_iterator IT;
    for (IT iter = dirlights.begin(); iter < dirlights.end(); iter++){

        CDirlight *curr_dirlight = *iter;
        ConstantBufferDirectionalLight cb;
        cb.dirlight_color = curr_dirlight->directional_color;
        cb.dirlight_direction = curr_dirlight->direction;
        cb.dirlight_intensity = curr_dirlight->directional_intensity;
        cb.ambient_color = skylight->ambient_color;
        cb.ambient_intensity = skylight->ambient_intensity;

        XMVECTOR determinant;
        cb.dirlight_inverse = XMMatrixInverse(&determinant, camera_manager.free_camera.getView());
        bool is_ok = tech_directionallight->setParam("ConstantBufferDirectionalLight", cb);
        assert(is_ok);

        DrawUtils::setWorldMatrix(XMMatrixIdentity());
        DrawUtils::drawTextured2D(0., 0., App.xres, App.yres, NULL, tech_directionallight);
    }
}

/**
 * Render the poinglights
 */
void CLightSystem::renderPointLights() {
    
    //// Activar nuestro estado de rasterizacion
    ID3D11RasterizerState *prev_rs_state;
    App.immediateContext->RSGetState(&prev_rs_state);
    render_manager.setRasterizerStates(TRasterizerConfig::RASTER_CULL_CLOCKWISE);

    tech_pointlight->activate();
    DrawUtils::activateCamera(camera_manager.free_camera );
    render_manager.setDepthConfig(Z_TEST_INVERSE_WRITE_OFF);


    //
    typedef vector<CPointlight *>::iterator IT;
    for (IT iter = pointlights.begin(); iter < pointlights.end(); iter++){
        CPointlight *p = *iter;
        CTransform *transform = p->getOwner()->get<CTransform>();
        
        ConstantBufferPointLight buf_pl;
        buf_pl.pointlight_color = p->color;
        XMVECTOR light_view_space = XMVector3TransformCoord(transform->getPosition(), camera_manager.free_camera.getView());
        XMStoreFloat4(&buf_pl.pointlight_viewpos, light_view_space);
        buf_pl.pointlight_dmin = p->min_radius;
        buf_pl.pointlight_dmax = p->max_radius;

        tech_pointlight->setParam("ConstantBufferPointLight", buf_pl);

        XMMATRIX m = XMMatrixScaling(p->max_radius, p->max_radius, p->max_radius);
        XMFLOAT3 location;
        XMStoreFloat3(&location, transform->getPosition());
        m *= XMMatrixTranslation(location.x, location.y, location.z);
        DrawUtils::setWorldMatrix(m);
        DrawUtils::drawIcosahedron();
    }

    // Restaurar el raster state
    App.immediateContext->RSSetState(prev_rs_state);
    if (prev_rs_state) prev_rs_state->Release();

}

void CLightSystem::exportEntities(){
    vector<CEntity*> entities;

    //Pointlights
    typedef vector<CPointlight *>::const_iterator IT1;
    for (IT1 iter = pointlights.begin(); iter < pointlights.end(); iter++){
        entities.push_back( (*iter)->getOwner());
    }

    //Skylight
    entities.push_back( skylight->getOwner());


    //Dirlights
    typedef vector<CDirlight *>::const_iterator IT3;
    for (IT3 iter = dirlights.begin(); iter < dirlights.end(); iter++){
        entities.push_back( (*iter)->getOwner() );
    }
    CEntityParser exporter;
    exporter.exportLights(entities);
}

void CLightSystem::destroy(){

}