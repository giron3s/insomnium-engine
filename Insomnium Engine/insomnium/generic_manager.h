#ifndef INC_GENERIC_MANAGER_H_
#define INC_GENERIC_MANAGER_H_

#include <map>
#include <string>

template <class TObj >
bool initObjFromName(TObj &obj, const char *name);

template <class TObj>
class CObjManager {

protected:
    typedef std::map< std::string, TObj *> MObjsByName;
    MObjsByName objs;

public:

    bool exists(const char *name) const {
        MObjsByName::const_iterator i = objs.find(name);
        return i != objs.end();
    }

    const TObj *getByName(const std::string &name) {
        // Busca si existe
        MObjsByName::const_iterator i = objs.find(name);
        if (i == objs.end()) {
            // si no existe...
            // lo creamos, 
            TObj *new_obj = new TObj;
            // lo asociamos al nombre name
            objs[name] = new_obj;
            // lo cargamos
            bool is_ok = initObjFromName(*new_obj, name.c_str());
            assert(is_ok);
            // y lo devolvemos
            return new_obj;
        }
        // Si existe, lo devolvemos
        return i->second;
    }
};

#endif
