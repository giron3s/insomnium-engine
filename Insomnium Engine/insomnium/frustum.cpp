#include "mcv_platform.h"
#include "frustum.h"
#include "aabb.h"
//--------------------------------------------------------------------------------------------------------------
//  Calculate the frustrum planes
//--------------------------------------------------------------------------------------------------------------
void CFrustum::Init(XMMATRIX aViewProjection) 
{
    XMMATRIX mtrans = XMMatrixTranspose(aViewProjection);
    planes[PLANE_LEFT]  = XMPlaneNormalize(mtrans.r[0] + mtrans.r[3]);
    planes[PLANE_RIGHT] = XMPlaneNormalize(-mtrans.r[0] + mtrans.r[3]);
    planes[PLANE_UP]    = XMPlaneNormalize(-mtrans.r[1] + mtrans.r[3]);
    planes[PLANE_DOWN]  = XMPlaneNormalize(mtrans.r[1] + mtrans.r[3]);
    planes[PLANE_NEAR]  = XMPlaneNormalize(mtrans.r[2]);
    planes[PLANE_FAR]   = XMPlaneNormalize(-mtrans.r[2] + mtrans.r[3]);
}

//--------------------------------------------------------------------------------------------------------------
// Determine if the point is visible or not
//--------------------------------------------------------------------------------------------------------------
bool CFrustum::IsVisible(XMVECTOR p) const
{
    for (int i = 0, lSize = PLANES_COUNT; i<lSize; ++i) {
        if (XMVectorGetX(XMVector4Dot(planes[i], p)) < 0)
            return false;
    }
    return true;
}


float CFrustum::Fast_fabsf(float f)
{
    union { float a; int b; } c;
    c.a = f;
    c.b &= 0x7fffffff;
    return c.a;
}

//--------------------------------------------------------------------------------------------------------------
// Determine if the aabb is visible or not
// Performance (cycles/AABB): Average = 63.9 (stdev = 10.8)
// http://www.gamedev.net/page/resources/_/technical/general-programming/useless-snippet-2-aabbfrustum-test-r3342
//--------------------------------------------------------------------------------------------------------------
bool CFrustum::IsVisible(const CAABB &aAABB)
{
    const XMFLOAT3 aabbCenter = XMFLOAT3(XMVectorGetX(aAABB.GetCenter()), XMVectorGetY(aAABB.GetCenter()), XMVectorGetZ(aAABB.GetCenter()));
    const XMFLOAT3 aabbSize = XMFLOAT3(XMVectorGetX(aAABB.GetHalfSize()), XMVectorGetY(aAABB.GetHalfSize()), XMVectorGetZ(aAABB.GetHalfSize()));

    unsigned int result = INSIDE; // Assume that the aabb will be inside the frustum
    for (unsigned int iPlane = 0; iPlane < PLANES_COUNT; ++iPlane){

        const XMFLOAT4 frustumPlane = XMFLOAT4(XMVectorGetX(planes[iPlane]), XMVectorGetY(planes[iPlane]), XMVectorGetZ(planes[iPlane]), XMVectorGetW(planes[iPlane]));
        float d = aabbCenter.x * frustumPlane.x +
            aabbCenter.y * frustumPlane.y +
            aabbCenter.z * frustumPlane.z;

        float r = aabbSize.x * Fast_fabsf(frustumPlane.x) +
            aabbSize.y * Fast_fabsf(frustumPlane.y) +
            aabbSize.z * Fast_fabsf(frustumPlane.z);

        float d_p_r = d + r;
        float d_m_r = d - r;

        if (d_p_r < -frustumPlane.w)
        {
            result = OUTSIDE;
            break;
        }
        else if (d_m_r < -frustumPlane.w)
            result = INTERSECT;
    }

    //Determine if has to culling or not
    if (result == INSIDE || result == INTERSECT){
        return true;
    }
    else{
        return false;
    }
}

//--------------------------------------------------------------------------------------------------------------
// Determine if the segment formed by the 2 points is visible
//--------------------------------------------------------------------------------------------------------------
bool CFrustum::IsVisible(XMVECTOR p1, XMVECTOR p2)
{
    for (int i = 0; i<6; i++){
        float distP1 = XMVectorGetX(XMVector4Dot(planes[i], p1));
        float distP2 = XMVectorGetX(XMVector4Dot(planes[i], p2));

        // completely outside
        if (distP1<0 && distP2<0)return false;

        // both points are inside, no clip
        if (distP1 * distP2 >0) continue;

        // now, clipping
        XMVECTOR intersecPoint;
        SegmentPlaneIntersection(planes[i], p1, p2, intersecPoint);


        if (distP1 < 0){
            p1 = intersecPoint;
        }
        else{
            p2 = intersecPoint;
        }
    }
    return true;

}

//--------------------------------------------------------------------------------------------------------------
// SegmentPlaneIntersection
//--------------------------------------------------------------------------------------------------------------
bool CFrustum::SegmentPlaneIntersection(XMVECTOR plane, XMVECTOR segA, XMVECTOR segB, XMVECTOR &interPoint)
{
    XMVECTOR segDir = segB - segA;
    XMVECTOR pointInPlane;
    if (fabs(XMVectorGetX(plane)) > 0.01f){
        pointInPlane = XMVectorSet(-XMVectorGetW(plane) / XMVectorGetX(plane), 0, 0, 1);
    }
    else if (fabs(XMVectorGetY(plane)) > 0.01f){
        pointInPlane = XMVectorSet(0.f, -XMVectorGetW(plane) / XMVectorGetY(plane), 0.f, 1);
    }
    else if (fabs(XMVectorGetZ(plane)) > 0.01f){
        pointInPlane = XMVectorSet(0.f, 0.f, -XMVectorGetW(plane) / XMVectorGetZ(plane), 1);
    }

    XMVECTOR u = segB - segA;
    XMVECTOR w = segA - pointInPlane;

    float D = XMVectorGetX(XMVector4Dot(plane, u));
    float N = -XMVectorGetX(XMVector4Dot(plane, w));

    // segment is parallel to plane
    if (fabs(D) < 0.01) {
        return false;
    }

    // they are not parallel
    // compute intersect param
    float sI = N / D;
    if (sI < 0 || sI > 1){
        // no intersection
        return false;
    }

    // compute segment intersect point
    interPoint = segA + sI * u;
    return true;
}

//--------------------------------------------------------------------------------------------------------------
//  Determine if the triangle formed by the 3 points is visible
//--------------------------------------------------------------------------------------------------------------
bool CFrustum::IsVisible(XMVECTOR aPoint1, XMVECTOR aPoint2, XMVECTOR aPoint3)
{
    //compute mid point for each edge
    XMVECTOR p1p2Mid = (aPoint1 + aPoint2) / 2.f;
    XMVECTOR p2p3Mid = (aPoint2 + aPoint3) / 2.f;
    XMVECTOR p3p1Mid = (aPoint3 + aPoint1) / 2.f;

    //if any of the edges is visible or any of its medians is visible
    return IsVisible(aPoint1, aPoint2) || IsVisible(aPoint2, aPoint3) || IsVisible(aPoint3, aPoint1) || IsVisible(p1p2Mid, aPoint3) || IsVisible(p2p3Mid, aPoint1) || IsVisible(p3p1Mid, aPoint2);
}