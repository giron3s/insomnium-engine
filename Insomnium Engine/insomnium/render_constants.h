#ifndef RENDER_CONSTANTS_H
#define RENDER_CONSTANTS_H


static const string PATH_MESH = "data\\meshes\\";
static const string PATH_TEXTURES = "data\\textures\\";
static const string PATH_SHADERS = "data\\shaders\\";
static const string PATH_SHADER_CONST = "data\\shaders\\constants";

static const int SHADOW_MAP_CASCADE_COUNT = 3; // Maximo numero de cascadas que se pueden generar

// Render textures
enum TRenderTechniqueGroup {
    TECH_DEFAULT,
    TECH_DEFERRED,
    TECH_REFLECTION,
    TECH_LIGHTS,
    TECH_SHADOWS,
    TECH_POST_PROCESS,
    TECH_TECH_COUNT
};


//Depth configuration
enum TZConfig {
    Z_DISABLE_ALL = 0
    , Z_DEFAULT         // z test and z write
    , Z_TEST_ON_WRITE_OFF
    , Z_TEST_INVERSE_WRITE_OFF
    , Z_REFLECTION_MARK  //mark the reflection zone to the stencil buffer
    , Z_REFLECTION       
    , Z_CONFIG_COUNT
};

// Raster configuration
enum TBlendConfig {
    BLEND_DISABLED = 0
    , BLEND_ALPHA    //Uses the alpha channel of thesource color to create a transparency effect
                     //Known as Blend combinative


    , BLEND_ADDITIVE //Add the 2 colors
    , BLEND_NO_WRITE //No write to the render target
    , BLEND_TRANSPARENT
    , BLEND_CONFIG_COUNT
};

// Raster configuration
enum TRasterizerConfig {
    RASTER_FRONT_SOLID = 0
    , RASTER_FRONT_WIREFRAME
    , RASTER_BACK_SOLID
    , RASTER_CULL_DISABLED
    , RASTER_NO_CULL_WIREFRAME
    , RASTER_SHADOWS
    , RASTER_CULL_CLOCKWISE
    , RASTER_CONFIG_COUNT
};


// Render textures
enum TRenderTextures {
    RT_ALBEDO,
    RT_NORMAL,
    RT_SPECULAR,
    RT_DISPLACEMENT,
    RT_EXTRA,  //View space position
    RT_REFLECTION,
    RT_ZBUFFER,
    RT_ACC_LIGHT,
    RT_COUNT
};

#endif