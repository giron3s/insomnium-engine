#include "mcv_platform.h"
#include "composite.h"

#include "render_manager.h"
#include "render_techniques_manager.h"
#include "draw_utils.h"

bool CComposite::init(){
    
    bool is_ok = true;

    setEnable(true);
    tech = (CRenderTechnique *)render_techniques_manager.getByName("deferred.composite");
    //is_ok = is_ok & tech->setTexture("txAccLight", &render_manager.rts[RT_ACC_LIGHT]);

    return is_ok;
}


void CComposite::render(CRenderTexture &target) {
    CTraceScoped t0("renderComposite");
    tech->setTexture("txAccLight", &render_manager.rts[RT_ACC_LIGHT]);

    target.begin();
        DrawUtils::drawTextured2D(0, 0, App.xres, App.yres, &render_manager.rts[RT_ALBEDO], tech);
    target.end();
}


void CComposite::destoy(){
    //SAFE_RELEASE(tech);
}