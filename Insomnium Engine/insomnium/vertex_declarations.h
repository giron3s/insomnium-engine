#ifndef INC_VERTEX_DECLARATIONS_H_
#define INC_VERTEX_DECLARATIONS_H_

// ----------------------------------------
class CVertexDeclaration {
public:
  const D3D11_INPUT_ELEMENT_DESC *elems;
  unsigned                        nelems;
  CVertexDeclaration( const D3D11_INPUT_ELEMENT_DESC *desc, unsigned n );
};

extern CVertexDeclaration vtx_dcl_solid;
extern CVertexDeclaration vtx_dcl_pos;
extern CVertexDeclaration vtx_dcl_textured;
extern CVertexDeclaration vtx_dcl_textured2;
extern CVertexDeclaration vtx_dcl_normalmap;
extern CVertexDeclaration vtx_dcl_skin;

// ----------------------------------------
struct CVertexSolid
{  
  XMFLOAT3 Pos;
  XMFLOAT4 Color;
  static const CVertexDeclaration *getDecl() { return &vtx_dcl_solid; }
};

struct CVertexPosition
{  
  XMFLOAT3 Pos;
  static const CVertexDeclaration *getDecl() { return &vtx_dcl_pos; }
};

struct CVertexTextured
{  
  XMFLOAT3 Pos;
  XMFLOAT2 Tex;
  static const CVertexDeclaration *getDecl() { return &vtx_dcl_textured; }
};

// Para pintar cal3d por software
struct CVertexSkin
{  
  XMFLOAT3      Pos;
  XMFLOAT3      Nor;
  XMFLOAT2      Tex;
  unsigned char weights[4];
  unsigned char indices[4];
  static const CVertexDeclaration *getDecl() { return &vtx_dcl_skin; }
};

#endif
