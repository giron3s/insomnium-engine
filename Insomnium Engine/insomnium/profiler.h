#ifndef _PROFILER_H_
#define _PROFILER_H_

#include "mcv_platform.h"

namespace utils{
    namespace profiler {
        namespace fps 
        {
            void      Init();
            void      Update();
            const int GetFPS();
        }

        namespace graphics
        {
            //Global variables
            extern uint32 mDrawCalls;

            extern float  mTextureSize;
            extern uint32 mTextureCount;
            extern float  mRenderTextureSize;
            extern float  mRenderTextureCount;
                          
            extern uint32 mVertexCount;
            extern float  mParamSize;
            extern uint32 mParamCount;

            extern uint32 mCulledObjectsCount;
            extern uint32 mRenderedObjectsCount;

            void          Init();
        }
    }
};
#endif 