#include "mcv_platform.h"
#include "numeric.h"

namespace utils{
    namespace numeric {

        float Clamp(float value, float min, float max) {
            if (value < min){
                return min;
            }
            else if (value > max){
                return max;
            }
            else{
                return value;
            }
        }

        float Lerp(float from, float to, float time){
            time = Clamp(time, 0.f, 1.f);
            return (from * time) + (to * (1 - time));
        }


        XMFLOAT3 XMFloat3Sum(const XMFLOAT3 a, const XMFLOAT3 b){
            XMFLOAT3 sum;
            sum.x = a.x + b.x;
            sum.y = a.y + b.y;
            sum.z = a.z + b.z;
            return sum;
        }

        XMFLOAT3 XMFloat3Sub(const XMFLOAT3 a, const XMFLOAT3 b){
            XMFLOAT3 sub;
            sub.x = a.x - b.x;
            sub.y = a.y - b.y;
            sub.z = a.z - b.z;
            return sub;
        }


        XMFLOAT3 XMTransform(const XMVECTOR point, const XMMATRIX matrix){

            XMFLOAT3 result;
            XMFLOAT4 temp = XMFLOAT4(XMVectorGetX(point), XMVectorGetY(point), XMVectorGetZ(point), 1); //need a 4-part vector in order to multiply by a 4x4 matrix
            XMFLOAT4 temp2;

            temp2.x = temp.x * matrix._11 + temp.y * matrix._21 + temp.z * matrix._31 + temp.w * matrix._41;
            temp2.y = temp.x * matrix._12 + temp.y * matrix._22 + temp.z * matrix._32 + temp.w * matrix._42;
            temp2.z = temp.x * matrix._13 + temp.y * matrix._23 + temp.z * matrix._33 + temp.w * matrix._43;
            temp2.w = temp.x * matrix._14 + temp.y * matrix._24 + temp.z * matrix._34 + temp.w * matrix._44;

            result.x = temp2.x / temp2.w; //view projection matrices make use of the W component
            result.y = temp2.y / temp2.w;
            result.z = temp2.z / temp2.w;

            return result;
        }
    }
}