#include "mcv_platform.h"
#include "postprocess_system.h"

#include "draw_utils.h"
#include "render_manager.h"

bool  CPostProcessSystem::init(){
    bool is_ok = true;
    is_ok &= blur.init(App.xres, App.yres);
    is_ok &= composite.init();
    
    return is_ok;
}

void  CPostProcessSystem::render(CRenderTexture &target){
    render_manager.setDepthConfig(Z_DISABLE_ALL);
    DrawUtils::activateFullScreenOrthoCamera();

    composite.render(target);
}

void  CPostProcessSystem::update(float delta){
}

void  CPostProcessSystem::destroy(){
    blur.destroy();
}