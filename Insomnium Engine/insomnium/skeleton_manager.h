#ifndef INC_SKELETON_MANAGER_H_
#define INC_SKELETON_MANAGER_H_

//--------------------------------------------------------------------------------------
#include "generic_manager.h"
#include "data_provider.h"
#include "mesh.h"
#include "cal3d/cal3d.h"

struct CCoreRigidAnimation;

struct CCoreSkeleton : public CalCoreModel {
  // ... todos los metadatos del skeleton q estan compartidas
  // por las instancias
  // int walk_id;
  int           idle_id;          // animacion para el idle
  int           walk_id;          // animacion para el walk
  int           mesh_id;
  CRenderMesh  *model_lines;    // Para pintar las lineas

  // Un dato opcional de cada animacion
  const CCoreRigidAnimation *rigid_anim_of_impact;

  typedef std::vector< int > VInts;
  VInts         used_bones_ids;

  // Lookat Info --------------------------------
  struct TLookAtStep {
    int       bone_id;
    CalVector axis;
    float     amount;
    void apply( CalModel *model, CalVector lookat_target, float global_amount ) const;
  };
  typedef std::vector< TLookAtStep > VLookAtSteps;
  VLookAtSteps lookat_steps;
  void configLookat( );
  // Lookat Info --------------------------------

  CCoreSkeleton( ) 
    : CalCoreModel( "unnamed" )
    , idle_id( 0 )
    , walk_id( 0 )
    , mesh_id( 0 )
    , model_lines( NULL ) 
  { }
};

class CSkeletonManager : public CObjManager<CCoreSkeleton> {
public:
};
extern CSkeletonManager skeleton_manager;

#endif
