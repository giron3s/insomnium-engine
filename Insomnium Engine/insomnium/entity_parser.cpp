#include "mcv_platform.h"
#include "logger.h"
#include "entity_parser.h"
#include "XMLParser.h"
#include "components/components.h"
#include "utils.h"
#include "material_manager.h"
#include "entity_manager.h"

void CEntityParser::onStartElement(const std::string &elem, MKeyValue &atts) {
    if (elem == "entity") {
        assert(curr_entity == NULL);
        curr_entity = new CEntity;
        curr_entity->setName(atts["name"].c_str());

    }
    else if (elem == "scene" || elem == "lights") {
        entities_loaded.clear();

    }
    else if (elem == "material_defs") {
        // Nothing to do...

    }
    else if (elem == "material_def") {
        material_manager.onStartElement(elem, atts);
        // crear y registrar un nuevo material en el material_manager

    }
    else {
        assert(curr_entity != NULL);
        ICompManagerBase *new_base = CCompManagersRegistry::get().getByCompName(elem);
        if (new_base) {
            assert(curr_base == NULL);
            curr_base = new_base;
            assert(curr_component == NULL);

            // Se pide una nueva instancia que venga inicializada con los
            // parametros del xml atts
            curr_component = curr_base->getNewInstance(atts);

            // Asociar el nuevo componente a la current entity
            curr_entity->addComponent(curr_base->getComponentID(), curr_component);

        }
        else {

            // elem no es un component, es pj 'material'
            // Si estoy dentro de un component...
            // hago que el manager de ese tipo de componentes haga el parsing
            if (curr_base) {
                curr_base->loadXMLTag(elem, atts, curr_component);
            }
            utils::logger::dbg("Hey, unknown tag %s\n", elem.c_str());
        }
    }
}
void CEntityParser::onEndElement(const std::string &elem) {
    if (elem == "entity") {
        assert(curr_entity != NULL);
        curr_entity->send(MSG_ENTITY_CREATED);
        entities_loaded.push_back(curr_entity);

        // Register the entity by name
        entity_manager.add(curr_entity);

        // append curr_entity to the list of active entities
        curr_entity = NULL;

    }
    else if (curr_base && elem == curr_base->getName()) {
        assert(curr_component);
        curr_base = NULL;
        curr_component = NULL;

    }
    else {
        // Estoy saliendo de 'material'
    }
}

void CEntityParser::exportScene(vector<CEntity *> entities){
    ofstream xmlfile;
    xmlfile.open("data/scene.xml");
    xmlfile << "<scene>";
    exportEntities(entities, xmlfile);
    xmlfile << "</scene>";
    xmlfile.close();
}

void CEntityParser::exportLights(vector<CEntity *> entities){
    ofstream xmlfile;
    xmlfile.open("data/lights.xml");
    xmlfile << "<lights>" << endl;
    exportEntities(entities, xmlfile);
    xmlfile << "</lights>";
    xmlfile.close();
}

/**
 * Export the entities into the xml file
 */
void CEntityParser::exportEntities(vector<CEntity *> entities, ofstream& xmlfile){
    //Export the entities and it's components
    for (size_t i = 0; i < entities.size(); i++){
        CEntity *curr_entity = entities.at(i);
        curr_entity->exportToXML(xmlfile);
    }
}