#include "mcv_platform.h"
#include "comp_SKYlight.h"
#include "XMLParser.h"
#include "render_manager.h"

DECL_COMP_MANAGER(CSkylight, "skylight");

CSkylight::CSkylight(){
    unique_id = 0;
    ambient_color = XMFLOAT4(1., 1., 1., 1.);
    ambient_intensity = 1.;
}

void CSkylight::registerMsgs() {
    SUBSCRIBE(CSkylight, MSG_ENTITY_CREATED, onEntityCreated);
}

void CSkylight::onEntityCreated(const TMsg &msg) {
    render_manager.light_system.registerSkylightToRender(this);
}

void CSkylight::update(float dt){

}

/**
* Load the attributes from the xml file
*/
void CSkylight::onStartElement(const std::string &elem, MKeyValue &atts){
    ambient_color = atts.getFloat4("color");
    ambient_intensity = atts.getFloat("intensity", 1.);
}

/**
* Export to the xml file
*/
void CSkylight::exportComp(ofstream &xmlfile){
    stringstream ss(stringstream::in | stringstream::out);

    ss << "\t<skylight";
    ss << " color=\"" << ambient_color.x << " " << ambient_color.y << " " << ambient_color.z << " " << ambient_color.w;
    ss << " intensity=\"" << ambient_intensity << "/>";
    xmlfile << ss.str() << endl;
}


