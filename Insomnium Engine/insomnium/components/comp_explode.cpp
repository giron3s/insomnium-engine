#include "mcv_platform.h"
#include "comp_explode.h"
#include "comp_renderable.h"
#include "comp_rigid_animation.h"
#include "XMLParser.h"
#include "entity_parser.h"

DECL_COMP_MANAGER( CExplode, "explode" );

CExplode::CExplode( ) 
{ }

void CExplode::registerMsgs( ) {
  SUBSCRIBE( CExplode, MSG_EXPLODE, onExplode );
}

void CExplode::onExplode( const TMsg &msg ) {
  getOwner()->del<CRenderable>();
  CEntityParser parser;
  bool is_ok = parser.xmlParseFile( "data/" + scene_parts + ".xml" );
  assert(is_ok);

  // A�ade el comp rigid que hace que los hijos se muevan
  CRigidAnimation *r = getOwner()->add<CRigidAnimation>();
  r->setCore("muro");
  r->findTargets( parser.entities_loaded );
  
  // Colocar las entidades cargadas en la posicion relativa a mi 
  // actual posicion
  getOwner()->del<CExplode>();
}

// ---------------------------------------------
void CExplode::update( float dt ) {
}

// ---------------------------------------------
void CExplode::onStartElement( const std::string &elem, MKeyValue &atts ) {
  scene_parts = atts[ "scene_parts"];
}

void CExplode::exportComp(ofstream &xmlfile){

}

