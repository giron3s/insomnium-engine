#ifndef INC_COMP_EXPLODE_
#define INC_COMP_EXPLODE_

#include "components.h"

// ------------------------------------------------------
struct CExplode : public CComponent {
  std::string                scene_parts;
  CExplode( );
  static void registerMsgs( );
  void onStartElement( const std::string &elem, MKeyValue &atts );
  void update( float dt );
  void onExplode( const TMsg &msg );
  void exportComp(ofstream &xmlfile);
};

#endif