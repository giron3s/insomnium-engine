#ifndef INC_COMPONENTS_H_
#define INC_COMPONENTS_H_

#include "mcv_platform.h"
#include <string>
#include <vector>
#include <map>      // for the msgs registration
#include "utils.h"
#include "draw_utils.h"

#include <iostream>
#include <fstream>
using namespace std;


typedef unsigned TComponentID;

template<class TObj>
const char *getClassName();

template<class TObj>
TComponentID getClassID();

// Forward declaration
class CComponent;
class CEntity;
struct TMsg;
class MKeyValue;

// ----------------------------------------------
class CComponent {
  
public:
    intptr_t owner;
    bool     render_debug3D;

  CEntity *getOwner() {
    return (CEntity*)(owner & (~(intptr_t)1));
  }
  const CEntity *getOwner() const {
    return (CEntity*)(owner & (~(intptr_t)1));
  }
  void setOwner(CEntity *new_owner) {
    owner = (intptr_t)new_owner;
  }
  // que entity es mi owner?
  CComponent( ) : owner( NULL ) { }
  void update( float dt ) { }
  void renderDebug3D( ) { }
  virtual void exportComp(ofstream &xmlfile) { }
  void markAsDeleted() {
    owner |= 1;
  }
  bool isDeleted() const { return owner & 1; }
  // No virtual methods please!!!
  void onStartElement( const std::string &elem, MKeyValue &atts ) { }
};

// -----------------------------
class CEntity {
public:
  static const TComponentID min_component_id = 1;
  static const TComponentID max_component_id = 64;

  // ---------------------------------------
  template< class TComp >
  TComp *add( ) {
    assert( !has<TComp>() );
    // Pedir un puntero a una nueva instancia del componente de tipo TComp
    TComp *new_comp = CCompManager< TComp >::get()->addNew( );
    
    // This can be done without knowing the concrete component type
    addComponent( CCompManager< TComp >::getID(), new_comp );
    
    return new_comp;
  }

  // Add a component without knowing the component type
  void addComponent( TComponentID comp_id, CComponent *new_comp ) { 
    // Asignar el owner del componente, para que sepa quien es su owner
    new_comp->setOwner( this );
    // Guardarlo en el slot que le toca segun el tipo
    comps[ comp_id ] = new_comp;
  }

  // ---------------------------------------
  template< class TComp >
  void del( ) {
    delComponent( CCompManager< TComp >::getID() );
  }

  // ---------------------------------------
  template< class TComp >
  TComp *get( ) {
    return static_cast<TComp*>( comps[ CCompManager< TComp >::getID() ] );
  }

  // ---------------------------------------
  // return a const pointer to the components
  template< class TComp >
  const TComp *get() const {
    return static_cast<const TComp*>(comps[CCompManager< TComp >::getID()]);
  }

  // ---------------------------------------
  template< class TComp >
  bool has( ) const {
    return comps[ CCompManager< TComp >::getID() ] != NULL;
  }

  // ---------------------------------------
  void send( const TMsg &msg );

  // ---------------------------------------
  CEntity( ) {
    // Set to ZERO all component pointers to avoid errors
    memset( comps, 0x00, sizeof( comps ) );
  }

  ~CEntity( );

  // El manager me avisa que me han cambiado el puntero que 
  // representaba mi component 'comp_id', pero los datos estan 
  // ya copiados!
  void notifyComponentSwap( TComponentID comp_id, CComponent *c ) {
    comps[ comp_id ] = c;
  }

  CEntity *clone( ) const;
  void exportToXML( ofstream &xmlFile );
  const char *getName() const { return name.c_str(); }
  void setName( const char *new_name ) { name = new_name; }
  bool isNamed(const char *aname) { return name == aname; }
private:
  CComponent *comps[ max_component_id ];

  void delComponent( TComponentID comp_id );
  
  std::string name;
};

typedef std::vector< CEntity *> VEntities;

// ----------------------------------------------
// Una clase base para ver a todos los managers 
// con el mismo interfaz
class ICompManagerBase {
public:
  ICompManagerBase() : last_update_time(0.f) { }
  virtual ~ICompManagerBase( ) { }
  virtual const char *getName() const = 0;
  virtual size_t size() const = 0;
  virtual void delComp( CComponent *c ) = 0;
  virtual void update( float delta ) { }
  virtual TComponentID getComponentID( ) const = 0;
  virtual void renderDebug3D( ) { }

  // Must create a new instance of the component and init it with the 
  // xml atts given
  virtual CComponent *getNewInstance( MKeyValue &atts ) = 0;
  // Generate a copy of base
  virtual CComponent *getNewInstance( CComponent *base ) = 0;
  // El Component 'comp' ha recibido un tag xml 'elem' con los atts que sean
  // Por ejemplo, un material leido con sus params dentro del comp renderable
  virtual void loadXMLTag( const std::string &elem, MKeyValue &atts, CComponent *comp ) = 0;

  float last_update_time;

};

// ----------------------------------------------
// La clase que va a guardar todos los component 
// managers que se han registrado
class CCompManagersRegistry {
  typedef std::vector< ICompManagerBase* > VCompManagersBase;
  VCompManagersBase   comp_managers;
  TComponentID        next_id;
  typedef std::map< std::string, ICompManagerBase* > MCompManagersByName;
  MCompManagersByName comp_manager_by_name;

public:

  CCompManagersRegistry( ) : next_id( CEntity::min_component_id ) {
    comp_managers.resize( CEntity::max_component_id );
  }

  TComponentID registerCompManager( ICompManagerBase* new_manager ) {
    TComponentID new_id = next_id++;
    
    // too many components defined?, increase cte CEntity::max_component_id
    assert( new_id < CEntity::max_component_id );
    comp_managers[ new_id ] = new_manager;

    // Register it in the dictionary by name
    comp_manager_by_name[ new_manager->getName() ] = new_manager;
    return new_id;
  }

  static CCompManagersRegistry &get() {
    static CCompManagersRegistry the_registry;
    return the_registry;
  }
  
  ICompManagerBase *getByCompID( TComponentID id ) {
    return comp_managers[ id ];
  }
  ICompManagerBase *getByCompName( const std::string &comp_name ) {
    MCompManagersByName::iterator i = comp_manager_by_name.find( comp_name );
    if( i == comp_manager_by_name.end() )
      return NULL;
    return i->second;
  }
  
  void updateAll( float delta ) {
    TComponentID i = CEntity::min_component_id;
    while( comp_managers[ i ] ) {
      CCronTimer tm;
      comp_managers[ i ]->update( delta );
      comp_managers[i]->last_update_time = tm.elapsed();
      ++i;

    }
  }

  void renderDebug3DAll( ) {
    TComponentID i = CEntity::min_component_id;
    while( comp_managers[ i ] ) {
      comp_managers[ i ]->renderDebug3D( );
      ++i;
    }
  }
};

// ------------------------------------------------------
// Un template para poder declarar todos los managers
// con una misma clase
template< class TComp >
class CCompManager : public ICompManagerBase {
  // Los datos de verdad
  std::vector< TComp > comps;

  bool updating_components;

  static CCompManager<TComp> the_manager;
  static TComponentID        comp_id;

public:
  static CCompManager<TComp> *get() { return &the_manager; }
  bool enable_debug3d;

  CCompManager() : enable_debug3d( false ) { }
  
  // Pide memoria para las instancias 
  void init( size_t ninstances ) {
    assert( comp_id == 0 );   // No llamar a este metodo mas de 1 vez!
    comp_id = CCompManagersRegistry::get().registerCompManager( this );
    comps.reserve( ninstances );
    TComp::registerMsgs();
  }

  // Devuelve un objeto nuevo
  TComp *addNew( ) {
    comps.push_back( TComp() );
    return &comps.back();
  }

  TComp *getByIndex(size_t idx) {
    assert(idx < comps.size());
    return &comps[idx];
  }

  void delComp( CComponent *generic_c ) {

    //assert( !doing_update );

    assert( generic_c );
    TComp *c = static_cast< TComp* > ( generic_c );

    // Confirmar que c es realmente una direccion controlada por mi!
    size_t index = c - &(*comps.begin());
    assert( index < comps.size() );

    // Para que los componentes sigan estando todos juntos
    // vamos a coger el ultimo y 'moverlo' a la posicion 'index'
    if( comps.empty( ) )
      return;

    size_t last_index = comps.size()-1;
    
    // Si soy el ultimo solo hacer el pop_back
    if( index == last_index ) {
      comps.pop_back();

    } else {
      // mover el last_index sobre index

      //comps[index].markAsDeleted();

      char buf[sizeof(TComp)];
      memcpy(buf, &comps[index], sizeof(TComp));
      memcpy(&comps[index], &comps[last_index], sizeof(TComp));
      memcpy(&comps[last_index], buf, sizeof(TComp));

      //std::swap( comps[ index ], comps[ last_index ] );
      comps.pop_back();

      // Corregir la entidad que apuntaba a last_index, decirle q
      // su puntero esta en 'index'
      CEntity *e = comps[ index ].getOwner();
      assert( e );
      e->notifyComponentSwap( comp_id, c );
    }
  }

  void update( float delta ) { 
    updating_components = true;
    for (int i = 0; i < comps.size(); ++i) {
      if (!comps[i].isDeleted())
        comps[ i ].update( delta );
    }
    updating_components = false;
  }

  void renderDebug3D( ) { 
    if( enable_debug3d ) {
      DrawUtils::setWorldMatrix(XMMatrixIdentity());
      for( int i=0; i<comps.size(); ++i ) 
        comps[ i ].renderDebug3D( );
    }
  }
  
  CComponent *getNewInstance( MKeyValue &atts ) {
    TComp *comp = addNew( );
    comp->onStartElement( getName(), atts );
    return comp;
  }

  // Genera una copia del component
  CComponent *getNewInstance( CComponent *base ) {
    // Pedir una nueva instancia
    TComp *comp = addNew( );
    // Hacer un upcast a la clase derivada
    TComp *base_as_comp = static_cast<TComp*>( base ); 
    // Usar el operador copia del componente
    *comp = *base_as_comp;
    return comp;
  }

  void loadXMLTag( const std::string &elem, MKeyValue &atts, CComponent *generic_comp ) {
    TComp *comp = static_cast<TComp*>( generic_comp );
    comp->onStartElement( elem, atts );
  }

  const char *getName() const { return getClassName<TComp>(); }
  TComponentID getComponentID( ) const { return comp_id; }
  static TComponentID getID() { return comp_id; }
  size_t size() const { return comps.size(); }
};

// Dado un tipo TComp, esta funcion dice que el component manager
// de ese tipo es una variable statica de la clase CompManager<tipo>
// que se llama 'the_manager'
template< class TComp >
CCompManager<TComp> *getCompManager() { 
  return CCompManager<TComp>::get();
}

// -------------------------------------------------
// Macro para declarar mas facilmente todas las funciones
// y variables asociadas a component manager. Solo proporcionando
// el tipo y un nombre, declaro las cosas.
#define DECL_COMP_MANAGER(atype, aname) \
  CCompManager<atype> CCompManager<atype>::the_manager; \
  TComponentID CCompManager<atype>::comp_id = 0; \
  template<> const char *getClassName<atype>() { return aname; }

#define INIT_COMP_MANAGER(atype, anumber_of_instances) \
  getCompManager<atype>()->init( anumber_of_instances );


// -------------------------------------------------------
struct IFunctorBase {
  virtual ~IFunctorBase( ) { }
  virtual void execute( CComponent *obj, const TMsg &msg ) = 0;
};

template< class TObj >
struct TFunctor : public IFunctorBase {
  
  // Puntero a un member fn de la clase TObj que devuelve void
  // y recibe como argumento un (const TMsg *)
  typedef void (TObj::*TMemberFn)( const TMsg &msg );
  
  // Una variable para guardar la direccion del metodo que queremos
  // llamar
  TMemberFn member;
  
  // El ctor guarda el member al que habra que llamar
  TFunctor( TMemberFn amember ) : member( amember ) { }

  // La implementacion del virtual, convierte el objeto al tipo 
  // del template y llama al metodo que se guard� pasando el msg
  // como argumento
  void execute( CComponent *obj, const TMsg &msg ) {
    TObj *obj_of_my_type = static_cast<TObj*>( obj );
    (obj_of_my_type->*member)( msg );
  }
};

// The information stored for each msg registered
struct TComponentCallbackInfo {
  TComponentID comp_id;     // comp_id registered
  IFunctorBase *method;     // method of the class to be called
};

#include "entity_msgs.h"

// A multimap to store who should be called when a msg type arrives
typedef std::multimap< eMsgType, TComponentCallbackInfo > TMsgsSubscriptions;

template< class TComp >
void subscribeTo( eMsgType msg_type, IFunctorBase *method ) { 
  std::pair<eMsgType, TComponentCallbackInfo> e;
  e.first = msg_type;
  e.second.comp_id = CCompManager<TComp>::getID( );
  e.second.method = method;
  msg_subscriptions.insert( e );
}

extern TMsgsSubscriptions msg_subscriptions;

// Macro to simplify msg subscription
#define SUBSCRIBE(acomp,amsg,amethod) \
  subscribeTo<acomp>( amsg, new TFunctor< acomp>( &acomp::amethod ) );

// --------------------

void initComponents();

#endif
