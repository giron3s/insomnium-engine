#ifndef _INC_COMP_DIRLIGHT_
#define _INC_COMP_DIRLIGHT_

#include "components.h"
class CDirlight : public CComponent {
private:
    unsigned           unique_id;    // in render manager

public:
    XMFLOAT3 direction;
    XMFLOAT4 directional_color;
    float    directional_intensity;

    CDirlight();
    static void registerMsgs();

    void update(float dt);
    void onEntityCreated(const TMsg &msg);
    void onStartElement(const std::string &elem, MKeyValue &atts);
    void exportComp(ofstream &xmlfile);
};

#endif