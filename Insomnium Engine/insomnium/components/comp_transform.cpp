#include "mcv_platform.h"
#include "comp_transform.h"
#include "XMLParser.h"
#include "draw_utils.h"
#include "angular.h"

DECL_COMP_MANAGER(CTransform, "transform");

void CTransform::registerMsgs() {
}

/**
 * Return with the reflection matrix
 */
XMMATRIX CTransform::getReflection(){
    //Reflection = T(p) x R(n) x S(1, -1, 1) x R(n)^-1 x T(-p)
    float yaw, pitch;
    getYawPitchFromVector(getFront(), &yaw, &pitch);
    XMMATRIX Tp = XMMatrixTranslationFromVector(getPosition());
    XMMATRIX Rn = XMMatrixRotationRollPitchYaw(pitch, yaw, 0);
    XMMATRIX S = XMMatrixScaling(1, -1, 1);

    XMVECTOR determinant;
    XMMATRIX inv_Rn = XMMatrixInverse(&determinant, Rn);
    XMMATRIX neg_Tp = XMMatrixTranslationFromVector(getPosition() * -1);

    XMMATRIX R = Tp * Rn * S * inv_Rn * neg_Tp;
    return R;
}

XMMATRIX CTransform::getWorld(){
    return world;
}

void CTransform::setPosition(XMVECTOR new_pos) {
    world(3, 0) = XMVectorGetX(new_pos);
    world(3, 1) = XMVectorGetY(new_pos);
    world(3, 2) = XMVectorGetZ(new_pos);
}

XMVECTOR CTransform::getPosition() const {
    return XMVectorSet(world._41, world._42, world._43, 0.0f);
}

XMVECTOR CTransform::getFront() const {
    return XMVectorSet(world._31, world._32, world._33, 0.0f);
}

XMVECTOR CTransform::getLeft() const {
    return XMVectorSet(world._11, world._12, world._13, 0.0f);
}

void CTransform::lookAt(XMVECTOR eye, XMVECTOR target) {
    XMVECTOR front = target - eye;
    lookTo(eye, front);
}

void CTransform::lookTo(XMVECTOR eye, XMVECTOR front) {
    XMMATRIX inv_world = XMMatrixLookToRH(eye, -front, XMVectorSet(0.f, 1.f, 0.f, 0.f));
    XMVECTOR det;
    world = XMMatrixInverse(&det, inv_world);
}

void CTransform::onStartElement(const std::string &elem, MKeyValue &atts) {
    XMVECTOR t = atts.getPoint("t");
    XMVECTOR s = atts.getPoint("s");
    XMVECTOR q = atts.getQuat("q");

    world = XMMatrixRotationQuaternion(q);
    //  world *= XMMatrixScaling( s );
    world._41 = XMVectorGetX(t);
    world._42 = XMVectorGetY(t);
    world._43 = XMVectorGetZ(t);
}


/**
 * Export the component
 * //<transform q = "0.0 0.0 0.0 -1.0" t = "0.0 0.0 0.0" s = "1.0 1.0 1.0" / >
 */
void CTransform::exportComp(ofstream &xmlfile){

    stringstream ss(stringstream::in | stringstream::out);
    XMVECTOR q = XMQuaternionRotationMatrix(world);

    ss << "\t<transform ";
    ss << "q=\"" << XMVectorGetX(q) << " " << XMVectorGetY(q) << " " << XMVectorGetZ(q) << " " << XMVectorGetW(q) << "\" ";
    ss << "t=\"" << world._41 << " " << world._42 << " " << world._43 << "\" ";
    ss << "s=\"1.0 1.0 1.0\">";
    xmlfile << ss.str();
}

void CTransform::renderDebug3D() {
    DrawUtils::setWorldMatrix(world);
    DrawUtils::drawAxis();
}