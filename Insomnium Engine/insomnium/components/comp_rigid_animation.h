#ifndef INC_COMP_RIGID_ANIMATION_
#define INC_COMP_RIGID_ANIMATION_

#include "components.h"
#include "rigid_animation_manager.h"
#include "comp_transform.h"

// ------------------------------------------------------
struct CRigidAnimation : public CComponent {
  const CCoreRigidAnimation *core_anim;
  float                      curr_time;
  VEntities                  targets;

  std::string                scene_name;
  void findTargets();
  void findTargets( VEntities &entities );

  void setCore(const std::string &new_core);
  CRigidAnimation( );
  static void registerMsgs( );
  void onStartElement( const std::string &elem, MKeyValue &atts );
  void update( float dt );
  void exportComp(ofstream &xmlfile);
};

#endif