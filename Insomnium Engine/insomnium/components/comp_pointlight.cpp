#include "mcv_platform.h"
#include "comp_pointlight.h"
#include "render_manager.h"
#include "XMLParser.h"

DECL_COMP_MANAGER( CPointlight, "pointlight" );

//Constructor
CPointlight::CPointlight(){
    unique_id = 0;
    min_radius = 0.;
    max_radius = 0.;
    color = XMFLOAT4(0.,0.,0.,0.);
}

void CPointlight::registerMsgs() {
    SUBSCRIBE(CPointlight, MSG_ENTITY_CREATED, onEntityCreated);
}

void CPointlight::onEntityCreated(const TMsg &msg) {
    render_manager.light_system.registerPointlightToRender(this);
}

void CPointlight::update(float delta){

}

/**
 * Load from the xml
 */
void CPointlight::onStartElement(const std::string &elem, MKeyValue &atts){
    min_radius = atts.getFloat("min_radius", 0.);
    max_radius = atts.getFloat("max_radius", 0.);
    color = atts.getFloat4("color");
}

/**
 * Export the components
 * //<pointlight min_radius=0.0 max_radius=3.0 color="0.0 0.5 0.5 1.0"/>
 */

void CPointlight::exportComp(ofstream &xmlfile){
    stringstream ss(stringstream::in | stringstream::out);

    ss << "\t<pointlight";
    ss << " min_radius=\"" << min_radius << "\" ";
    ss << " max_radius=\"" << max_radius << "\" ";
    ss << " color=\"" << color.x << " " << color.y << " " << color.z << " " << color.w << "\">";
    xmlfile << ss.str() << endl;
}