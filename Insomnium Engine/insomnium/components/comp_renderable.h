#ifndef INC_COMP_RENDERABLE_
#define INC_COMP_RENDERABLE_

#include "components.h"

class CRenderMesh;
class CVertexShader;
class CPixelShader;
class CRenderTechnique;
class CMaterial;

class CRenderable : public CComponent {
public:

    static const int      max_materials_per_mesh = 8;
    static int            curr_loading_material_idx;
    const CRenderMesh     *mesh;
    const CMaterial       *mats[max_materials_per_mesh];
    unsigned              unique_id;                        // in render manager
    bool                  mIsCulled;

    CRenderable() : mesh(NULL), unique_id(0), mIsCulled(false){
        memset(mats, 0x00, sizeof(const CMaterial   *) * max_materials_per_mesh);
    }
    ~CRenderable();
    static void registerMsgs();

    void onEntityCreated(const TMsg &msg);

    void registerInRenderManager();
    void unregisterInRenderManager();
    void update(float delta);
    void uploadToGPU( CRenderTechnique* aRenderTech );

    void onStartElement(const std::string &elem, MKeyValue &atts);
    void renderDebug3D();
    void exportComp(ofstream &xmlfile);

    inline bool IsCulled() const { return mIsCulled; };
    CAABB getAABB() const;
};

#endif