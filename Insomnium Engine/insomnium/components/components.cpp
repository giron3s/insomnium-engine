#include "mcv_platform.h"
#include <vector>
#include "components.h"
#include "entity_msgs.h"
#include "utils.h"

// All msgs subscriptions
TMsgsSubscriptions msg_subscriptions;

// ------------------------------------------------------
CEntity::~CEntity() {
    // Borrar todos los componentes que tuviese my entity
    for (TComponentID i = min_component_id; i < max_component_id; ++i) {
        if (comps[i])
            delComponent(i);
    }
}

// Borrar el component 'id'
void CEntity::delComponent(TComponentID comp_id) {
    // Coger el manager 
    ICompManagerBase *base = CCompManagersRegistry::get().getByCompID(comp_id);
    // Pedirle que lo borre pasandole el puntero
    base->delComp(comps[comp_id]);
    comps[comp_id] = NULL;
}


void CEntity::send(const TMsg &msg) {

    // Para todos los callbacks registrados para ese msg_type
    TMsgsSubscriptions::const_iterator i = msg_subscriptions.find(msg.msg_type);
    while (i != msg_subscriptions.end() && i->first == msg.msg_type) {

        // Si esta entity tiene el componente
        if (comps[i->second.comp_id]) {

            // Obtener el component a clase base
            CComponent *comp = comps[i->second.comp_id];

            // El method->execute es virtual, hace el upcast a la clase 
            // Tcomponent derivada y pasa el msg a la clase
            i->second.method->execute(comp, msg);
        }
        ++i;
    }
}


CEntity *CEntity::clone() const {

    // Pido memoria para una nueva entidad vacia
    CEntity *e = new CEntity;

    // Para todos los componentes
    for (TComponentID comp_id = min_component_id; comp_id < max_component_id; ++comp_id) {

        // Si tengo una copia de ese componente
        if (comps[comp_id]) {
            // Pedir una nueva instancia
            ICompManagerBase *base = CCompManagersRegistry::get().getByCompID(comp_id);

            // Copiarla con mi original
            CComponent* new_comp = base->getNewInstance(comps[comp_id]);

            // asociarla a la nueva entidad e
            e->addComponent(comp_id, new_comp);
        }
    }

    return e;
}

void CEntity::exportToXML(ofstream &xmlFile){
    xmlFile << "<entity name=\"" + name  + "\">"<< endl;
    //Run over the whole componets
    for (TComponentID comp_id = min_component_id; comp_id < max_component_id; ++comp_id) {
        // Si tengo una copia de ese componente
        if (comps[comp_id]) {
            CComponent *c = comps[comp_id];
            c->exportComp( xmlFile );
        }
    }
    xmlFile << "</entity>" << endl;
}
