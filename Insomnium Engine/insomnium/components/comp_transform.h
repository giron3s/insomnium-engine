#ifndef INC_COMP_TRANSFORM_
#define INC_COMP_TRANSFORM_

#include "components.h"

// ------------------------------------------------------
struct CTransform : public CComponent {
private:
  XMMATRIX world;

public:
  static void registerMsgs();

  void onStartElement(const std::string &elem, MKeyValue &atts);
  void renderDebug3D();

  void setPosition( XMVECTOR new_loc );
  XMVECTOR getPosition() const;
  XMVECTOR getFront() const;
  XMVECTOR getLeft() const;

  XMMATRIX getReflection();
  XMMATRIX getWorld();

  void lookAt( XMVECTOR eye, XMVECTOR target );
  void lookTo( XMVECTOR eye, XMVECTOR front );
  void exportComp(ofstream &xmlfile);
};

#endif