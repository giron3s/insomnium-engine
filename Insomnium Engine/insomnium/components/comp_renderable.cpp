#include "mcv_platform.h"
#include "comp_renderable.h"
#include "comp_transform.h"
#include "render_manager.h"
#include "XMLParser.h"
#include "mesh_manager.h"
#include "material_manager.h"
#include "camera_manager.h"
#include "draw_utils.h"

/// 
DECL_COMP_MANAGER(CRenderable, "renderable");

int CRenderable::curr_loading_material_idx = 0;

void CRenderable::registerMsgs() {
    SUBSCRIBE(CRenderable, MSG_ENTITY_CREATED, onEntityCreated);
}

CRenderable::~CRenderable() {
    if (unique_id)
        unregisterInRenderManager();
}

void CRenderable::onEntityCreated(const TMsg &msg) {
    registerInRenderManager();
}

void CRenderable::registerInRenderManager() {
    assert(unique_id == 0);
    unique_id = render_manager.registerToRender(this);
}

void CRenderable::unregisterInRenderManager() {
    assert(unique_id != 0);
    render_manager.unregisterFromRender(unique_id);
    unique_id = 0;
}

//------------------------------------------------------------------
// XML parser
//------------------------------------------------------------------
void CRenderable::onStartElement(const std::string &elem, MKeyValue &atts) {
    if (elem == "renderable") 
    {
        std::string mesh_name = atts.getString("mesh", "");
        assert(!mesh_name.empty());
        mesh = mesh_manager.getByName(mesh_name.c_str());
        curr_loading_material_idx = 0;

    }
    else if (elem == "material") 
    {
        assert(curr_loading_material_idx < max_materials_per_mesh);
        std::string mat_name = atts.getString("name", "");
        const CMaterial *m = material_manager.getByName(mat_name.c_str());
        mats[curr_loading_material_idx] = m;
        ++curr_loading_material_idx;
    }
}

//------------------------------------------------------------------
// Update of the component
//------------------------------------------------------------------
void CRenderable::update( float delta )
{
    CAABB lAABB = getAABB();
    mIsCulled = !camera_manager.free_camera.getFrustum().IsVisible(lAABB);
}

//------------------------------------------------------------------
// Upload to GPU
//------------------------------------------------------------------
void CRenderable::uploadToGPU( CRenderTechnique* aRenderTech )
{
    bool lIsOk = true;
    ConstantBufferObjectViewSpace cb;
    const CCamera lCamera = camera_manager.free_camera;
    const XMMATRIX lWorld = this->getOwner()->get< CTransform >()->getWorld();
    cb.WorldView          = lWorld * lCamera.getView();
    cb.WorldViewProj      = lWorld * lCamera.getViewProjection();
    lIsOk = lIsOk & aRenderTech->setParam("ConstantBufferObjectViewSpace", cb);
    assert(lIsOk);
    DrawUtils::setWorldMatrix(lWorld);
}

//------------------------------------------------------------------
// Get the AABB in world space
//------------------------------------------------------------------
CAABB CRenderable::getAABB() const {
    assert(mesh);
    CTransform *transform = (CTransform *)getOwner()->get<CTransform>();
    return CAABB(mesh->getAABB(), transform->getWorld());
}

//------------------------------------------------------------------
// Render the AABB of the mesh
//------------------------------------------------------------------
void CRenderable::renderDebug3D() {
    DrawUtils::draw(getAABB(), XMFLOAT4(1.f, 1.f, 0.f, 1.f));
}

// TODO: 
/**
 * 
 * Export the component
 <renderable mesh="sponza_377">
     <material name="Material__25" />
     <material name="Material__298" />
 </renderable>
 */
void CRenderable::exportComp(ofstream &xmlfile){

}
