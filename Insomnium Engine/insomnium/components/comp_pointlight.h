#ifndef _INC_COMP_POINTLIGHT_
#define _INC_COMP_POINTLIGHT_

#include "components.h"

class CPointlight : public CComponent {
private:
    unsigned           unique_id;    // in render manager

public:
    XMFLOAT4    color;
    float       min_radius;
    float       max_radius;

    CPointlight();
    static void registerMsgs();
    void update( float delta );
    void onEntityCreated(const TMsg &msg);
    void onStartElement(const std::string &elem, MKeyValue &atts);
    void exportComp(ofstream &xmlfile);
};

#endif