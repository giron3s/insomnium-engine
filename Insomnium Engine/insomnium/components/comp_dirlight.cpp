#include "mcv_platform.h"
#include "comp_dirlight.h"
#include "render_manager.h"
#include "XMLParser.h"

DECL_COMP_MANAGER( CDirlight, "dirlight" );

CDirlight::CDirlight(){
    unique_id = 0;
    direction = XMFLOAT3(0., 1., 0.);
    directional_color = XMFLOAT4(1., 1., 1., 1.);
    directional_intensity = 1.;
}

void CDirlight::registerMsgs( ) {
    SUBSCRIBE(CDirlight, MSG_ENTITY_CREATED, onEntityCreated);
}

void CDirlight::onEntityCreated(const TMsg &msg) {
    render_manager.light_system.registerDirlightToRender(this);
}

void CDirlight::update(float dt){

}

/**
 * Load the attributes from the xml file
 */
void CDirlight::onStartElement(const std::string &elem, MKeyValue &atts){
    direction = atts.getFloat3("direction");
    directional_color = atts.getFloat4("color");
    directional_intensity = atts.getFloat("intensity", 1.);
}

/**
 * Export to the xml file
 */
void CDirlight::exportComp(ofstream &xmlfile){
    stringstream ss(stringstream::in | stringstream::out);
    
    ss << "\t<dirlight";
    ss << " direction=\"" << direction.x << " " << direction.y << " " << direction.z;
    ss << " color=\"" << directional_color.x << " " << directional_color.y << " ";
    ss << directional_color.z << " " << directional_color.w;
    ss << " intensity=\"" << directional_intensity << "\"/>";
    xmlfile << ss.str() << endl;
}


