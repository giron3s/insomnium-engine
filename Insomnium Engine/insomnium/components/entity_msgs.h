#ifndef INC_ENTITY_MSGS_H_
#define INC_ENTITY_MSGS_H_

class CEntity;

// -----------------------------
enum eMsgType {
  MSG_INVALID
, MSG_ENTITY_CREATED
, MSG_IMPACT
, MSG_EXPLODE
, MSG_DIED
// , ..
, MSG_COUNT
};

struct TMsg {

  eMsgType msg_type;

  TMsg( ) : msg_type( MSG_INVALID ) { }
  TMsg( eMsgType atype ) : msg_type( atype ) { }

  union {

    /// --------------------
    struct {
      int weapon_id;
      CEntity *sender;
    } impact;

    /// --------------------
    struct {
      CEntity *killer;
      float    distance;
      float    superhit;
    } died;
  };

  /// --------------------
  static TMsg createImpact( int weapon_id, CEntity *sender ) {
    TMsg msg;
    msg.msg_type = MSG_IMPACT;
    msg.impact.sender = sender;
    msg.impact.weapon_id = weapon_id;
    return msg;
  }
  static TMsg createDie( ) {
    TMsg msg;
    msg.msg_type = MSG_DIED;
    msg.died.distance = 1.0f;
    msg.died.killer = NULL;
    msg.died.superhit = 2.0f;
    return msg;
  }
};



#endif
