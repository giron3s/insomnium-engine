#ifndef _INC_COMP_SKYLIGHT_
#define _INC_COMP_SKYLIGHT_

#include "components.h"
class CSkylight : public CComponent {
private:
    unsigned           unique_id;    // in render manager

public:
    XMFLOAT4 ambient_color;
    float    ambient_intensity;

    CSkylight();
    static void registerMsgs();

    void update(float dt);
    void onStartElement(const std::string &elem, MKeyValue &atts);
    void onEntityCreated(const TMsg &msg);
    void exportComp(ofstream &xmlfile);
};

#endif