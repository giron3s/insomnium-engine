#include "mcv_platform.h"
#include "utils.h"
#include "components\comp_transform.h"
#include "components\comp_renderable.h"
#include "components\comp_rigid_animation.h"
#include "components\comp_dirlight.h"
#include "components\comp_pointlight.h"
#include "components\comp_explode.h"
#include "components\comp_skylight.h"

// ------------------------------------------------------
void initComponents() {
  INIT_COMP_MANAGER(CTransform,    1024 );
  INIT_COMP_MANAGER(CRenderable,   1024 );
  INIT_COMP_MANAGER(CPointlight,   1024 );
  INIT_COMP_MANAGER(CSkylight,     1024 );
  INIT_COMP_MANAGER(CDirlight,     1024 );
  INIT_COMP_MANAGER(CRigidAnimation, 32);
  INIT_COMP_MANAGER(CExplode, 32);
}
