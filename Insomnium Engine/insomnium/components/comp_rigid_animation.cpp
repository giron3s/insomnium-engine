#include "mcv_platform.h"
#include "XMLParser.h"
#include "comp_rigid_animation.h"
#include "entity_manager.h"

DECL_COMP_MANAGER( CRigidAnimation, "rigid_animation" );

CRigidAnimation::CRigidAnimation( ) 
  : core_anim( NULL )
  , curr_time( 0.f )
{ }

void CRigidAnimation::registerMsgs( ) {

}


void CRigidAnimation::update( float dt ) {
  
  assert( core_anim );
  
  curr_time += dt;
  if( curr_time > core_anim->header.total_time ) {
    curr_time = core_anim->header.total_time;
    return;
  }

  core_anim->apply(curr_time, targets, getOwner()->get<CTransform>()->getWorld());
}

// ---------------------------------------------
void CRigidAnimation::findTargets() {
  assert( core_anim );
  
  targets.resize( core_anim->names.size() );

  // Para cada nombre en la defincion de la animacion
  for( size_t i=0; i<core_anim->names.size(); ++i ) {

    // buscar el objecto en la escena por nombre
    targets[ i ] = entity_manager.get( core_anim->names[ i ].name );
  }
}

// ---------------------------------------------
CEntity *findByName( VEntities &entities, const char *name ) {
  VEntities::iterator i = entities.begin();
  while( i != entities.end() ) {
    if( (*i)->isNamed( name ) ) 
      return *i;
    ++i;
  }
  return NULL;
}

// ---------------------------------------------
void CRigidAnimation::findTargets( VEntities &entities ) {
  assert( core_anim );
  
  targets.resize( core_anim->names.size() );

  // Para cada nombre en la defincion de la animacion
  for( size_t i=0; i<core_anim->names.size(); ++i ) {

    // buscar el objecto en la escena por nombre
    targets[ i ] = findByName( entities, core_anim->names[ i ].name );
  }
}

// ---------------------------------------------
void CRigidAnimation::setCore(const std::string &name) {
  assert(!name.empty());
  core_anim = rigid_animation_manager.getByName(name.c_str());
}


// ---------------------------------------------
void CRigidAnimation::onStartElement( const std::string &elem, MKeyValue &atts ) {
  std::string name = atts.getString( "name", "" );
  setCore(name);
  // Asume que en este punto los objetos de la escena que voy a usar
  // ya se han definido en la escena
  findTargets( );
}


void CRigidAnimation::exportComp(ofstream &xmlfile){

}

