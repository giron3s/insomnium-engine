#include "mcv_platform.h"
#include "debug_manager.h"
#include "render_constants.h"
#include "render_manager.h"

//Wireframe
void TW_CALL getWireFrame(void *value, void *clientData){
    *static_cast<bool *>(value) = debug_manager::IsDebugWireFrame();
}

void TW_CALL setWireFrame(const void *value, void *clientData){
    debug_manager::SetDebugWireFrame( *static_cast<const bool *>(value) );
}


//AABB
void TW_CALL getAABB(void *value, void *clientData){
    *static_cast<bool *>(value) = debug_manager::IsDebugAABB();
}

void TW_CALL setAABB(const void *value, void *clientData){
    debug_manager::SetDebugAABB( *static_cast<const bool*>(value) );
}

//Axis
void TW_CALL getAxis(void *value, void *clientData){
    *static_cast<bool *>(value) = debug_manager::IsDebugAxis();
}

void TW_CALL setAxis(const void *value, void *clientData){
    debug_manager::SetDebugAxis( *static_cast<const bool *>(value) );
}


//Grid
void TW_CALL getGrid(void *value, void *clientData){
    *static_cast<bool *>(value) = debug_manager::IsDebugGrid();
}

void TW_CALL setGrid(const void *value, void *clientData){
    debug_manager::SetDebugGrid (*static_cast<const bool *>(value));

}

//RT
void TW_CALL getDeferredRT(void *value, void *clientData){
    *static_cast<bool *>(value) = debug_manager::IsDebugDeferred();
}

void TW_CALL setDeferredRT(const void *value, void *clientData){
    debug_manager::SetDebugDeferred( *static_cast<const bool *>(value) );
}


/***************************************************************************************************************
*                               AMBIENT LIGHTS CALLBACKS
****************************************************************************************************************/

//COLOR
void TW_CALL getAmbientColor(void *value, void *clientData){
    CSkylight *skylight = static_cast<CSkylight *>(clientData);
    XMVECTOR xnaColor = XMLoadFloat4(&(skylight->ambient_color));
    xnaColor *= 255.;

    int red = XMVectorGetX(xnaColor);
    int green = XMVectorGetY(xnaColor);
    int blue = XMVectorGetZ(xnaColor);
    int alpha = XMVectorGetW(xnaColor);

    unsigned int bgColor = D3DCOLOR_RGBA((int)blue, (int)green, (int)red, (int)alpha);
    *static_cast<unsigned int *>(value) = bgColor;
}

void TW_CALL setAmbientColor(const void *value, void *clientData){
    CSkylight *skylight = static_cast<CSkylight *>(clientData);
    unsigned int bgColor = *(const unsigned int *)value;

    int red = bgColor & 0xff;
    int green = (bgColor & (0xff << 8)) >> 8;
    int blue = (bgColor & (0xff << 16)) >> 16;
    int alpha = (bgColor & (0xff << 24)) >> 24;
    XMVECTOR xnaColor = XMVectorSet(red, green, blue, alpha);
    xnaColor /= 255;
    XMFLOAT4 color;
    XMStoreFloat4(&color, xnaColor);
    skylight->ambient_color = color;
}


void TW_CALL getAmbientIntensity(void *value, void *clientData){
    CSkylight *skylight = static_cast<CSkylight *>(clientData);
    *static_cast<float *>(value) = skylight->ambient_intensity;
}

void TW_CALL setAmbientIntensity(const void *value, void *clientData){
    CSkylight *skylight = static_cast<CSkylight *>(clientData);
    skylight->ambient_intensity = *static_cast<const float *>(value);
}

/***************************************************************************************************************
*                               DIRECTIONAL LIGHTS CALLBACKS
****************************************************************************************************************/
static void TW_CALL getDirectionalDirection(void *value, void * clientData) {
    CDirlight* dirlight = static_cast<CDirlight *>(clientData);
    double *dir = (double*)(value);
    dir[0] = XMVectorGetX(XMLoadFloat3( &dirlight->direction));
    dir[1] = XMVectorGetY(XMLoadFloat3(&dirlight->direction));
    dir[2] = XMVectorGetZ(XMLoadFloat3(&dirlight->direction));
}


static void TW_CALL setDirectionalDirection(const void *value, void *clientData) {
    const double *dir = (const double *)(value);
    CDirlight *dirlight = static_cast<CDirlight *>(clientData);
    dirlight->direction = XMFLOAT3(dir[0], dir[1], dir[2]);
}

//COLOR
void TW_CALL getDirectionalColor(void *value, void *clientData){
    CDirlight *dirlight = static_cast<CDirlight *>(clientData);
    XMVECTOR xnaColor = XMLoadFloat4(&(dirlight->directional_color));
    xnaColor *= 255.;

    int red = XMVectorGetX(xnaColor);
    int green = XMVectorGetY(xnaColor);
    int blue = XMVectorGetZ(xnaColor);
    int alpha = XMVectorGetW(xnaColor);

    unsigned int bgColor = D3DCOLOR_RGBA((int)blue, (int)green, (int)red, (int)alpha);
    *static_cast<unsigned int *>(value) = bgColor;
}

void TW_CALL setDirectionalColor(const void *value, void *clientData){
    CDirlight *dirlight = static_cast<CDirlight *>(clientData);
    unsigned int bgColor = *(const unsigned int *)value;

    int red = bgColor & 0xff;
    int green = (bgColor & (0xff << 8)) >> 8;
    int blue = (bgColor & (0xff << 16)) >> 16;
    int alpha = (bgColor & (0xff << 24)) >> 24;
    XMVECTOR xnaColor = XMVectorSet(red, green, blue, alpha);
    xnaColor /= 255;
    XMFLOAT4 color;
    XMStoreFloat4(&color, xnaColor);
    dirlight->directional_color = color;
}


void TW_CALL getDirectionalIntensity(void *value, void *clientData){
    CDirlight *dirlight = static_cast<CDirlight *>(clientData);
    *static_cast<float *>(value) = dirlight->directional_intensity;
}

void TW_CALL setDirectionalIntensity(const void *value, void *clientData){
    CDirlight *dirlight = static_cast<CDirlight *>(clientData);
    dirlight->directional_intensity = *static_cast<const float *>(value);
}


/***************************************************************************************************************
*                               POINTLIGHTS CALLBACKS
****************************************************************************************************************/

//COLOR
void TW_CALL getPointlightColor(void *value, void *clientData){
    CPointlight *pointlight = static_cast<CPointlight *>(clientData);
    XMVECTOR xnaColor = XMLoadFloat4(&(pointlight->color));
    xnaColor *= 255.;

    int red = XMVectorGetX(xnaColor);
    int green = XMVectorGetY(xnaColor);
    int blue = XMVectorGetZ(xnaColor);
    int alpha = XMVectorGetW(xnaColor);

    unsigned int bgColor = D3DCOLOR_RGBA((int)blue, (int)green, (int)red, (int)alpha);
    *static_cast<unsigned int *>(value) = bgColor;
}

void TW_CALL setPointlightColor(const void *value, void *clientData){
    CPointlight *pointlight = static_cast<CPointlight *>(clientData);
    unsigned int bgColor = *(const unsigned int *)value;

    int red = bgColor & 0xff;
    int green = (bgColor & (0xff << 8)) >> 8;
    int blue = (bgColor & (0xff << 16)) >> 16;
    int alpha = (bgColor & (0xff << 24)) >> 24;
    XMVECTOR xnaColor = XMVectorSet(red, green, blue, alpha);
    xnaColor /= 255;
    XMFLOAT4 color;
    XMStoreFloat4(&color, xnaColor);
    pointlight->color = color;
}


void TW_CALL getPointlightMin(void *value, void *clientData){
    CPointlight *pointlight = static_cast<CPointlight *>(clientData);
    *static_cast<float *>(value) = pointlight->min_radius;
}

void TW_CALL setPointlightMin(const void *value, void *clientData){
    CPointlight *pointlight = static_cast<CPointlight *>(clientData);
    pointlight->min_radius = *static_cast<const float *>(value);
}

void TW_CALL getPointlightMax(void *value, void *clientData){
    CPointlight *pointlight = static_cast<CPointlight *>(clientData);
    *static_cast<float *>(value) = pointlight->max_radius;
}

void TW_CALL setPointlightMax(const void *value, void *clientData){
    CPointlight *pointlight = static_cast<CPointlight *>(clientData);
    pointlight->max_radius = *static_cast<const float *>(value);
}

//Export lights
void TW_CALL exportLights(void *clientData){
    render_manager.light_system.exportEntities();
}

// Callback function to create a bar with a given title
void TW_CALL CreateRenderBarCB(void *clientData){
    TwBar *bar = TwNewBar("Render TweakBar");
    TwDefine("label='Render TweakBar' position='16 16' size='260 600'");

    
    //Toogle vars
    TwAddButton(bar, "Toogle vars", NULL, NULL, " label='Toogle variables' ");
    TwAddVarCB(bar, "WireFrame", TW_TYPE_BOOL8, setWireFrame, getWireFrame, NULL, " label='WireFrame'");
    TwAddVarCB(bar, "AABB", TW_TYPE_BOOL8, setAABB, getAABB, NULL, " label='AABB'");
    TwAddVarCB(bar, "Axis", TW_TYPE_BOOL8, setAxis, getAxis, NULL, " label='Axis'");
    TwAddVarCB(bar, "Grid", TW_TYPE_BOOL8, setGrid, getGrid, NULL, " label='Grid'");


    //Render textures
    TwAddButton(bar, "white_space_1", NULL, NULL, "label=' '");
    TwAddButton(bar, "Deferred RTs", NULL, NULL, " label='Deferred RTs' ");
    TwAddVarCB(bar, "RT enable", TW_TYPE_BOOL8, setDeferredRT, getDeferredRT, NULL, " label='RT enable'");
    TwEnumVal rendertextureEV[] = { { TRenderTextures::RT_ALBEDO, "ALBEDO" },
                                    { TRenderTextures::RT_NORMAL, "NORMAL" },
                                    { TRenderTextures::RT_SPECULAR, "SPECULAR" },
                                    { TRenderTextures::RT_DISPLACEMENT, "DISPLACEMENT" },
                                    { TRenderTextures::RT_EXTRA, "VIEW POS" },
                                    { TRenderTextures::RT_REFLECTION, "REFLECTION" },
                                    { TRenderTextures::RT_ZBUFFER, "Z-BUFFER" },
                                    { TRenderTextures::RT_ACC_LIGHT, "ACC_LIGHT" } };

    TwType rt_Type = TwDefineEnum("RenderTexture Mode", rendertextureEV, 8);
    TwAddVarRW(bar, "RenderTexture", rt_Type, &debug_manager::mDebugFrameID, "");

    TwAddButton(bar, "white_space_2", NULL, NULL, "label=' '");
    TwAddButton(bar, "Lights vars", NULL, NULL, " label='Lights variables' ");
    TwAddButton(bar, "Export lights", exportLights, NULL, " label='Export lights' ");
    
    CSkylight *skylight = render_manager.light_system.skylight;
    TwAddVarCB(bar, "Ambient color", TW_TYPE_COLOR32, setAmbientColor, getAmbientColor, skylight,               " label='Ambient color' ");
    TwAddVarCB(bar, "Ambient intensity", TW_TYPE_FLOAT, setAmbientIntensity, getAmbientIntensity, skylight, " min=0.00 max=1 step=0.0005 precision=5 label='Ambient intensity' ");
        
    TwAddButton(bar, "white_space_3", NULL, NULL, "label=' '");
    TwAddButton(bar, "Directionals", NULL, NULL, " label='Directionals  ' ");


    vector<CDirlight *> dirlights = render_manager.light_system.dirlights;
    for (int i = 0; i < dirlights.size(); i++){

        CDirlight *current_dirlight = dirlights.at(i);

        char spotlightName[128];
        _snprintf(spotlightName, sizeof(spotlightName), "%s", current_dirlight->getOwner()->getName());
        string groupName("group='" + string(current_dirlight->getOwner()->getName()) + "' ");

        TwAddVarCB(bar, string(string(current_dirlight->getOwner()->getName()) + "_direction").c_str(), TW_TYPE_DIR3D, setDirectionalDirection, getDirectionalDirection, current_dirlight, " label='direction' ");
        TwAddVarCB(bar, string(string(current_dirlight->getOwner()->getName()) + "_color").c_str(),     TW_TYPE_COLOR32, setDirectionalColor, getDirectionalColor, current_dirlight, "label='color'");
        TwAddVarCB(bar, string(string(current_dirlight->getOwner()->getName()) + "_intensity").c_str(), TW_TYPE_FLOAT, setDirectionalIntensity, getDirectionalIntensity, current_dirlight, "label='intensity' min=0 max=1 step=0.005");
    }


    TwAddButton(bar, "white_space_4", NULL, NULL, "label=' '");
    TwAddButton(bar, "Pointlights", NULL, NULL, " label='Pointlights' ");

    vector<CPointlight *> pointlights = render_manager.light_system.pointlights;
    for (int i = 0; i < pointlights.size(); i++){

        CPointlight *current_pointlight = pointlights.at(i);

        char spotlightName[128];
        _snprintf(spotlightName, sizeof(spotlightName), "%s", current_pointlight->getOwner()->getName());
        string groupName("group='" + string(current_pointlight->getOwner()->getName()) + "' ");

        TwAddVarCB(bar, string(string(current_pointlight->getOwner()->getName()) + "_min").c_str(), TW_TYPE_FLOAT, setPointlightMin, getPointlightMin, current_pointlight, string( groupName + " label='min radius' min=0 step=0.005").c_str() );
        TwAddVarCB(bar, string(string(current_pointlight->getOwner()->getName()) + "_max").c_str(), TW_TYPE_FLOAT, setPointlightMax, getPointlightMax, current_pointlight, string(groupName + " label='max radius' min=0 step=0.005").c_str() );
        TwAddVarCB(bar, string(string(current_pointlight->getOwner()->getName()) + "_color").c_str(), TW_TYPE_COLOR32, setPointlightColor, getPointlightColor, current_pointlight, string(groupName + " label='color'").c_str());
    }
}