#include "mcv_platform.h"
#include "shader.h"
#include "vertex_declarations.h"
#include "utils.h"
#include "draw_utils.h"
#include "render_manager.h"
#include "render_techniques_manager.h"
#include "texture.h"


const CVertexShader *CVertexShader::current = NULL;

CShader::TParam::TParam() : nbytes(0), slot(0), dx11_cte_buffer(NULL) {
    memset(name, 0x00, sizeof(name));
}

// Buscar linealmente en la lista de params de este shader
// uno con el nombre que me han dado, o NULL si no esta
const CShader::TParam *CShader::getParamByName(const char *name) const {
    VParams::const_iterator i = params.begin();
    while (i != params.end()) {
        if (strcmp(i->name, name) == 0)
            return &(*i);
        ++i;
    }
    return NULL;
}


// Buscar linealmente en la lista de params de este shader
// uno con el nombre que me han dado, o NULL si no esta
CShader::TTextureParam *CShader::getTextureParamByName(const char *name) {
    VTextureParams::iterator i = texture_params.begin();
    while (i != texture_params.end()) {
        if (strcmp(i->name, name) == 0)
            return &(*i);
        ++i;
    }
    return NULL;
}

void CShader::scanParams(ID3DBlob* pBlob) {
    HRESULT hr;

    // From the compiled data, ask for the reflection information
    void *compiled_data = pBlob->GetBufferPointer();
    size_t compiled_size = pBlob->GetBufferSize();
    D3DReflect(compiled_data, compiled_size, IID_ID3D11ShaderReflection, (void**)&reflector);
    assert(reflector);

    D3D11_SHADER_DESC shader_desc;
    reflector->GetDesc(&shader_desc);

    for (unsigned i = 0; i < shader_desc.ConstantBuffers; ++i) {
        ID3D11ShaderReflectionConstantBuffer *cte_buffer = reflector->GetConstantBufferByIndex(i);

        // Buffer desc
        D3D11_SHADER_BUFFER_DESC cte_buff_desc;
        hr = cte_buffer->GetDesc(&cte_buff_desc);
        assert(!FAILED(hr));
        const char *name = cte_buff_desc.Name;

        // Bind info
        D3D11_SHADER_INPUT_BIND_DESC bind_desc;
        hr = reflector->GetResourceBindingDescByName(name, &bind_desc);

        utils::logger::dbg("Shader param %d has name %s and takes %ld bytes at slot %d\n", i, name, cte_buff_desc.Size, bind_desc.BindPoint);

        TParam p;
        strcpy(p.name, name);
        p.nbytes = cte_buff_desc.Size;
        p.slot = bind_desc.BindPoint;

        // Already owned by DrawUtils
        if (strcmp(name, "ConstantBufferCamera") == 0) {
            p.dx11_cte_buffer = g_pConstantBufferCamera;
            p.owned_by_shader = false;
        }
        else if (strcmp(name, "ConstantBufferObject") == 0) {
            p.dx11_cte_buffer = g_pConstantBufferObject;
            p.owned_by_shader = false;
        }
        else if (strcmp(name, "ConstantBufferLight") == 0) {
            p.dx11_cte_buffer = g_pConstantBufferLight;
            p.owned_by_shader = false;

        }
        else {

            // Create a constant buffers to hold the buffer
            D3D11_BUFFER_DESC bd;
            ZeroMemory(&bd, sizeof(bd));
            bd.Usage = D3D11_USAGE_DEFAULT;
            bd.ByteWidth = cte_buff_desc.Size;
            bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
            bd.CPUAccessFlags = 0;
            hr = App.d3dDevice->CreateBuffer(&bd, NULL, &p.dx11_cte_buffer);
            assert(!FAILED(hr));
            p.owned_by_shader = true;

        }
        params.push_back(p);
    }

    // resources
    for (unsigned i = 0; i < shader_desc.BoundResources; ++i) {
        // Bind info
        D3D11_SHADER_INPUT_BIND_DESC bind_desc;
        hr = reflector->GetResourceBindingDesc(i, &bind_desc);

        // Acordarnos de las texturas
        if (bind_desc.Type == D3D_SIT_TEXTURE) {
            utils::logger::dbg("Shader resource %d has name %s at point %d is a texture!!\n", i, bind_desc.Name, bind_desc.BindPoint);
            TTextureParam tp;
            memset(&tp, 0x00, sizeof(tp));
            strcpy(tp.name, bind_desc.Name);
            tp.slot = bind_desc.BindPoint;
            texture_params.push_back(tp);
        }
        else {
            utils::logger::dbg("Shader resource %d has name %s at point %d Type:%d\n", i, bind_desc.Name, bind_desc.BindPoint, bind_desc.Type);
        }

    }

    reflector->Release();
}

//--------------------------------------------------------------------------------------
// Helper for compiling shaders with D3DX11
//--------------------------------------------------------------------------------------
HRESULT CompileShaderFromFile(const char* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut)
{
    HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

    // Para evitar tener que hacer el tranpose cada vez que subo
    // una matriz, y mantener el order point * Matrix
    dwShaderFlags |= D3D10_SHADER_PACK_MATRIX_ROW_MAJOR;

#if defined( DEBUG ) || defined( _DEBUG )
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* pErrorBlob = NULL;
    hr = D3DX11CompileFromFile(szFileName, NULL, NULL
        , szEntryPoint, szShaderModel,
        dwShaderFlags, 0, NULL, ppBlobOut, &pErrorBlob, NULL);
    if (FAILED(hr))
    {
        if (pErrorBlob != NULL)
            OutputDebugString((char*)pErrorBlob->GetBufferPointer());
        if (pErrorBlob) pErrorBlob->Release();
        return hr;
    }
    if (pErrorBlob) pErrorBlob->Release();

    return S_OK;
}

// -------------------------------------------
CVertexShader::CVertexShader()
    : vs(NULL)
    , vertex_layout(NULL)
    , vertex_decl(NULL)
{ }

bool CVertexShader::compile(const char *filename
    , const char *entry_point
    , const CVertexDeclaration *avtx_decl
    ) {
    HRESULT hr;

    vertex_decl = avtx_decl;

    ID3DBlob* pVSBlob = NULL;
    hr = CompileShaderFromFile(filename, entry_point, "vs_5_0", &pVSBlob);
    if (FAILED(hr))
    {
        char buffer[512];
        _snprintf(buffer, 512, "The FX file %s file cannot be compiled. Please run this executable from the directory that contains the FX file.", filename);
        MessageBox(NULL, buffer, "Error", MB_OK);
        return false;
    }

    // Create the vertex shader
    hr = App.d3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &vs);
    if (FAILED(hr))
    {
        pVSBlob->Release();
        return false;
    }

    // Create the input layout
    hr = App.d3dDevice->CreateInputLayout(vertex_decl->elems
        , vertex_decl->nelems
        , pVSBlob->GetBufferPointer(),
        pVSBlob->GetBufferSize(), &vertex_layout);

    if (FAILED(hr)) {
        pVSBlob->Release();
        utils::logger::fatal("CreateInputLayout failed\n");
        return false;
    }
    setDbgName(vs, filename);

    scanParams(pVSBlob);

    pVSBlob->Release();
    return true;
}

void CVertexShader::destroy() {
    if (vs) {
        vs->Release();
        vs = NULL;
    }
    if (vertex_layout)
        vertex_layout->Release();
    vertex_layout = NULL;

}

void CVertexShader::activate() const {
    assert(vertex_layout);
    assert(vs);
    App.immediateContext->IASetInputLayout(vertex_layout);
    App.immediateContext->VSSetShader(vs, NULL, 0);

    current = this;
}

// -----------------------------
CPixelShader::CPixelShader()

{ }

void CPixelShader::activate() const
{
    App.immediateContext->PSSetShader(ps, NULL, 0);
}

bool CPixelShader::compile(const char *filename
    , const char *entry_point
    ) {

    // shadows tech don't require any pixel shader
    if (entry_point == NULL)
        return NULL;

    HRESULT hr;
    ID3DBlob* pVSBlob = NULL;

    // Compile the pixel shader
    ID3DBlob* pPSBlob = NULL;
    hr = CompileShaderFromFile(filename, entry_point, "ps_5_0", &pPSBlob);
    if (FAILED(hr))
    {
        MessageBox(NULL,
            "The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK);
        return false;
    }

    // Create the pixel shaderCreatePixelShader
    hr = App.d3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &ps);
    if (FAILED(hr))
        return false;

    scanParams(pPSBlob);
    setDbgName(ps, filename);

    pPSBlob->Release();
    return true;
}

void CPixelShader::destroy() {
    if (ps) {
        ps->Release();
        ps = NULL;
    }
}

// ------------------------------------------
ID3D11SamplerState* CShader::sampler_linear_wrap = NULL;
ID3D11SamplerState* CShader::sampler_linear_clamp = NULL;

bool CShader::createSamplers() {
    HRESULT  hr;
    // Create the sample state
    D3D11_SAMPLER_DESC sampDesc;
    ZeroMemory(&sampDesc, sizeof(sampDesc));
    sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.MinLOD = 0;
    sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
    hr = App.d3dDevice->CreateSamplerState(&sampDesc, &sampler_linear_wrap);
    if (FAILED(hr))
        return false;
    setDbgName(sampler_linear_wrap, "SamplerLinearWrap");

    ZeroMemory(&sampDesc, sizeof(sampDesc));
    sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.MinLOD = 0;
    sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
    hr = App.d3dDevice->CreateSamplerState(&sampDesc, &sampler_linear_clamp);
    if (FAILED(hr))
        return false;
    setDbgName(sampler_linear_clamp, "SamplerLinearClamp");
    return true;
}

void CShader::destroySamplers() {
    if (sampler_linear_wrap)
        sampler_linear_wrap->Release(), sampler_linear_wrap = NULL;
    if (sampler_linear_clamp)
        sampler_linear_clamp->Release(), sampler_linear_clamp = NULL;
}


// --------------
bool CRenderTechnique::create(  const char *aname
                                , const char *shader
                                , const char *vs_function
                                , const char *ps_function
                                , const CVertexDeclaration *avtx_decl
                                , TRenderTechniqueGroup atech_group
                                , TBlendConfig ablend_config ) {

    name = aname;
    bool is_ok = vs.compile(shader, vs_function, avtx_decl);
    is_ok &= ps.compile(shader, ps_function);
    uses_bones = vs.getParamByName("ConstantBufferBones") != NULL;

    // Register myself in the manager
    render_techniques_manager.registerTechnique(this);
    blend_config = ablend_config;
    group = atech_group;
    return is_ok;
}

void CRenderTechnique::activate() const {
    vs.activate();
    ps.activate();

    // para todos los param
    // si es owned by me, set
    for (size_t i = 0; i < vs.params.size(); ++i) {
        int start_slot = vs.params[i].slot;
        App.immediateContext->VSSetConstantBuffers(start_slot, 1, &vs.params[i].dx11_cte_buffer);
    }
    for (size_t i = 0; i < ps.params.size(); ++i) {
        int start_slot = ps.params[i].slot;
        App.immediateContext->PSSetConstantBuffers(start_slot, 1, &ps.params[i].dx11_cte_buffer);
    }

    CShader::VTextureParams::const_iterator tp = ps.texture_params.begin();
    while (tp != ps.texture_params.end()) {
        if (tp->current_value)
            App.immediateContext->PSSetShaderResources(tp->slot, 1, &tp->current_value->mResourceView);
        ++tp;
    }


    // Activate samplers of the tech
    App.immediateContext->PSSetSamplers(0, 1, &CShader::sampler_linear_wrap);
    App.immediateContext->PSSetSamplers(1, 1, &CShader::sampler_linear_clamp);

    render_manager.setBlendConfig(blend_config);
}

bool CRenderTechnique::setTexture(const char *name, const CTexture *texture) {
    CShader::TTextureParam *pps = ps.getTextureParamByName(name);
    if (pps)
    {
        utils::profiler::graphics::mRenderTextureCount++;
        utils::profiler::graphics::mRenderTextureSize += texture->GetMemSize();
        pps->current_value = texture;
    }
    else
    {
        utils::logger::dbg("Hey, tech %s does not use texture %s\n", getName(), name);
        assert(true);
    }
    return pps != NULL;
}


bool CRenderTechnique::setParamDataBytes(const char *name, const void *data, size_t nbytes) const {
    // Check in the vs shader
    const CShader::TParam *pvs = vs.getParamByName(name);
    if (pvs) {
        assert(pvs->nbytes == nbytes);
        assert(pvs->dx11_cte_buffer != NULL);
        App.immediateContext->UpdateSubresource(pvs->dx11_cte_buffer, 0, NULL, data, 0, 0);
    }

    const CShader::TParam *pps = ps.getParamByName(name);
    if (pps)
        App.immediateContext->UpdateSubresource(pps->dx11_cte_buffer, 0, NULL, data, 0, 0);

    return pvs || pps;
}
