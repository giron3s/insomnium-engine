#include "mcv_platform.h"
#include "utils.h"
#include <cstdio>
#include <cstdarg>


namespace utils{
    void Init()
    {
        profiler::fps::Init();
        profiler::graphics::Init();
    }
    void Update(){
        profiler::fps::Update();
    }
}


// TODO remove the next functions
float randomFloat( float vmin, float vmax ) {
  return vmin + ( vmax - vmin ) * unitRandom();
}

float unitRandom( ) {
  return (float) rand() / (float) RAND_MAX;
}

// --------------------------------------------

bool keyIsPressed( int key ) {
  return (::GetAsyncKeyState( key ) & 0x8000 ) != 0;
}

bool keyBecomesPressed( int key ) {
  return (::GetAsyncKeyState( key ) & 1 ) == 1;
}
