#ifndef _CAMERA_MANAGER_
#define _CAMERA_MANAGER_

#include "camera.h"
#include "entity_controllers.h"
class CRenderTechnique; 

class CCameraManager{

public:
    CCamera                     ortho_camera;

    CCamera                     free_camera;
    CEntity                     *free_camera_entity;
    CFreeCameraController       free_cam_controller;

    CCamera                     light_camera;
    CEntity                     *light_camera_entity;


    bool init();
    void update                     ( float delta );
    void uploadCameraToGPU          ( const CCamera* aCamera );
    void uploadCameraParamsToGPU    ( CRenderTechnique *aRenderTech ); 
    void destroy();
};

extern CCameraManager camera_manager;
#endif 