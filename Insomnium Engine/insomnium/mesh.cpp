#include "mcv_platform.h"
#include "mesh.h"
#include "shader.h"
#include "data_provider.h"
#include "utils.h"
#include "vertex_declarations.h"

CRenderMesh::CRenderMesh()
    : vb(NULL)
    , ib(NULL)
    , primitive_type(D3D_PRIMITIVE_TOPOLOGY_UNDEFINED)
    , nvertexs(0)
    , nindices(0)
    , ngroups_used(0)
    , bytes_per_vertex(0)
{ }

void CRenderMesh::destroy() {
    if (vb) vb->Release(), vb = NULL;
    if (ib) ib->Release(), ib = NULL;
}

bool CRenderMesh::createRawMesh(const void *  new_vertexs
    , unsigned      new_nvertexs
    , unsigned      new_nbytes_per_vertex
    , const TIndex *new_indices
    , unsigned      new_nindices
    , ePrimitiveType new_type
    , const CVertexDeclaration *adecl
    , bool           is_updateable
    ) {

    assert(vb == NULL);

    // Save params
    nvertexs = new_nvertexs;
    nindices = new_nindices;
    bytes_per_vertex = new_nbytes_per_vertex;
    mVertexDeclaration = adecl;

    // Translate primitive type from our system to dx
    switch (new_type) {
    case POINT_LIST: primitive_type = D3D11_PRIMITIVE_TOPOLOGY_POINTLIST; break;
    case LINE_LIST:  primitive_type = D3D11_PRIMITIVE_TOPOLOGY_LINELIST; break;
    case TRIANGLE_LIST: primitive_type = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST; break;
    case TRIANGLE_STRIP: primitive_type = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP; break;
    default:
        utils::logger::fatal("Unknown primitive type %d\n", new_type);
        break;
    }

    HRESULT hr;

    // Create the vertex buffer for read-only 
    D3D11_BUFFER_DESC bd;
    ZeroMemory(&bd, sizeof(bd));
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = new_nbytes_per_vertex * new_nvertexs;
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bd.CPUAccessFlags = 0;

    // Change flags if the user wants to update the buffer in the future
    if (is_updateable) {
        bd.Usage = D3D11_USAGE_DYNAMIC;
        bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    }

    D3D11_SUBRESOURCE_DATA *p_init_data = NULL;
    D3D11_SUBRESOURCE_DATA InitData;
    if (new_vertexs) {
        ZeroMemory(&InitData, sizeof(InitData));
        InitData.pSysMem = new_vertexs;
        p_init_data = &InitData;
    }
    hr = App.d3dDevice->CreateBuffer(&bd, p_init_data, &vb);
    if (FAILED(hr))
        return false;

    if (new_nindices > 0) {
        assert(new_indices);
        // Create index buffer
        bd.Usage = D3D11_USAGE_DEFAULT;
        bd.ByteWidth = sizeof(TIndex) * new_nindices;
        bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
        bd.CPUAccessFlags = 0;
        InitData.pSysMem = new_indices;
        hr = App.d3dDevice->CreateBuffer(&bd, &InitData, &ib);
        if (FAILED(hr))
            return false;
    }
    else {
        ib = NULL;
    }

    // Define the defaul group
    groups[0].first_index = 0;
    groups[0].nindices = new_nindices;
    ngroups_used = 1;

    return true;
}

// -----------------------------------------------------
bool CRenderMesh::updateFromCPU(const void *new_data, unsigned nbytes) {
    assert(vb);
    D3D11_MAPPED_SUBRESOURCE MappedResource;
    HRESULT hr = App.immediateContext->Map(vb, 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedResource);
    assert(!FAILED(hr));
    assert(MappedResource.pData);
    memcpy(MappedResource.pData, new_data, nbytes);
    App.immediateContext->Unmap(vb, 0);
    return true;
}


// -----------------------------------------------------
void CRenderMesh::render() const {


    assert(vb != NULL);

    //Profiller
    utils::profiler::graphics::mDrawCalls++;
    utils::profiler::graphics::mVertexCount += nvertexs;
    // El vertex shader activo tiene que usar la misma vertex
    // declaration que estoy mandando yo
    assert(CVertexShader::current->vertex_decl == mVertexDeclaration);

    // Set vertex buffer
    UINT stride = bytes_per_vertex;
    UINT offset = 0;
    App.immediateContext->IASetVertexBuffers(0, 1, &vb, &stride, &offset);

    // Set primitive topology
    App.immediateContext->IASetPrimitiveTopology(primitive_type);

    if (ib) {
        // Set index buffer
        DXGI_FORMAT fmt = (sizeof(TIndex) == 2) ? DXGI_FORMAT_R16_UINT : DXGI_FORMAT_R32_UINT;
        App.immediateContext->IASetIndexBuffer(ib, fmt, 0);
        App.immediateContext->DrawIndexed(nindices, 0, 0);        // 36 vertices needed for 12 triangles in a triangle list
    }
    else {
        // A non-indexed primitive
        App.immediateContext->Draw(nvertexs, 0);
    }
}

// -----------------------------------------------------
void CRenderMesh::renderGroup(unsigned group_idx) const {

    assert(group_idx < ngroups_used);
    assert(vb != NULL);

    //Profiler
    utils::profiler::graphics::mDrawCalls++;
    utils::profiler::graphics::mVertexCount += nvertexs;

    // El vertex shader activo tiene que usar la misma vertex
    // declaration que estoy mandando yo
    assert(CVertexShader::current->vertex_decl == mVertexDeclaration);

    // Set vertex buffer
    UINT stride = bytes_per_vertex;
    UINT offset = 0;
    App.immediateContext->IASetVertexBuffers(0, 1, &vb, &stride, &offset);

    // Set primitive topology
    App.immediateContext->IASetPrimitiveTopology(primitive_type);

    const TGroup &g = groups[group_idx];

    if (ib) {
        // Set index buffer
        DXGI_FORMAT fmt = (sizeof(TIndex) == 2) ? DXGI_FORMAT_R16_UINT : DXGI_FORMAT_R32_UINT;
        App.immediateContext->IASetIndexBuffer(ib, fmt, 0);
        App.immediateContext->DrawIndexed(g.nindices, g.first_index, 0);        // 36 vertices needed for 12 triangles in a triangle list
    }
    else {
        // A non-indexed primitive
        App.immediateContext->Draw(g.nindices, g.first_index);
    }
}

// ------------------------------------------------
bool CRenderMesh::load(CDataProvider &dp) {

    // Leer la cabecera
    THeader h;
    dp.read(h);
    if (!h.isValid()) {
        utils::logger::dbg("RenderMesh header is not valid\n");
        return false;
    }

    // Convertir el h.vertex_type en el equivalente vertex_declaration
    const CVertexDeclaration* file_vertex_decl = NULL;
    if (h.vertex_type == 1000)
        file_vertex_decl = CVertexTextured::getDecl();
    else if (h.vertex_type == 1001)
        file_vertex_decl = &vtx_dcl_textured2;
    else if (h.vertex_type == 1010)
        file_vertex_decl = &vtx_dcl_normalmap;
    else {
        utils::logger::dbg("Invalid vertex type %d read from file\n", h.vertex_type);
        return false;
    }

    // Update aabb
    mAABB.SetCenter(XMVectorSet(h.aabb_center[0], h.aabb_center[1], h.aabb_center[2], h.aabb_center[3]));
    mAABB.SetHalfSize(XMVectorSet(h.aabb_half_size[0], h.aabb_half_size[1], h.aabb_half_size[2], h.aabb_half_size[3]));

    // Los datos van en 'chunks' predecidos por una cabecera que dice el tipo de datos que viene
    struct TChunkHeader {
        unsigned magic;
        unsigned bytes;
    };

    u8 *vtxs = NULL;
    TIndex *idxs = NULL;

    static const unsigned magic_vertexs_chunk = 0x33998899;
    static const unsigned magic_indices_chunk = 0x33998888;
    static const unsigned magic_groups_chunk = 0x33998822;
    static const unsigned magic_end_of_file_chunk = 0x33998877;

    while (true) {
        TChunkHeader chunk_header;
        dp.read(chunk_header);

        if (chunk_header.magic == magic_vertexs_chunk) {
            // Vienen los vertices
            size_t bytes_for_vtxs = h.bytes_per_vertex * h.nvertexs;
            assert(bytes_for_vtxs == chunk_header.bytes);

            // Pedir memoria para los vertices
            vtxs = new u8[bytes_for_vtxs];

            // Leerlos del dp
            dp.read(vtxs, bytes_for_vtxs);

        }
        else if (chunk_header.magic == magic_indices_chunk) {
            // Vienen los indices
            assert(h.bytes_per_index == sizeof(TIndex));
            size_t bytes_for_idxs = h.bytes_per_index * h.nindices;
            assert(bytes_for_idxs == chunk_header.bytes);

            // Pedir memoria para los vertices
            idxs = new TIndex[h.nindices];

            // Leerlos del dp
            dp.read(idxs, bytes_for_idxs);

        }
        else if (chunk_header.magic == magic_groups_chunk) {
            dp.read(ngroups_used);
            assert(ngroups_used <= max_groups_supported);
            if (ngroups_used > max_groups_supported)
                return false;
            dp.read(groups, ngroups_used * sizeof(TGroup));

        }
        else if (chunk_header.magic == magic_end_of_file_chunk) {
            // Chunk final. Salimos!
            break;

        }
        else {
            //assert( !"Invalid chunk header\n" );
        }
    }

    bool is_ok = createRawMesh(vtxs
        , h.nvertexs
        , h.bytes_per_vertex
        , idxs
        , h.nindices
        , TRIANGLE_LIST
        , file_vertex_decl
        , false
        );
    mRenderGroup = static_cast<TRenderTechniqueGroup>(h.render_group);

    delete[] vtxs;
    delete[] idxs;
    setDbgName(vb, dp.getName());
    setDbgName(ib, dp.getName());

    return is_ok;
}

