#ifndef INC_DRAW_UTILS_H_
#define INC_DRAW_UTILS_H_

#include "mesh.h"
#include "font.h"

class CCamera;
class CAABB;
class CTexture;
class CRenderTechnique;

namespace DrawUtils {

  bool createDrawUtils();
  void destroyDrawUtils();
  void drawAxis();
  void drawGrid();
  void drawIcosahedron();
  void drawLine( XMVECTOR a, XMVECTOR b, XMFLOAT4 clr );
  void drawCubeTextured();
  void drawCameraFrustum( const CCamera &frustum_to_render );
  void draw(const CAABB &aabb, XMFLOAT4 color);
  void drawTextured2D(int xmin, int ymin, int w, int h, CTexture *t, CRenderTechnique *tech = NULL); 

  void activateReflection(CRenderTechnique *tech, XMMATRIX reflection, const CCamera &camera);
  void activateDefaultConstantBuffers( );
  void activateCamera( const CCamera &camera );
  void setWorldMatrix( const XMMATRIX &new_world );
  void activateFullScreenOrthoCamera();
  void activateLight(const CCamera &camera);

  extern TFont       dbg_font;

  // to be deleted...
  extern CRenderMesh mesh_axis;
  extern CRenderMesh mesh_cube_textured;

}

// To be removed...
#include "data/shaders/constants/buffer_camera.h"
#include "data/shaders/constants/buffer_object.h"
#include "data/shaders/constants/buffer_light.h"

extern ID3D11Buffer*           g_pConstantBufferCamera;
extern ID3D11Buffer*           g_pConstantBufferObject;
extern ID3D11Buffer*           g_pConstantBufferLight;

#endif
