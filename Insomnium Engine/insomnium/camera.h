#ifndef INC_CAMERA_H_
#define INC_CAMERA_H_

#include "mcv_platform.h"
#include "frustum.h"

class CCamera {
    enum ProjectionType{
        PERSPECTIVE,
        ORTHO
    };

    // View info
    XMVECTOR            pos;
    XMVECTOR            target;
    XMVECTOR            up_aux;        // Defaults to 0,1,0
    XMVECTOR            front, left, up;


    // Perspective projection
    float               fov;           // en radianes
    float               aspect_ratio;  // w / h
    float               znear, zfar;

    // Ortho projection
    float               xMin;
    float               xMax;
    float               yMin;
    float               yMax;
    float               zMin;
    float               zMax;

    XMMATRIX            view;
    XMMATRIX            projection;
    XMMATRIX            view_projection;

    XMMATRIX            reflection;
    CFrustum            mFrustum;

    ProjectionType      projectionType;
    void                updateViewMatrix();
    void                updateProjectionMatrix();

public:
    CCamera();
    void                loadOrtho(float xmin, float xmax, float ymin, float ymax, float zmin, float zmax);
    void                lookAt(XMVECTOR new_pos, XMVECTOR new_target);
    void                updatePerspective(float new_fov, float new_aspect_ratio, float new_znear, float new_zfar);
    void                updateOrtho(float xMin, float xMax, float yMin, float yMax, float zMin, float zMax);


    float               getAspectRatio() const { return aspect_ratio; }
    float               getFov() const { return fov; }
    float               getZNear() const { return znear; }
    float               getZFar() const { return zfar; }

    const XMMATRIX      getView()  const { return view; }
    const XMMATRIX      getProjection() const { return projection; }
    const XMMATRIX      getViewProjection() const { return view_projection; }

    XMVECTOR            getPosition() const { return pos; }
    XMVECTOR            getTarget() const { return target; }
    XMVECTOR            getFront() const { return front; }

    // Returns true if the world_pos is inside the z range of the view frustum 
    bool                getScreenCoords(XMVECTOR world_pos, float *x, float *y) const;
    
    CFrustum            getFrustum() const { return mFrustum; }
};
#endif

