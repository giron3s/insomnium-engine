#include "logger.h"
#include "mcv_platform.h"

namespace utils{
    namespace logger{

        void dbg(const char *fmt, ...) {
            va_list args;
            va_start(args, fmt);

            char buf[2048];
            int n = vsnprintf(buf, sizeof(buf), fmt, args);

            // Confirm the msg fits in the given buffer
            if (n < 0)
                buf[sizeof(buf) - 1] = 0x00;

            // Redirect to windows dbg console
            ::OutputDebugString(buf);
        }


        bool fatal(const char *fmt, ...) {
            va_list args;
            va_start(args, fmt);

            char buf[2048];
            int n = vsnprintf(buf, sizeof(buf), fmt, args);

            // Confirm the msg fits in the given buffer
            if (n < 0)
                buf[sizeof(buf) - 1] = 0x00;

            // Redirect to windows dbg console
            ::OutputDebugString(buf);
            ::MessageBoxA(NULL, buf, "Error", MB_OK);
            return false;
        }
    }
}