#ifndef INC_SHADOWS_SYSTEM_H_
#define INC_SHADOWS_SYSTEM_H_

#include "mcv_platform.h"
#include "render_texture.h"
#include "shader.h"
#include "aabb.h"
#include "camera.h"
#include "render_constants.h"

class CShadowsSystem {
private:
    int                    dimension;
    XMMATRIX               cameraProj[SHADOW_MAP_CASCADE_COUNT];

    float                  cascadeSplit[SHADOW_MAP_CASCADE_COUNT];
    XMMATRIX               cascadeView[SHADOW_MAP_CASCADE_COUNT];
    XMMATRIX               cascadeProj[SHADOW_MAP_CASCADE_COUNT];
    CRenderTexture         rt_zmap[SHADOW_MAP_CASCADE_COUNT];

    ID3D11SamplerState    *sample_state;
    ID3D11RasterizerState *rasterize_state;

    void   generate(const XMMATRIX view, const XMMATRIX proj, int cascade_i); 
public:
    CShadowsSystem();
    bool init(int dimension);
    void destroy();
    void generate(const CCamera &camera, const CCamera& light);

    void uploadParamsShadowToGPU(CRenderTechnique &tech);
    void uploadParamsLightToGPU(const XMMATRIX view, const XMMATRIX proj);

    CRenderTexture &getRT(){ return rt_zmap[0]; }
    CRenderTexture &getRT(int cascade_index){ return rt_zmap[cascade_index]; }
};


#endif
