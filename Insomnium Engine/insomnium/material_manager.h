#ifndef INC_MATERIAL_MANAGER_H_
#define INC_MATERIAL_MANAGER_H_

//--------------------------------------------------------------------------------------
#include "generic_manager.h"
#include "data_provider.h"
#include "material.h"
#include "XMLParser.h"

class CMaterialManager : public CObjManager<CMaterial> {
public:
  void onStartElement (const std::string &elem, MKeyValue &atts);
};
extern CMaterialManager material_manager;

#endif
