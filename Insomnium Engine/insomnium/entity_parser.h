#ifndef INC_ENTITY_PARSER_H_
#define INC_ENTITY_PARSER_H_

#include "XMLParser.h"
#include "components/components.h"

class CEntity;
class CComponent;
class ICompManagerBase;

class CEntityParser : public CXMLParser {
  CEntity          *curr_entity;
  CComponent       *curr_component;
  ICompManagerBase *curr_base;

  void exportEntities(vector<CEntity *> entities, ofstream& xmlfile);

public:
  CEntityParser( ) : curr_entity( NULL ), curr_component( NULL ), curr_base( NULL ) { }
  void onStartElement (const std::string &elem, MKeyValue &atts);
  void onEndElement (const std::string &elem);

  void exportLights(vector<CEntity *> entities);
  void exportScene(vector<CEntity *> entities);
  VEntities entities_loaded;

};

#endif
