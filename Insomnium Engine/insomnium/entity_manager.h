#ifndef INC_ENTITY_MANAGER_H_
#define INC_ENTITY_MANAGER_H_

#include "components/components.h"
#include <vector>
#include <map>

class CEntityManager {
  VEntities all_entities;
  typedef std::map< std::string, CEntity * > MEntitiesByName;
  MEntitiesByName entities_by_name;

public:

  CEntity *get( const std::string &name );
  void add( CEntity *e, const std::string &name );
  void add( CEntity *e );

};

extern CEntityManager entity_manager;

#endif
