#include "mcv_platform.h"
#include "render_manager.h"
#include "render_techniques_manager.h"
#include "camera.h"
#include "camera_manager.h"
#include "draw_utils.h"
#include "components/comp_renderable.h"
#include "components/comp_transform.h"
#include "shader.h"
#include "material.h"
#include "utils.h"
#include "technique_parser.h"

CRenderManager render_manager;

CRenderManager::CRenderManager() : seq_unique_id(1){ }

//------------------------------------------------------------------
// Init the render manager
//------------------------------------------------------------------
bool CRenderManager::init( int xres, int yres)
{
    bool is_ok = true;

    //Create the stencils
    is_ok = is_ok & DrawUtils::createDrawUtils();
    is_ok = is_ok & createDepthStencilStates();
    is_ok = is_ok & createBlendingStates();
    is_ok = is_ok & createRasterizerStates();

    //Load the render technique from the xml
    CTechniqueParser parser;
    is_ok &= parser.xmlParseFile("data/techniques.xml");

    //Create the render textures
    is_ok = is_ok & rts[RT_ZBUFFER].create("zbuffer", xres, yres, DXGI_FORMAT_R16_UNORM, DXGI_FORMAT_UNKNOWN);
    is_ok = is_ok & rts[RT_NORMAL].create("normal", xres, yres, DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_UNKNOWN);
    is_ok = is_ok & rts[RT_ALBEDO].create("albedo", xres, yres, DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_UNKNOWN);
    is_ok = is_ok & rts[RT_DISPLACEMENT].create("displacement", xres, yres, DXGI_FORMAT_R16_UNORM, DXGI_FORMAT_UNKNOWN);
    is_ok = is_ok & rts[RT_EXTRA].create("extra", xres, yres, DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_UNKNOWN);
    is_ok = is_ok & rts[RT_SPECULAR].create("specular", xres, yres, DXGI_FORMAT_R16_UNORM, DXGI_FORMAT_UNKNOWN);
    is_ok = is_ok & rts[RT_REFLECTION].create("reflection", xres, yres, DXGI_FORMAT_R16_UNORM, DXGI_FORMAT_UNKNOWN);
    is_ok = is_ok & rts[RT_ACC_LIGHT].create("deferred.accLight", xres, yres, DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_UNKNOWN);
    assert(is_ok);

    //Init the world, light and shadows
    is_ok = is_ok & world_system.init();
    is_ok = is_ok & light_system.init();
    is_ok = is_ok & shadows_system.init(512);
    is_ok = is_ok & postprocess_system.init();
    assert(is_ok);
    return is_ok;
}

//------------------------------------------------------------------
// Create the raster states
//------------------------------------------------------------------
bool CRenderManager::createRasterizerStates()
{
    HRESULT hr = S_OK;

    // Description of the resterization
    D3D11_RASTERIZER_DESC rsDesc;
    ZeroMemory(&rsDesc, sizeof(D3D11_RASTERIZER_DESC));

    // Configuracion de un descriptor para un modo de rasterizado solido
    rsDesc.FillMode = D3D11_FILL_SOLID;
    rsDesc.CullMode = D3D11_CULL_FRONT;
    rsDesc.FrontCounterClockwise = TRUE;
    rsDesc.DepthClipEnable = TRUE;

    // RASTERIZADO SOLIDO (FRONT) --> No dibuja triangulos que son front-facing
    hr = App.d3dDevice->CreateRasterizerState(&rsDesc, &raster_cfs[RASTER_FRONT_SOLID]);
    if (FAILED(hr))
        return false;

    // Descriptor para un modo de rasterizado "wired"
    rsDesc.FillMode = D3D11_FILL_WIREFRAME;
    // RASTERIZADO WIRED --> No dibuja triangulos que son front-facing
    hr = App.d3dDevice->CreateRasterizerState(&rsDesc, &raster_cfs[RASTER_FRONT_WIREFRAME]);
    if (FAILED(hr))
        return false;

    // RASTERIZADO SOLIDO (BACK) --> No dibuja triangulos que son back-facing
    rsDesc.FillMode = D3D11_FILL_SOLID;
    rsDesc.CullMode = D3D11_CULL_BACK;
    rsDesc.FrontCounterClockwise = TRUE;
    hr = App.d3dDevice->CreateRasterizerState(&rsDesc, &raster_cfs[RASTER_BACK_SOLID]);
    if (FAILED(hr))
        return false;

    // RASTERIZADO SIN CULLING (Se dibujan los triangulos front facing y back facing)
    rsDesc.CullMode = D3D11_CULL_NONE;
    hr = App.d3dDevice->CreateRasterizerState(&rsDesc, &raster_cfs[RASTER_CULL_DISABLED]);
    if (FAILED(hr))
        return false;

    // RASTERIZADO WIRED SIN CULLING (Se dibujan los triangulos front facing y back facing)
    rsDesc.FillMode = D3D11_FILL_WIREFRAME;
    rsDesc.CullMode = D3D11_CULL_NONE;
    hr = App.d3dDevice->CreateRasterizerState(&rsDesc, &raster_cfs[RASTER_NO_CULL_WIREFRAME]);
    if (FAILED(hr))
        return false;

    // RASTERIZADO PARA SOMBRAS
    //rsDesc.CullMode = D3D11_CULL_BACK;
    rsDesc.FillMode = D3D11_FILL_SOLID;
    rsDesc.CullMode = D3D11_CULL_NONE;
    rsDesc.FrontCounterClockwise = FALSE;
    rsDesc.DepthBias = 13;
    rsDesc.DepthBiasClamp = 0.f;
    rsDesc.SlopeScaledDepthBias = 2.f;
    rsDesc.DepthClipEnable = TRUE;
    rsDesc.ScissorEnable = FALSE;
    rsDesc.MultisampleEnable = FALSE;
    rsDesc.AntialiasedLineEnable = FALSE;
    hr = App.d3dDevice->CreateRasterizerState(&rsDesc, &raster_cfs[RASTER_SHADOWS]);
    if (FAILED(hr))
        return false;


    // Note: Define such that we still cull backfaces by making front faces CCW.
    // If we did not cull backfaces, then we have to worry about the BackFace
    // property in the D3D11_DEPTH_STENCIL_DESC.
    ZeroMemory(&rsDesc, sizeof(D3D11_RASTERIZER_DESC));
    rsDesc.FillMode = D3D11_FILL_SOLID;
    rsDesc.CullMode = D3D11_CULL_BACK;
    rsDesc.FrontCounterClockwise = true;
    rsDesc.DepthClipEnable = true;

    hr = App.d3dDevice->CreateRasterizerState(&rsDesc, &raster_cfs[RASTER_CULL_CLOCKWISE]);
    if (FAILED(hr)){
        return false;
    }

    App.immediateContext->RSSetState(raster_cfs[RASTER_FRONT_SOLID]);
    return true;
}


//------------------------------------------------------------------
// Create the blending states
//------------------------------------------------------------------
bool CRenderManager::createBlendingStates()
{
    HRESULT hr;
    D3D11_BLEND_DESC desc;
    memset(&desc, 0x00, sizeof(desc));

    // Blend Disabled
    desc.RenderTarget[0].BlendEnable = FALSE;
    desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
    hr = App.d3dDevice->CreateBlendState(&desc, &blend_cfs[Z_DISABLE_ALL]);
    if (FAILED(hr))
        return false;
    setDbgName(blend_cfs[BLEND_DISABLED], "BLEND_DISABLED");

    // Combinative, opacity depends on pixel shader alfa
    desc.RenderTarget[0].BlendEnable = TRUE;
    desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
    desc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
    desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_SRC_ALPHA;
    desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
    hr = App.d3dDevice->CreateBlendState(&desc, &blend_cfs[BLEND_ALPHA]);
    if (FAILED(hr))
        return false;
    setDbgName(blend_cfs[BLEND_ALPHA], "BLEND_ALPHA");

    // Additive
    desc.RenderTarget[0].BlendEnable = TRUE;
    desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
    desc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
    hr = App.d3dDevice->CreateBlendState(&desc, &blend_cfs[BLEND_ADDITIVE]);
    if (FAILED(hr))
        return false;
    setDbgName(blend_cfs[BLEND_ADDITIVE], "BLEND_ADDITIVE");

    // No render target write
    memset(&desc, 0x00, sizeof(desc));
    desc.AlphaToCoverageEnable = false;
    desc.IndependentBlendEnable = false;

    desc.RenderTarget[0].BlendEnable = false;
    desc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
    desc.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
    desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    desc.RenderTarget[0].RenderTargetWriteMask = 0;

    hr = App.d3dDevice->CreateBlendState(&desc, &blend_cfs[BLEND_NO_WRITE]);
    if (FAILED(hr)){
        return false;
    }
    setDbgName(blend_cfs[BLEND_NO_WRITE], "BLEND_NO_WRITE");


    // TransparentBS
    memset(&desc, 0x00, sizeof(desc));
    desc.AlphaToCoverageEnable = false;
    desc.IndependentBlendEnable = false;

    desc.RenderTarget[0].BlendEnable = true;
    desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
    desc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
    desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

    hr = App.d3dDevice->CreateBlendState(&desc, &blend_cfs[BLEND_TRANSPARENT]);
    if (FAILED(hr)){
        return false;
    }
    setDbgName(blend_cfs[BLEND_TRANSPARENT], "BLEND_TRANSPARENT");

    return true;
}

//------------------------------------------------------------------
// Create the depth stencils
//------------------------------------------------------------------
bool  CRenderManager::createDepthStencilStates()
{
    D3D11_DEPTH_STENCIL_DESC desc;
    memset(&desc, 0x00, sizeof(desc));
    desc.DepthEnable = FALSE;
    desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
    desc.DepthFunc = D3D11_COMPARISON_ALWAYS;
    desc.StencilEnable = FALSE;
    HRESULT hr;
    hr = App.d3dDevice->CreateDepthStencilState(&desc, &z_cfs[Z_DISABLE_ALL]);
    if (FAILED(hr))
        return false;
    setDbgName(z_cfs[Z_DISABLE_ALL], "Z_DISABLE_ALL");

    // Default app, only pass those which are near than the previous samples
    memset(&desc, 0x00, sizeof(desc));
    desc.DepthEnable = TRUE;
    desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
    desc.DepthFunc = D3D11_COMPARISON_LESS;
    desc.StencilEnable = FALSE;
    hr = App.d3dDevice->CreateDepthStencilState(&desc, &z_cfs[Z_DEFAULT]);
    if (FAILED(hr))
        return false;
    setDbgName(z_cfs[Z_DEFAULT], "Z_DEFAULT");

    // Testing the Z at the depth buffer, but it don't write it
    memset(&desc, 0x00, sizeof(desc));
    desc.DepthEnable = TRUE;
    desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
    desc.DepthFunc = D3D11_COMPARISON_LESS;
    desc.StencilEnable = FALSE;
    hr = App.d3dDevice->CreateDepthStencilState(&desc, &z_cfs[Z_TEST_ON_WRITE_OFF]);
    if (FAILED(hr))
        return false;
    setDbgName(z_cfs[Z_TEST_ON_WRITE_OFF], "Z_TEST_ON_WRITE_OFF");

    // Testing the Z at the depth buffer, and the source data is greater than the destination data
    memset(&desc, 0x00, sizeof(desc));
    desc.DepthEnable = TRUE;
    desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
    desc.DepthFunc = D3D11_COMPARISON_GREATER_EQUAL;
    desc.StencilEnable = FALSE;
    hr = App.d3dDevice->CreateDepthStencilState(&desc, &z_cfs[Z_TEST_INVERSE_WRITE_OFF]);
    if (FAILED(hr))
        return false;
    setDbgName(z_cfs[Z_TEST_INVERSE_WRITE_OFF], "Z_TEST_INVERSE_WRITE_OFF");

    //mark the reflection zone to the stencil buffer
    memset(&desc, 0x00, sizeof(desc));
    desc.DepthEnable = true;
    desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
    desc.DepthFunc = D3D11_COMPARISON_LESS;
    desc.StencilEnable = true;
    desc.StencilReadMask = 0xff;
    desc.StencilWriteMask = 0xff;
    
    desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
    desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_REPLACE;
    desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

    // We are not rendering backfacing polygons, so these settings do not matter.
    desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
    desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_REPLACE;
    desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

    hr = App.d3dDevice->CreateDepthStencilState(&desc, &z_cfs[Z_REFLECTION_MARK]);
    if (FAILED(hr)){
        return false;
    }
    setDbgName(z_cfs[Z_REFLECTION_MARK], "Z_REFLECTION_MARK");

    //Reflection
    memset(&desc, 0x00, sizeof(desc));
    desc.DepthEnable = true;
    desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
    desc.DepthFunc = D3D11_COMPARISON_LESS;
    desc.StencilEnable = true;
    desc.StencilReadMask = 0xff;
    desc.StencilWriteMask = 0xff;

    desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
    desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    desc.FrontFace.StencilFunc = D3D11_COMPARISON_EQUAL;

    // We are not rendering backfacing polygons, so these settings do not matter.
    desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
    desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    desc.BackFace.StencilFunc = D3D11_COMPARISON_EQUAL;

    hr = App.d3dDevice->CreateDepthStencilState(&desc, &z_cfs[Z_REFLECTION]);
    if (FAILED(hr)){
        return false;
    }
    setDbgName(z_cfs[Z_REFLECTION], "Z_REFLECTION");
    return true;
}


//------------------------------------------------------------------
// Bind the depth configs
//------------------------------------------------------------------
void CRenderManager::setDepthConfig( TZConfig config_id )
{
    assert(z_cfs[config_id] != NULL);

    // rendering the mirror to the stencil buffer, it follows that all the pixels in the stencil buffer will be 0 except for the pixels that correspond to the visible part of the mirror�they will have a 1.
    if (Z_REFLECTION_MARK == config_id || Z_REFLECTION == config_id)
    {
        App.immediateContext->OMSetDepthStencilState(z_cfs[config_id], 1);
    }
    else
    {
        App.immediateContext->OMSetDepthStencilState(z_cfs[config_id], 0);
    }
}

//------------------------------------------------------------------
// Bind the blend config
//------------------------------------------------------------------
void CRenderManager::setBlendConfig(TBlendConfig config_id)
{
    assert(blend_cfs[config_id] != NULL);
    App.immediateContext->OMSetBlendState(blend_cfs[config_id], 0, 0xffffffff);
}

//------------------------------------------------------------------
// Bind the raster state
//------------------------------------------------------------------
void CRenderManager::setRasterizerStates(TRasterizerConfig config_id)
{
    assert(raster_cfs[config_id] != NULL);
    App.immediateContext->RSSetState(raster_cfs[config_id]);
}

//------------------------------------------------------------------
// Sort the render keys
//------------------------------------------------------------------
bool sortRenderKeys(const CRenderManager::TRenderKey& key1, const CRenderManager::TRenderKey& key2)
{
    //Por shader
    //Por textura
    //Por mesh
    return key1.mat->mName < key2.mat->mName;
}

//------------------------------------------------------------------
// Register render keys
//------------------------------------------------------------------
unsigned CRenderManager::registerToRender( CRenderable *aRenderable )
{
    const CRenderMesh *lMesh = aRenderable->mesh;
    for (uint32 i = 0, lRenderKeys = lMesh->getGroupsCount(); i < lRenderKeys; ++i)
    {
        TRenderKey key;
        key.unique_id    = seq_unique_id;
        key.mesh         = lMesh;
        key.sub_mesh     = i;
        key.mat          = aRenderable->mats[i];
        key.owner        = aRenderable->getOwner();
        key.gbuffer_tech = render_techniques_manager.GetTechniqueByGroup(lMesh->GetTechniqueGroup(), lMesh->GetVertexDeclaration());
        assert(key.mat != NULL);
        keys.push_back(key);
    }
    
    std::sort( keys.begin(), keys.end(), sortRenderKeys);
    return seq_unique_id++;
}

void CRenderManager::unregisterFromRender(unsigned aunique_id) 
{
    if ( keys.empty() )
        return;
    size_t old_size = keys.size();
    VRenderKeys::iterator it = std::remove_if(keys.begin(), keys.end(), [&](const TRenderKey &k) { return k.unique_id == aunique_id; });
    keys.erase(it, keys.end());
}


void CRenderManager::render(CRenderTexture &target){
    //Clear the color, depth and stencil view of the back buffer
    float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
    App.immediateContext->ClearRenderTargetView(App.renderTargetView, ClearColor);
    App.immediateContext->ClearDepthStencilView(App.depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
    render_manager.setDepthConfig(Z_DEFAULT);

    //Generate the cascade shadows
    //shadows_system.generate(camera_manager.free_camera, camera_manager.light_camera);
    //const CRenderTechnique *ctech = render_techniques_manager.getByName("deferred.gbuffer");
    //CRenderTechnique *tech = const_cast<CRenderTechnique *>(ctech);
    //shadows_system.uploadParamsShadowToGPU(*tech);

//#ifndef NDEBUG
//    PROFILE_BEGIN(WORLD_SYSTEM);
//#endif
    //Render deferred
    CTraceScoped t0("Deferred");
    world_system.render();

//#ifndef NDEBUG
//    PROFILE_END();
//#endif
//
//#ifndef NDEBUG
//    PROFILE_BEGIN(LIGHT_SYSTEM);
//#endif
    //Lights
    //CTraceScoped t1("Lights");
    //light_system.render();

//#ifndef NDEBUG
//    PROFILE_END();
//#endif


//#ifndef NDEBUG
//    PROFILE_BEGIN(PP_SYSTEM);
//#endif
    //Render the postprocess 
    CTraceScoped t2("PP");
    postprocess_system.render(target);
//#ifndef NDEBUG
//    PROFILE_END();
//#endif
}

//------------------------------------------------------------------

//------------------------------------------------------------------
void CRenderManager::renderShadowCasters() {
    VRenderKeys::const_iterator k = keys.begin();
    while (k != keys.end()) {

        if (k->mat->cast_shadows) {
            DrawUtils::setWorldMatrix(k->owner->get< CTransform >()->getWorld());
            k->mesh->renderGroup(k->sub_mesh);
        }

        ++k;
    }
}

//------------------------------------------------------------------
// Used by deferred rendering
//------------------------------------------------------------------
void CRenderManager::renderSolidObjects( ) 
{
    VRenderKeys::iterator lCurrentKey = keys.begin();
    static TRenderKey lNullKey        = { NULL, 0, NULL, NULL, 0};
    const TRenderKey* lPrevKey        = &lNullKey;

    while ( lCurrentKey != keys.end() )
    {
        //Mesh is inside the frustum
        CRenderable *lRenderable = lCurrentKey->owner->get<CRenderable>();
        if ( !lRenderable->IsCulled() )
        {
            // Could skip if the previous technique uses the same mat
            if (lPrevKey->gbuffer_tech != lCurrentKey->gbuffer_tech )
            {
                lCurrentKey->gbuffer_tech->activate();
                camera_manager.uploadCameraParamsToGPU( lCurrentKey->gbuffer_tech );
            }

            // Could skip if the previous key uses the same mat
            if ( lPrevKey->mat != lCurrentKey->mat )
            {
                lCurrentKey->mat->activate();
            }

            //Bind world, view, projection to GPU
            lRenderable->uploadToGPU(lCurrentKey->gbuffer_tech);
            
            //Render object
            lCurrentKey->mesh->renderGroup(lCurrentKey->sub_mesh);
            lPrevKey = &(*lCurrentKey);

            //Profiler
            utils::profiler::graphics::mRenderedObjectsCount++;
        }
        else
        {
            //Profiler
            utils::profiler::graphics::mCulledObjectsCount++;
        }
        ++lCurrentKey;
    }
}

void CRenderManager::calculateSceneAABB()
{
    int minX = std::numeric_limits<int>::max();
    int maxX = std::numeric_limits<int>::min();
    int minY = std::numeric_limits<int>::max();
    int maxY = std::numeric_limits<int>::min();
    int minZ = std::numeric_limits<int>::max();
    int maxZ = std::numeric_limits<int>::min();

    const CRenderTechnique *ctech = render_techniques_manager.getByName("solid");
    ctech->activate();
    DrawUtils::setWorldMatrix(XMMatrixIdentity());

    VRenderKeys::const_iterator k = keys.begin();
    while (k != keys.end()) {
        CAABB aabb = k->owner->get<CRenderable>()->getAABB();
        CTransform *transform = k->owner->get<CTransform>();
        XMVECTOR minCorner = aabb.GetMinCorner();
        XMVECTOR maxCorner = aabb.GetMaxCorner();

        //Scene bounding box
        minX = minX > XMVectorGetX(minCorner) ? XMVectorGetX(minCorner) : minX;
        maxX = maxX < XMVectorGetX(maxCorner) ? XMVectorGetX(maxCorner) : maxX;
        minY = minY > XMVectorGetY(minCorner) ? XMVectorGetY(minCorner) : minY;
        maxY = maxY < XMVectorGetY(maxCorner) ? XMVectorGetY(maxCorner) : maxY;
        minZ = minZ > XMVectorGetZ(minCorner) ? XMVectorGetZ(minCorner) : minZ;
        maxZ = maxZ < XMVectorGetZ(maxCorner) ? XMVectorGetZ(maxCorner) : maxZ;

        //DrawUtils::setWorldMatrix(transform->getWorld());
        XMVECTOR lCenterPos = aabb.GetCenter();
        DrawUtils::setWorldMatrix(XMMatrixTranslation(XMVectorGetX(lCenterPos), XMVectorGetY(lCenterPos), XMVectorGetZ(lCenterPos)));
        DrawUtils::draw(k->mesh->getAABB(), XMFLOAT4(1.f, 0.f, 0.f, 1.f));
        DrawUtils::drawAxis();
        k++;
    }

    XMVECTOR min = XMVectorSet(minX, minY, minZ, 1.);
    XMVECTOR max = XMVectorSet(maxX, maxY, maxZ, 1.);
    XMVECTOR offset = (max - min) * .5;
    XMVECTOR center = (max + min) * .5;

    //Render the screen AABB
    //CAABB *sceenAABB = new CAABB();
    //sceenAABB->center = center;
    //sceenAABB->half_size = offset;
    //DrawUtils::draw(*sceenAABB, XMFLOAT4(1.f, 1.f, 0.f, 1.f));
}


void CRenderManager::destroy(){
    //destroyRasterizerStates
    for (int i = 0; i < RASTER_CONFIG_COUNT; i++){
        raster_cfs[i]->Release();
        raster_cfs[i] = 0;
    }

    //destroyBlendingStates
    for (int i = 0; i < BLEND_CONFIG_COUNT; i++){
        blend_cfs[i]->Release();
        blend_cfs[i] = 0;
    }

    //destroyDepthStencilStates
    for (int i = 0; i < Z_CONFIG_COUNT; i++){
        z_cfs[i]->Release();
        z_cfs[i] = 0;
    }

    //Destroy the rts
    rts[RT_ALBEDO].destroy();
    rts[RT_NORMAL].destroy();
    rts[RT_REFLECTION].destroy();
    rts[RT_ZBUFFER].destroy();
    rts[RT_ACC_LIGHT].destroy();

    //Destroy the systems
    world_system.destroy();
    light_system.destroy();
    //shadows_system.destroy();
}


//------------------------------------------------------------------
// DEPERCATED
// TODO REMOVE
//------------------------------------------------------------------
//void CRenderManager::render(CRenderTechnique *tech, const CCamera &camera) {
//
//    static TRenderKey null_key = { NULL, 0, NULL, NULL, 0 };
//    const TRenderKey* pk = &null_key;
//    VRenderKeys::const_iterator k = keys.begin();
//    while (k != keys.end()) {
//
//        if (tech == NULL || k->mat->tech == tech) {
//
//            // material changes?
//            if (pk->mat != k->mat) {
//
//                // The tech of the two material has changed?
//                if (pk->mat == NULL || pk->mat->tech != k->mat->tech)
//                    k->mat->tech->activate();
//
//                k->mat->activate();
//            }
//
//            // It's a differente owner/component?
//            if (pk->unique_id != k->unique_id) {
//
//                // Send the world * view and world * view
//                const XMMATRIX world = k->owner->get< CTransform >()->getWorld();
//                ConstantBufferObjectViewSpace obj_vs;
//                obj_vs.WorldView = world * camera.getView();
//                obj_vs.WorldViewProj = world * camera.getViewProjection();
//
//                DrawUtils::setWorldMatrix(k->owner->get< CTransform >()->getWorld());
//
//                bool is_ok = tech->setParam("ConstantBufferObjectViewSpace", obj_vs);
//            }
//
//            k->mesh->renderGroup(k->sub_mesh);
//            pk = &(*k);
//        }
//
//        ++k;
//    }
//}

//------------------------------------------------------------------
// DEPERCATED
// TODO REMOVE
// Used by deferred rendering
//------------------------------------------------------------------
//void CRenderManager::renderSolidObjects2(CRenderTechnique *tech, const CCamera &camera, XMMATRIX reflection) {
//    VRenderKeys::const_iterator k = keys.begin();
//    while (k != keys.end()) {
//
//        if (k->mat->cast_shadows) {
//            // Could skip if the previous key uses the same mat
//            k->mat->activate();
//
//            // Send the world * view and world * view
//            const XMMATRIX world = k->owner->get< CTransform >()->getWorld();
//            //ConstantBufferObjectViewSpace obj_vs;
//            //obj_vs.WorldView = world * reflection*  camera.getView();
//            //obj_vs.WorldViewProj = world * reflection * camera.getViewProjection();
//
//            DrawUtils::setWorldMatrix(k->owner->get< CTransform >()->getWorld() * reflection);
//
//            //bool is_ok = tech->setParam("ConstantBufferObjectViewSpace", obj_vs);
//            //assert(is_ok);
//
//            k->mesh->renderGroup(k->sub_mesh);
//        }
//
//        ++k;
//    }
//}
