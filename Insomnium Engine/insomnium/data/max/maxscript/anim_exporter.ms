clearListener()


fn exportAnimationSceneCore outfilename objs min_time max_time fps = (
	
	format "Exporing animation scene to %\n" outfilename
	
	-- Collect all objects
	local nobjs = objs.count
	
	-- Choose animation range
	local nsamples = max_time - min_time
	local total_time = nsamples / ( fps as float)
	local bytes_per_name = 32
	
	-- Create output file
	local fs = fopen outfilename "wb"
	if fs == undefined then throw ("Can't create output anim filename " + outfilename )
	
	-- Save header
	writeLong fs 0x6a6a6a6a		-- Magic
	writeLong fs 1						-- # Version
	writeLong fs nobjs                 
	writeLong fs nsamples

    writeLong fs bytes_per_name
	writeFloat fs total_time
	writeLong fs 0                           -- unused
	writeLong fs 0
	
	-- Save object names
	for obj in objs do (
		local name_len = obj.name.count
		if name_len > 31 then throw ("Name " + obj.name + " is too long")
		writeString fs obj.name
		
		-- End the 32 bytes with zeros
		local j = name_len + 1
		while j < bytes_per_name do (
			writeByte fs 0
			j = j + 1
		)
	)
	
	local max2mcv = rotateXMatrix -90 
	local mcv2max = rotateXMatrix 90 
	
	-- For each frame
	for f = 1 to nsamples do (
		-- For each obj
		for obj in objs do (
			-- Save pos / orientation of each object
			local mtx = at time f obj.transform
				
			-- Exportar los components transform y renderable
			local mtx_mcv_space = mcv2max * mtx * max2mcv 
			local q = mtx_mcv_space.rotationpart
			local t = mtx_mcv_space.translation
			
			writeFloat fs t.x
			writeFloat fs t.y
			writeFloat fs t.z
			writeFloat fs 0
			writeFloat fs q.x
			writeFloat fs q.y
			writeFloat fs q.z
			writeFloat fs -q.w
		)
	)
	
	fclose fs
)


fn exportAnimationScene = (
	local outfilename = "c:/code/mcv/mcv/data/rigid_anims/muro.anim"
	local objs = $*
	local min_time = animationrange.start
	local max_time = animationrange.end
	exportAnimationSceneCore outfilename objs min_time max_time framerate
)

--exportAnimationScene()