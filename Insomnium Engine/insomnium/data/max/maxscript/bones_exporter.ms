clearListener()

/*

# A poner en el user properties del root del biped
export_skel
export_mesh cavernicola
export_anim idle 0 32 30
end

basura 


*/


function isBone obj = (
	if matchPattern obj.name pattern:"*Nub" then return false
	return classof obj == Biped_Object and obj.wirecolor != black
)

function isRootBone obj = (
	return isBone obj and obj.parent == undefined
)

function getFirstRootBoneOfScene = (
	for p in $* do (
		if isRootBone p then return p
	)
	return undefined
)

function getChildrenBoneOf obj = (
	local new_children = #()
	if isBone obj then append new_children obj
	for k in obj.children do (
		local nietos = getChildrenBoneOf k
		join new_children nietos
	)
	return new_children
)


function test = (
	
	
	local root = getFirstRootBoneOfScene()
	if root == undefined then throw "No root skeletons found in the scene\n"
	local root_path = "c:/code/mcv/mcv/data/characters/" + root.name + "/"
	makedir root_path all:true
	local csf_filename = root_path + root.name + ".csf"
	local all_bones = getChildrenBoneOf root
	
	for b in all_bones do (
		format "Exporting bone -%-\n" b.name
	)
	
	local initial_figure_mode = root.controller.figureMode
	local buf = getuserpropbuffer root
	local ss = buf as stringStream 
	while not ( eof ss )do (
		local cmdline = readLine ss
		local sl = cmdline as stringStream 
		local cmd = readToken sl
		if cmd == undefined then continue
			
		-- Skip comments
		if matchPattern cmd pattern:"--*" then (
			format "Skipping line '%'\n" cmd
			continue
		)
		format "Parsing line %\n" cmdline

		if cmd == "end" then exit
		
		else if cmd == "export_skel" then (
			-- Buscar el csf_filename out a partir del root.name
			root.controller.figureMode = true
			ExportCalSkel csf_filename all_bones false
		) else if cmd == "export_anim" then (
			-- export_anim idle 0 30 25
			root.controller.figureMode = false
			local anim_name = readToken sl
			local caf_filename = root_path + anim_name + ".caf"
			local start_frame = (readToken sl) as integer
			local end_frame = (readToken sl) as integer
			local frame_offset = 0
			local fps = (readToken sl) as integer
			ExportCalAnim caf_filename csf_filename all_bones start_frame end_frame frame_offset fps 
		) else if cmd == "export_mesh" then (
			-- export_mesh cavernicola
			root.controller.figureMode = true
			local node_name = readToken sl
			local max_mesh = getNodeByName node_name
			if max_mesh == undefined then throw ("Can't find mesh " + node_name + " to export as cal mesh")
			local cmf_filename = root_path + node_name + ".cmf"
			ExportCalMesh cmf_filename csf_filename max_mesh 4 0.01 false false 
			
		) else if cmd == "export_movement" then (
			-- export_movement  idle 0 30 25
			local anim_name = readToken sl
			local node_name = readToken sl
			local max_mesh = getNodeByName node_name
			local anim_filename = root_path + anim_name + ".anim"
			local start_frame = (readToken sl) as integer
			local end_frame = (readToken sl) as integer
			local fps = (readToken sl) as integer
			exportAnimationSceneCore anim_filename #(max_mesh) start_frame end_frame fps
		) else (
			throw ("Invalid command " + cmd )
		)
		 
	)
	root.controller.figureMode = initial_figure_mode
	
)

test()










