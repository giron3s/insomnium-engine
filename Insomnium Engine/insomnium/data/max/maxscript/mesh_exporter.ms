clearListener()
gc()

fn areVtxsEqual v1 v2 = (
	local idx = 1
	for q in v1 do (
		if q != v2[ idx ] then return false
		idx = idx + 1
	)
	return true
)

-- Devuelve el indice de new_vtx en prev_vtxs
-- Si hace falta, lo registra en prev_vtxs
fn getIndexOfVertex strvtxs prev_vtxs new_vtx = (
	
	local sv = with printAllElements off new_vtx as string
	local idx = findItem strvtxs sv
	
	if idx > 0 then (
		return idx - 1
	)
	
/*	
	-- Buscar si new_vtx esta en prev_vtxs
	local idx = 1
	for v in prev_vtxs do (
		if areVtxsEqual v new_vtx then (
			-- Si existe, devolver la posicion en el array donde este
			return idx
		)
		idx = idx + 1
	)
	*/
	
	-- Si no, registrarlo en prev_vtxs y devolver el tama�o del array prev_vtxs
	-- pq lo he a�adido al final
	append prev_vtxs new_vtx
	append strvtxs  sv
	
	-- Menos 1 pq en c++ y dx11 trabajamos en base 0
	return prev_vtxs.count-1
)

fn getAABB vtxs = (
	
	if vtxs.count == 0 then throw "Invalid array at getAABB"
	local pmin, pmax
	local n = 0
	for vdata in vtxs do (
		local v =  [ vdata[1], vdata[2], vdata[3] ]
		if n == 0 then (
			pmin = copy v
			pmax = copy v
			n = 1
		) else (
			if v.x < pmin.x then pmin.x = v.x
			if v.y < pmin.y then pmin.y = v.y
			if v.z < pmin.z then pmin.z = v.z
			if v.x > pmax.x then pmax.x = v.x
			if v.y > pmax.y then pmax.y = v.y
			if v.z > pmax.z then pmax.z = v.z
		)
	)
	return #( pmin, pmax )
)

fn writeFloat4 f p3 = (
	writeFloat f p3.x
	writeFloat f p3.y
	writeFloat f p3.z
	writeFloat f 1.0
)

fn saveMeshToFile obj vtxs idxs channelB render_group_id= ( 
	local outpath = "c:\\code\\mcv\\mcv\\data\\meshes\\"
	
	-- Determinar el nombre del fichero
	local filename = outpath + obj.name + ".mesh"
	
	-- Abrir el fichero en modo binario
	local f = fopen filename "wb"
	if f == undefined then throw ( "Can't create output mesh file " + filename )
	
	local num_faces = getNumFaces obj
	
	-- Cada float ocupa 4 bytes. Cojo el 1er vtx como referencia
	-- de cuantos attributos tiene cada vertice de esta malla
	local bytes_per_vtx = vtxs[ 1 ].count * 4
	
	local vertex_type = 1010  -- Pos + 2UVs
	if( channelB > 0 ) then vertex_type = vertex_type + 1		
	
	local aabb = getAABB vtxs
	local aabb_center = (aabb[1] + aabb[2]) * 0.5
	local aabb_halfsize = (aabb[2] - aabb[1]) * 0.5

	-- Escribir una cabecera
	WriteLong f 0x33887755	-- Magic 
	WriteLong f 4			-- version number
	WriteLong f vtxs.count	-- # vertices
	WriteLong f num_faces	-- # faces
	WriteLong f idxs.count  -- # indices
	WriteLong f vertex_type    
	WriteLong f bytes_per_vtx -- # bytes_per_vertex
	WriteLong f 2  			    -- # bytes_per_index
	WriteLong f render_group_id  		 -- # bytes_per_render group
	writeFloat4 f aabb_center
	writeFloat4 f aabb_halfsize
	WriteLong f 0x33887755	-- Magic 
	
	-- Escribir vertices
	WriteLong f 0x33998899	-- Magic para vertices
	WriteLong f ( vtxs.count * bytes_per_vtx )	-- Bytes usados por los vertices sin cabecera
	for v in vtxs do (
		for c in v do (
			writeFloat f c
		)
	)
	
	-- Escribir indices como unsigned shorts, coherente con # bytes_per_index
	WriteLong f 0x33998888	-- Magic para indices
	WriteLong f ( idxs.count * 2 )	-- Bytes usados por los indices
	for i in idxs do (
		WriteShort  f i #unsigned
	)

	WriteLong f 0x33998877	-- Magic para fin de fichero
	WriteLong f 0
	
	-- Cerrar el fichero
	fclose f
)

fn getFacesPerMaterial obj = (
	local num_faces = getNumFaces obj
	-- Identificar los grupos de caras que usa cada material
	local faces_per_material = #()
	local fid
	for fid = 1 to num_faces do (
		local matID = getFaceMatID obj fid
		if faces_per_material[matID] == undefined then (
			faces_per_material[matID] = #()
		)
		append faces_per_material[matID] fid
	)
	return faces_per_material
)

fn getPositionOfMeshVtx all_vtxs idx = (
	-- Must be coherent with line 'local v = #( lpos.x,lpos.y, lpos.z, lnormal.x, lnormal.y, lnormal.z, uvA.x, 1-uvA.y )
	local v = all_vtxs[ idx ]
	return [ v[ 1 ], v[ 2 ], v[ 3 ] ]
)

fn getNormalOfMeshVtx all_vtxs idx = (
	-- Must be coherent with line 'local v = #( lpos.x,lpos.y, lpos.z, lnormal.x, lnormal.y, lnormal.z, uvA.x, 1-uvA.y )
	local v = all_vtxs[ idx ]
	return [ v[ 4 ], v[ 5 ], v[ 6 ] ]
)

fn getTexCoordOfMeshVtx all_vtxs idx = (
	-- Must be coherent with line 'local v = #( lpos.x,lpos.y, lpos.z, lnormal.x, lnormal.y, lnormal.z, uvA.x, 1-uvA.y )
	local v = all_vtxs[ idx ]
	return [ v[ 7 ], v[ 8 ], 0 ]
)

-- taken from http://www.terathon.com/code/tangent.html
fn computeTangentSpace all_vtxs all_idxs = (

	local tan1 = #()
	local tan2 = #()
	local a
	
	for a = 1 to all_vtxs.count do (
		tan1[ a ] = [0,0,0]
		tan2[ a ] = [0,0,0]
	)

	local triangleCount = all_idxs.count / 3
    for a = 0 to (triangleCount-1) do (
		local i1 = all_idxs[ a*3 + 1 ] + 1 
		local i2 = all_idxs[ a*3 + 2 ] + 1
		local i3 = all_idxs[ a*3 + 3 ] + 1
		
		local v1 = getPositionOfMeshVtx all_vtxs i1
		local v2 = getPositionOfMeshVtx all_vtxs i2
		local v3 = getPositionOfMeshVtx all_vtxs i3
		
		local w1 = getTexCoordOfMeshVtx all_vtxs i1
		local w2 = getTexCoordOfMeshVtx all_vtxs i2
		local w3 = getTexCoordOfMeshVtx all_vtxs i3
		
		local x1 = v2.x - v1.x
		local x2 = v3.x - v1.x
		local y1 = v2.y - v1.y
		local y2 = v3.y - v1.y
		local z1 = v2.z - v1.z
		local z2 = v3.z - v1.z

        local s1 = w2.x - w1.x
        local s2 = w3.x - w1.x
        local t1 = w2.y - w1.y
        local t2 = w3.y - w1.y
		
		local r = 1.0 / (s1 * t2 - s2 * t1)
        local sdir = [ (t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r]
        local tdir = [(s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r ]
		
        tan1[i1] += sdir
        tan1[i2] += sdir
        tan1[i3] += sdir
        
        tan2[i1] += tdir
        tan2[i2] += tdir
        tan2[i3] += tdir		
	)
    
	for a = 1 to all_vtxs.count do (
		local n = getNormalOfMeshVtx all_vtxs a
		local t = tan1[ a ]
		
        -- Gram-Schmidt orthogonalize
		local dot_n_t = dot n t
		local tangent = t - n * dot_n_t
		tangent = normalize tangent
		
        -- Calculate handedness
		local cross_n_t = cross n t
		local dot_c_t = dot cross_n_t tan2[a]
		
        local tangent_w = 1.0
		if dot_c_t < 0 then tangent_w = -1.0
			
		-- Append the 4 floats of the tangent to the information of each vertex
		join all_vtxs[ a ] #(tangent.x, tangent.y, tangent.z, tangent_w )
	)
)


fn exportMesh orig_obj = (
	local obj = orig_obj
	
	-- Comprobar que es una editableMesh?
	if classof orig_obj != Editable_mesh then (
		-- Si no lo es, hacer una copia y convertirlo a editable mesh.
		obj = copy orig_obj
		convertToMesh obj
		obj.name = orig_obj.name
	)
		
	-- En este punto sabemos que tenemos un editable mesh en obj
	local channelA = 1
	local channelB = 3
	
	if meshop.getMapSupport  obj  channelB == false then channelB = -1
	channelB = -1
	
	local all_svtxs = #()		-- Vtxs en formato string
	local all_vtxs = #()
	local all_idxs = #()
	
	local world_to_local = inverse obj.transform
	local local_to_mcv = rotateXMatrix -90
	local world_to_mcv = world_to_local * local_to_mcv
	
	-- Un bucle por el # de caras.
	local vtxs_of_face = #(1,3,2)
	local num_faces = getNumFaces obj
	local fid
	
	--Identify teh 
	
	local render_group_id = 0
	local renderGroup = getUserProp orig_obj "render_group"
	if renderGroup == undefined then(
		render_group_id = 0
	)
	else if renderGroup == "parallax" then(
		render_group_id = 1
	)
	
	-- Identificar los grupos de caras que usa cada material
	local faces_per_material = #()
	for fid = 1 to num_faces do (
		local matID = getFaceMatID obj fid
		if faces_per_material[matID] == undefined then (
			faces_per_material[matID] = #()
		)
		append faces_per_material[matID] fid
	)
	
	for fid = 1 to num_faces do (
		-- Pedir la face para las posiciones.  = [4,1,7]
		local face = getFace obj fid
	
		-- Pedir la face para el mapChannel A
		local fmapA = meshop.getMapFace obj channelA fid
		local fmapB
		if ( channelB > 0 ) then meshop.getMapFace obj channelB fid
	
		-- The 3 normals (one per vertex) for this face
		local normals = meshop.getFaceRNormals obj fid
		
		-- Para los 3 vertices de la cara 1 3 2
		for i in vtxs_of_face do (
			
			-- La posicion x,y,z
			local wpos = getVert obj face[i]
			local lpos = wpos * world_to_mcv
	   
			-- El 1er juego de coordenadas de textura
			local uvA = meshop.getMapVert obj channelA fmapA[i]
			local uvB 
			if ( channelB > 0 ) then uvB = meshop.getMapVert obj channelB fmapB[i]
	   
			-- ... otras cosas a exportar por vertice ...
			local lnormal = normals[ i ] * world_to_mcv.rotationpart
			
			-- Construir un vertice completo
			local v = #( lpos.x,lpos.y, lpos.z, lnormal.x, lnormal.y, lnormal.z, uvA.x, 1-uvA.y )
			
			-- Si hay soporte para el canal 3, a�ade dichas coords de textura
			if ( channelB > 0 ) then join v #(uvB.x, 1-uvB.y )
			
			-- Buscar si el vertice ya existe, o registrarlo
			local idx_v = getIndexOfVertex all_svtxs all_vtxs v
			append all_idxs idx_v
		)
	)

	format "Exporting mesh % with % vtxs and % idxs and render_group % \n" obj.name all_vtxs.count all_idxs.count render_group_id

	if all_vtxs.count > 65535 then throw ("Too many unique vertexs in the mesh " + obj.name )
	
	computeTangentSpace all_vtxs all_idxs
	-- 
	saveMeshToFile obj all_vtxs all_idxs channelB render_group_id

	-- Si hemos hecho una copia, borrarla..
	if obj != orig_obj then (
		delete obj
	)
)

--exportMesh $




