clearListener()
gc()

filein "mesh_exporter.ms"

fn export_transform_component obj fs = (
	
	local max2mcv = rotateXMatrix -90 
	local mcv2max = rotateXMatrix 90 
	
	-- Exportar los components transform y renderable
	local trans_mcv_space = mcv2max * obj.transform * max2mcv 
	local q = trans_mcv_space.rotationpart
	local t = trans_mcv_space.translation
	local s = trans_mcv_space.scalepart
	format "  <transform q=\"% % % %\" t=\"% % %\" s=\"% % %\"/>\n" \
   q.x q.y q.z -q.w t.x t.y t.z s.x s.y s.z to:fs
)

fn copy_texture_to_mcv fs slot texture_map = (
	if texture_map == undefined then return false
	if texture_map.bitmap == undefined then return false
	local filename = texture_map.bitmap.filename
	if filename == undefined then return false
	local local_name = filenameFromPath filename
	local mcv_filename = "c:\\code\\mcv\\mcv\\data\\textures\\" + local_name
	if getFileSize mcv_filename == 0 then (
		copyfile filename mcv_filename
	)
	format " %=\"%\"" slot local_name to:fs
)


fn export_std_material mat fs = (
	format "    <material_def name=\"%\"" mat.name to:fs
	format mat.name
	--Exporting the diffuse, normal and specular maps
	copy_texture_to_mcv fs "diffuse" mat.diffusemap
	copy_texture_to_mcv fs "normal" mat.bumpMap.normal_map
	copy_texture_to_mcv fs "specular" mat.specularMap
	
	--Exporting the displacement maps
	if mat.displacementMap != undefined then(
		copy_texture_to_mcv fs "displacement " mat.displacementMap
	)
	
	--copy_texture_to_mcv fs "lightmap" mat.selfillummap 
	format "/>\n" to:fs
)

-- ---------------------------------------
fn export_material obj mat fs = (
	
	if classof mat == StandardMaterial then (
		format "    <material name=\"%\" />\n" mat.name to:fs
		
	) else if classof mat == Multimaterial then (
		
		-- Preguntar al objecto qu� materiales del multimaterial esta usando
		-- y en que posiciones
		-- Exportar del multimaterial esas posiciones como std material
		-- Por ejemplo, el multimaterial mat, puede tener 20 submateriales
		-- pero este obj, solo usar los slots 18 y 4 por ejemplo, por tanto
		-- solo hay que exportar en este momento los slots 18 y 4 pero como
		-- std material
		local mat_idxs = #(1, 2 )
		for idx in mat_idxs do (
			export_material obj mat[ idx ] fs
		)
	
	) else (
		throw ("Invalid class of material " + ((classof mat) as string))
	)
	
)

fn export_renderable_component obj fs = (
	format "  <renderable mesh=\"%\">\n" obj.name to:fs
	export_material obj obj.mat fs
	format "  </renderable>\n" to:fs
	exportMesh obj
)

-- Si el mat es std, lo exporta si es nuevo, si es complejo, se autollama para cada sencillo 
-- que lo compone
fn export_materials_if_new mat prev_mats fs = (
	
	if classof mat == StandardMaterial then (
		if appendIfUnique prev_mats mat then (
			export_std_material mat fs
		) 
	) else if classof mat == Multimaterial then (
		for submat in mat do (
			export_materials_if_new submat prev_mats fs
		)
	)
)

-- Exportar la lista de todos los materiales std referenciados por los objetos de la escena
fn export_materials_used fs = (
	format "  <material_defs>\n" to:fs
	local exported_materials = #()
	for obj in $* do (
		-- es un susceptible de ser exportado? -> es una editamble / can bve converted to mesh
		if canConvertTo obj Editable_Mesh then (
			export_materials_if_new obj.material exported_materials fs
		)
	)
	format "  </material_defs>\n" to:fs
)


fn export_mcv_scene = (
	
	local outfilename = "c:\\code\\mcv\\mcv\\data\\scene.xml"
	local fs = createFile outfilename
	--fs = listener
	if fs == undefined then throw ("Can't create " + outfilename + " file\n") 
	
	-- Para todo lo que este definido en la scena actual
	progressStart ("Scene exporting...") 
	format "<scene>\n" to:fs
	
	--Export the materials
	export_materials_used fs
	local allObjects = $*
	local num_mesh_processed = 0
	
	for obj in $* do (
		-- Process bar and option to exit 
		local should_continue = progressUpdate ((num_mesh_processed / (allObjects.count as float)) * 100)
		if should_continue == false then (
			exit 
		)
		num_mesh_processed = num_mesh_processed + 1
		
		--Fix the pivot point of the object
		obj.pivot = obj.center
		
		-- es un susceptible de ser exportado? -> es una editamble / can bve converted to mesh
		if canConvertTo obj Editable_Mesh then (
		
			format "<entity name=\"%\">\n" obj.name  to:fs
			export_transform_component obj fs
			export_renderable_component obj fs
			
			format "</entity>\n" to:fs
		)
		
	)
	format "</scene>\n" to:fs
	close fs
	progressEnd() 
)

export_mcv_scene()