//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "data/shaders/utils.fx"

static const float4 cascadeColorsMultiplier[4] =
{
    float4 (1.5f, 0.0f, 0.0f, 1.0f),
    float4 (0.0f, 1.5f, 0.0f, 1.0f),
    float4 (0.0f, 0.0f, 5.5f, 1.0f),
    float4 (1.5f, 0.0f, 5.5f, 1.0f)
};

#define CASCADE_COUNT_FLAG 3

//--------------------------------------------------------------------------------------
// Calculate the index of the cascade shadow
//--------------------------------------------------------------------------------------
int getCascadeIndex(in float Z)
{
    float3 cascadeFrustumsEyeSpaceDepthsFloat = float3(camera_zfar * 0.4 / (camera_zfar - camera_znear), 
                                                       camera_zfar * 0.6 / (camera_zfar - camera_znear), 
                                                       camera_zfar);
    
    float nCascade = 0;
    if (Z < cascadeFrustumsEyeSpaceDepthsFloat.x){
            nCascade = 0;
    }
    else if (Z <= cascadeFrustumsEyeSpaceDepthsFloat.y){
        nCascade = 1;
    }
    else
    {
        nCascade = 2;
    }
    return nCascade;
}


float computeShadowFactor(float3 coords, int nCascade)
{
    if (nCascade == 0){
        return zMapCascade0.SampleCmpLevelZero(zMapSamplerPCF, coords.xy, coords.z);
    }
    else if (nCascade == 1){
        return zMapCascade1.SampleCmpLevelZero(zMapSamplerPCF, coords.xy, coords.z);
    }
    else{
        return zMapCascade2.SampleCmpLevelZero(zMapSamplerPCF, coords.xy, coords.z);
    }
}


float3 computeSoftShadowFactor(float3 worldPos, int nCascade) {
    float4 lightSpacePos = mul(float4(worldPos.xyz, 1), cascade_view_projs[nCascade]);

        float2 shadowTexCoords;
    shadowTexCoords.x = 0.5f + (lightSpacePos.x / lightSpacePos.w * 0.5f);
    shadowTexCoords.y = 0.5f - (lightSpacePos.y / lightSpacePos.w * 0.5f);

    float  resolution = 1. / 512.;
    float  shadow_factor = 0.;

    //Avarage
    for (int y = -1; y <= 1; y++) {
        for (int x = -1; x <= 1; x++) {
            shadow_factor += computeShadowFactor(float3(shadowTexCoords, lightSpacePos.z / lightSpacePos.w) + float3(resolution * x, resolution * y, 0), nCascade);
        }
    }

    //Convince that not draw a shadow behind the camera
    if (lightSpacePos.z / lightSpacePos.w < 0){
        shadow_factor = 0.;
    }

    return shadow_factor / 18.0 * 0.75 + 0.25;
}



//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
void VS(    in  float3 iPos : POSITION,
            out float4 oPos : SV_POSITION,
            out float4 oWorldPos : TEXCOORD1) {

    float4 world_pos = mul(float4(iPos, 1.f), World);
    oPos = mul(world_pos, ViewProjection);
    oWorldPos = oPos;
}

