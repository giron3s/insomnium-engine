
#include "data/shaders/utils.fx"




//--------------------------------------------------------------------------------------
// Reflection
//--------------------------------------------------------------------------------------
void VS_Reflection(    in float4 iPos           : POSITION,
                       in float3 iNormal        : NORMAL,
                       in float2 iTex           : TEXCOORD0,
                       in float4 iTangent       : TANGENT,


                       out float4 oPos          : SV_POSITION,
                       out float2 oTex          : TEXCOORD0,
                       out float4 oReflectionPos : TEXCOORD1,
                       out float3 oViewPos      : TEXCOORD2,
                       out float3 oViewNormal   : TEXCOORD3,
                       out float4 oViewTangent  : TEXCOORD4 ){


    float4 world_pos = mul(iPos, World);
    oPos = mul(world_pos, ViewProjection);
    oTex = iTex;
    oReflectionPos = mul(iPos, World);
    oReflectionPos = mul(oReflectionPos, Reflection);
    oReflectionPos = mul(oReflectionPos, Reflection_ViewProj);


    oViewPos = mul(iPos, WorldView).xyz;
    oViewNormal = mul(iNormal, (float3x3)WorldView);
    oViewTangent.xyz = mul(iTangent.xyz, (float3x3)WorldView);
    oViewTangent.w = iTangent.w;
}


void PS_Reflection(    in float4 iPos           : SV_POSITION,
                       in float2 iTex           : TEXCOORD0,
                       in float4 iReflectionPos : TEXCOORD1,
                       in float3 iViewPos       : TEXCOORD2,
                       in float3 iViewNormal    : TEXCOORD3,
                       in float4 iViewTangent   : TEXCOORD4,
                       
                       out float4 oDiffuse      : SV_TARGET0,
                       out float4 oNormal       : SV_TARGET1,
                       out float  oSpecular     : SV_TARGET2,
                       out float  oDisplacement : SV_TARGET3,
                       out float4 oExtra        : SV_TARGET4,
                       out float  oZBuffer      : SV_TARGET5)
{

    // Sample the texture pixel at this location.
    float4 textureColor = txDiffuse.Sample(samLinear, iTex);

    // Calculate the projected reflection texture coordinates.
    float2 reflectTexCoord;
    reflectTexCoord.x = iReflectionPos.x / iReflectionPos.w / 2.0f + 0.5f;
    reflectTexCoord.y = -iReflectionPos.y / iReflectionPos.w / 2.0f + 0.5f;

    // Sample the texture pixel from the reflection texture using the projected texture coordinates.
    float4 reflectionColor = txReflection.Sample(samLinear, reflectTexCoord);

    // Do a linear interpolation between the two textures for a blend effect.
    oDiffuse  = lerp(textureColor, reflectionColor, 0.65f);
    oSpecular = txSpecular.Sample(samLinear, iTex);
    oDisplacement = txDisplacement.Sample(samLinear, iTex);
    float3 N = computeNormal(iViewTangent, iViewNormal, iTex);
    oNormal = float4(N, 1.);
    oZBuffer = -iViewPos.z / camera_zfar;
    oExtra.xyz = iViewPos;
}