#include "data/shaders/constants/shaders_defs.h"

#define MAX_SUPPORTED_CASCADES 6

cbuffer ConstantBufferCascadeShadow
{
    matrix cascade_view[MAX_SUPPORTED_CASCADES];
    matrix cascade_projs[MAX_SUPPORTED_CASCADES];
    matrix cascade_view_projs[MAX_SUPPORTED_CASCADES];
    float  cascade_splits[MAX_SUPPORTED_CASCADES];
    float  cascade_resolution;
    bool   renderDebug;
};


cbuffer ConstantBufferPointLight
{
  float4 pointlight_viewpos;
  float4 pointlight_color;
  float  pointlight_dmin;
  float  pointlight_dmax;
  float  pointlight_offset[2];
};


cbuffer ConstantBufferDirectionalLight
{
    float4  ambient_color;
    float   ambient_intensity;

    float3  dirlight_direction;
    float4  dirlight_color;
    float   dirlight_intensity;
    float   dirlight_offset[3];
    matrix  dirlight_inverse; //Remove
};


//Deprecated
cbuffer ConstantBufferLight
{
    matrix LightViewProjection;
    matrix LightViewProjectionOffset;
    float4 WorldLightPos;
};