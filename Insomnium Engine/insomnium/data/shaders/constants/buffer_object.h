#include "data/shaders/constants/shaders_defs.h"

cbuffer ConstantBufferObject USING_REGISTER( b1 )
{
  matrix World;
};

// Used by deferred
cbuffer ConstantBufferObjectViewSpace
{
  matrix WorldView;
  matrix WorldViewProj;
};


// Used by deferred
cbuffer ConstantBufferReflection{
    matrix Reflection;
    matrix Reflection_View;
    matrix Reflection_ViewProj;
};
