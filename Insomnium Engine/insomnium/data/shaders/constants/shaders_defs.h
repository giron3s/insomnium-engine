
#ifndef __cplusplus

// Compiling hlsl
#define USING_REGISTER(reg_name)	 : register( reg_name )

#else

// Compiling c++

#define matrix   XMMATRIX
#define float4x4 XMMATRIX
#define float4   XMFLOAT4
#define float3   XMFLOAT3
#define float2   XMFLOAT2
#define cbuffer  struct
#define USING_REGISTER(reg_name)		

#endif
