#include "data/shaders/constants/shaders_defs.h"

cbuffer ConstantBufferBlur
{
  float2 blur_delta_uv;
};
