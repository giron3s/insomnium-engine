#include "data/shaders/constants/shaders_defs.h"

#define MAX_SUPPORTED_BONES   128
cbuffer ConstantBufferBones USING_REGISTER( b2 )
{
  float4x4 bones[ MAX_SUPPORTED_BONES ];
};