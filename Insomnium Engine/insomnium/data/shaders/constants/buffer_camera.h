#include "data/shaders/constants/shaders_defs.h"

//TODO: Replace to the 
cbuffer ConstantBufferCamera USING_REGISTER(b0)
{
    float4 WorldCameraPos;
    matrix View;
    matrix inv_view;
    matrix Projection;
    matrix ViewProjection;
};

/*cbuffer ConstantBufferCamera USING_REGISTER( b0 )
{
	matrix ortho_view;
	matrix ortho_proj;
	matrix ortho_view_proj;
};*/


cbuffer ConstantBufferDeferredCamera
{
    matrix deferred_view;
    matrix deferred_proj;
    matrix deferred_view_proj;
    float4 deferred_cameraPos;
};


cbuffer ConstantBufferCameraParams
{
  float  camera_zfar;
  float  camera_znear;
  float  tan_half_fov;
  float  camera_aspect_ratio;
};