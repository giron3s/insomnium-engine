#include "utils.fx"

//--------------------------------------------------------------------------------------
// Point Lights
//--------------------------------------------------------------------------------------
void VS_PointLights(      in  float4 iPos : POSITION
                        , out float4 oPos : SV_POSITION) 
{

    float4 wPos = mul(iPos, World);
    oPos = mul(wPos, ViewProjection);
}

float4 PS_PointLights(in float4 iPos : SV_Position) : SV_Target
{
    int3 scr_coords = uint3(iPos.xy, 0);

    float  Z = txZBuffer.Load(scr_coords).x;
    float3 N = txNormal.Load(scr_coords).xyz;
    N = normalize(N);

    // Generate the position in viewspace from the screen coords
    float3 view_dir;
    view_dir.x = (iPos.x - 400.) / 400.;
    view_dir.y = -(iPos.y - 300.) / 300.;
    view_dir.z = -1.;
    view_dir.x *= tan_half_fov * camera_aspect_ratio;
    view_dir.y *= tan_half_fov;
    float3 pos_vs = view_dir * Z * camera_zfar;

    //L - Light direction vector in view space
    float3 L = pointlight_viewpos.xyz - pos_vs;
    //L = normalize(L);
    //E - Eye vector
  
    float3 E = normalize(-pointlight_viewpos.xyz);

    //H - Half of
    float H = normalize(L + E);

    // Compute attenuation, the point
    float  distance_to_light = length(L);
    float  attenuation = 1 - saturate((distance_to_light - pointlight_dmin) / (pointlight_dmax - pointlight_dmin));
    float  diffuse_factor = saturate(dot(N, normalize(L)));
    float4 dif_color = pointlight_color * diffuse_factor;

    float  spec_amount = saturate(dot(N, H));
    spec_amount = pow(spec_amount, 150);
    return dif_color;
}