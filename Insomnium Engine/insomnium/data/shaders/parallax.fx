
#include "data/shaders/utils.fx"


//--------------------------------------------------------------------------------------
void VS_GBuffer(      in float4 iPos          : POSITION
                    , in float3 iNormal       : NORMAL
                    , in float2 iTex          : TEXCOORD0
                    , in float4 iTangent      : TANGENT

                    , out float4 oPos         : SV_POSITION
                    , out float3 oWorldPos    : TEXCOORD0
                    , out float2 oTex         : TEXCOORD1
                    , out float3 oViewNormal  : NORMAL
                    , out float3 oViewPos     : TEXCOORD2
                    , out float4 oViewTangent : TEXCOORD3
                    , out float3 oEye         : TEXCOORD4
                    , out float3 oNormal      : TEXCOORD5){



    // Calculate world position local_pos * world
    float4 world_pos    = mul(iPos, World);
    oPos                = mul(iPos, WorldViewProj);
    oWorldPos           = world_pos.xyz;
    oViewPos            = mul(iPos, WorldView).xyz;
    oViewNormal         = mul(iNormal, (float3x3)WorldView);
    oViewTangent.xyz    = mul(iTangent.xyz, (float3x3)WorldView);
    oViewTangent.w      = iTangent.w;
    oTex                = iTex;

    float3 PT           = mul(normalize(iTangent.xyz), World);
    float3 PN           = mul(normalize(iNormal.xyz), World);
    float3 PB           = iTangent.w * (cross(PN, PT));

    float3x3 tangentToWS;
    tangentToWS[0]       = normalize(PT);
    tangentToWS[1]       = normalize(-PB);
    tangentToWS[2]       = normalize(PN);

    float3x3 worldToTangentSpace = transpose(tangentToWS);

    // Output the tangent space normal, eye, and light vectors.
    //float4 eye = mul(float4(0, 0, 0, 1), inv_view);
    float3 Nws          = mul(oViewNormal, (float3x3)inv_view);
    float4 wp           = mul(float4(oViewPos, 1), inv_view);
    float3 eye          = world_pos.xyz - WorldCameraPos.xyz;

    eye.xyz             = mul(eye.xyz, worldToTangentSpace);
    Nws                 = mul(Nws, worldToTangentSpace);

    oNormal             = Nws;
    oEye                = eye.xyz;
}


void PS_GBuffer(      in float4 iPos            : SV_POSITION
                    , in float3 iWorldPos       : TEXCOORD0
                    , in float2 iTex            : TEXCOORD1
                    , in float3 iViewNormal     : NORMAL
                    , in float3 iViewPos        : TEXCOORD2
                    , in float4 iViewTangent    : TEXCOORD3
                    , in float3 iEye            : TEXCOORD4
                    , in float3 iNormal         : TEXCOORD5

                    , out float4 oDiffuse       : SV_TARGET0
                    , out float4 oNormal        : SV_TARGET1
                    , out float  oSpecular      : SV_TARGET2
                    , out float  oDisplacement  : SV_TARGET3
                    , out float4 oExtra         : SV_TARGET4
                    , out float  oZBuffer       : SV_TARGET5){



        float	fHeightMapScale = 0.015;
        int		nMaxSamples = 5;
        int		nMinSamples = 1;

        float fParallaxLimit = -length(iEye.xy) / iEye.z;
        fParallaxLimit *= fHeightMapScale;

        float2 vOffsetDir = normalize(iEye.xy);
        float2 vMaxOffset = vOffsetDir * fParallaxLimit;

        // Calculate the geometric surface normal vector, the vector from
        // the viewer to the fragment, and the vector from the fragment
        // to the light.
        float3 N = normalize(iNormal);
        float3 E = normalize(iEye.xyz);

        // Calculate how many samples should be taken along the view ray
        // to find the surface intersection.  This is based on the angle
        // between the surface normal and the view vector.
        int nNumSamples = (int)lerp(nMaxSamples, nMinSamples, dot(E, N));

        // Specify the view ray step size.  Each sample will shift the current
        // view ray by this amount.
        float fStepSize = 1.0 / (float)nNumSamples;

        // Calculate the texture coordinate partial derivatives in screen
        // space for the tex2Dgrad texture sampling instruction.
        float2 dx = ddx(iTex);
        float2 dy = ddy(iTex);

        // Initialize the starting view ray height and the texture offsets.
        float fCurrRayHeight = 1.0;
        float2 vCurrOffset = float2(0, 0);
        float2 vLastOffset = float2(0, 0);


        float fLastSampledHeight = 1;
        float fCurrSampledHeight = 1;

        int nCurrSample = 0;

        while (nCurrSample < nNumSamples)
        {
            fCurrSampledHeight = txDisplacement.SampleGrad(samLinear, iTex + vCurrOffset, dx, dy).r;
            if (fCurrSampledHeight > fCurrRayHeight){
                float delta1 = fCurrSampledHeight - fCurrRayHeight;
                float delta2 = (fCurrRayHeight + fStepSize) - fLastSampledHeight;

                float ratio = delta1 / (delta1 + delta2);

                vCurrOffset = (ratio)* vLastOffset + (1.0 - ratio) * vCurrOffset;

                nCurrSample = nNumSamples + 1;
            }
            else
            {
                nCurrSample++;

                fCurrRayHeight -= fStepSize;

                vLastOffset = vCurrOffset;
                vCurrOffset += fStepSize * vMaxOffset;

                fLastSampledHeight = fCurrSampledHeight;
            }
        }


    // Calculate the final texture coordinate at the intersection point.
    float2 vFinalCoords = iTex + vCurrOffset;

    //Get the cascade index
    //float Z = -iViewPos.z / camera_zfar;
    //int nCascade = getCascadeIndex(Z);

    //DBG
    float4 visualizeCascadeColor = float4(0., 0., 0., 1.);
    //visualizeCascadeColor = cascadeColorsMultiplier[nCascade];

    //Calculate the shadow intensity
    //float shadow_factor = computeSoftShadowFactor(iWorldPos, nCascade);


#define GRIDLINES
#ifdef GRIDLINES
    float2 vGridCoords = frac(vFinalCoords * 100.0f);

    if ((vGridCoords.x < 0.025f) || (vGridCoords.x > 0.975f))
        oDiffuse = float4(1.0f, 1.0f, 1.0f, 1.0f);

    if ((vGridCoords.y < 0.025f) || (vGridCoords.y > 0.975f))
        oDiffuse = float4(1.0f, 1.0f, 1.0f, 1.0f);

    float4 lDiffuse = oDiffuse * txDiffuse.Sample(samLinear, vFinalCoords);
    oDiffuse        = lDiffuse;
    //oDiffuse = txDiffuse.Sample(samLinear, input.Tex) * shadow_factor * visualizeCascadeColor;
#else 
    float4 lDiffuse = oDiffuse * txDiffuse.Sample(samLinear, vFinalCoords);
    oDiffuse = lDiffuse;
    //oDiffuse = txDiffuse.Sample(samLinear, input.Tex) * shadow_factor * visualizeCascadeColor;
#endif

    oSpecular = txSpecular.Sample(samLinear, vFinalCoords);
    oDisplacement = txDisplacement.Sample(samLinear, iTex).r;
    oNormal = float4(computeNormal(iViewTangent, iViewNormal, vFinalCoords), 1);
    oExtra.xyz = iViewPos;
    
    // Clip based on the alpha value of the texture.
    if (lDiffuse.a > 0.4f){
        oZBuffer = -iViewPos.z / camera_zfar;
    }
    else{
        discard;
    }
}