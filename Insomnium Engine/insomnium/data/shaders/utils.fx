Texture2D    txDiffuse      : register(t0);  //Slot ID
Texture2D    txLightMap     : register(t1);  //
Texture2D    txNormal       : register(t2);  //
Texture2D    txSpecular     : register(t3); //
Texture2D    txDisplacement : register(t4); //
Texture2D    txLightmap     : register(t5); //


Texture2D    txExtra;
Texture2D    txZBuffer; 
Texture2D    txProjector;
Texture2D    txAccLight;

Texture2D    zMapCascade0;
Texture2D    zMapCascade1;
Texture2D    zMapCascade2;
Texture2D    zMapCascade3;
Texture2D    zMapCascade4;
Texture2D    zMapCascade5;
Texture2D    zMapCascade6;
Texture2D    zMapCascade7;

Texture2D    txReflection;
Texture2D    txBackground; //Water effect - Todo remove

//--------------------------------------------------------------------------------------
// Samplers
//--------------------------------------------------------------------------------------
SamplerState samLinear : register(s0);
SamplerComparisonState zMapSamplerPCF : register(s5);

//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "data/shaders/constants/buffer_camera.h"
#include "data/shaders/constants/buffer_object.h"
#include "data/shaders/constants/buffer_time.h"
#include "data/shaders/constants/buffer_projector.h"
#include "data/shaders/constants/buffer_blur.h"
#include "data/shaders/constants/buffer_bones.h"
#include "data/shaders/constants/buffer_light.h"

// -------------------------------------------------
// Calculate N in the TBN space
// -------------------------------------------------
float3 computeNormal(    in float4 iTangent
                       , in float3 iNormal
                       , in float2 iUV     ) {

    float3 PT = normalize(iTangent.xyz);
    float3 PN = normalize(iNormal);
    float3 PB = iTangent.w * (cross(PN, PT));

    // Normalmap
    float3 normal_tangent_space = txNormal.Sample(samLinear, iUV).xyz;      //  0..1
    normal_tangent_space = normalize(normal_tangent_space * 2 - 1);         // -1..1

    float3x3 TBN = float3x3(PT, -PB, PN);
    float3 N = mul(normal_tangent_space, TBN);
    N = normalize(N);
    return N;
}

// -------------------------------------------------
// Calculate the TBN matrix
// -------------------------------------------------
float3x3 computeTBNMatrix(   in float4 iTangent
                           , in float3 iNormal
                           , in float2 iUV    ){


    float3 PT = normalize( iTangent.xyz );
    float3 PN = normalize( iNormal );
    float3 PB = iTangent.w * (cross(PN, PT));

    // Normalmap
    float3 normal_tangent_space = txNormal.Sample( samLinear, iUV ).xyz;    //  0..1
    normal_tangent_space = normalize(normal_tangent_space * 2 - 1);         // -1..1

    float3x3 TBN = float3x3(PT, -PB, PN);
    return TBN;
}

