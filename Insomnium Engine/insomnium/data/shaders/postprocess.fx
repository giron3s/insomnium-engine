Texture2D    txDiffuse : register(t0);
Texture2D    txNormals;
SamplerState samLinear : register(s0);
SamplerState samLinearClamp : register(s1);

//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "data/shaders/constants/buffer_camera.h"
#include "data/shaders/constants/buffer_object.h"
#include "data/shaders/constants/buffer_blur.h"

//--------------------------------------------------------------------------------------
struct VS_INPUT_TEXTURED
{
  float4 Pos : POSITION;
  float2 Tex : TEXCOORD0;
};

struct PS_INPUT_TEXTURED
{
  float4 Pos : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT_TEXTURED VS_textured(VS_INPUT_TEXTURED input)
{
  PS_INPUT_TEXTURED output = (PS_INPUT_TEXTURED)0;
  output.Pos = mul(input.Pos, World);
  output.Pos = mul(output.Pos, ViewProjection);
  output.Tex = input.Tex;
  return output;
}

// ----------------------------------------------------
float4 PS_border_detector (
    in float4 iPosition : SV_Position
  , in PS_INPUT_TEXTURED input
  ) : SV_Target
{
  float2 delta_y = float2(0., 4. / 600.);
  float2 delta_x = float2(4. / 800., 0.);
  float4 up = txDiffuse.Sample(samLinearClamp, input.Tex - delta_y);
  float4 dw = txDiffuse.Sample(samLinearClamp, input.Tex + delta_y);
  float4 left = txDiffuse.Sample(samLinearClamp, input.Tex - delta_x);
  float4 right = txDiffuse.Sample(samLinearClamp, input.Tex + delta_x);
  float  delta1 = abs(dw.x - up.x)*2;
  float  delta2 = abs(left.x - right.x)*2;
  float  intensity = delta1 + delta2;

  float4 nup = txNormals.Sample(samLinearClamp, input.Tex - delta_y);
    float4 ndw = txNormals.Sample(samLinearClamp, input.Tex + delta_y);
    float deltaN = 1-saturate( dot(nup.xyz, ndw.xyz) );

  return float4(0, 0, 0., intensity + deltaN);
}

// ----------------------------------------------------
float4 PS_test(
in float4 iPosition : SV_Position
, in PS_INPUT_TEXTURED input
) : SV_Target
{
  return float4(iPosition.x / 800., iPosition.y/600,0,1);
  return float4(input.Tex.x, input.Tex.y,0,1);
}






//--------------------------------------------------------------------------------------
// BLUR
//--------------------------------------------------------------------------------------
PS_INPUT_TEXTURED VS_Blur(VS_INPUT_TEXTURED input)
{
  PS_INPUT_TEXTURED output = (PS_INPUT_TEXTURED)0;
  output.Pos = mul(input.Pos, World);
  output.Pos = mul(output.Pos, ViewProjection);
  output.Tex = input.Tex;
  return output;
}

// ------------------------------------------ -
float4 PS_Blur(
  in float4 iPosition : SV_Position
, in float2 iTex0     : TEXCOORD0
) : SV_Target
{
  float2 d1 = blur_delta_uv;
  float4 c   = txDiffuse.Sample(samLinearClamp, iTex0);
  float4 cu1 = txDiffuse.Sample(samLinearClamp, iTex0 + d1);
  float4 cu2 = txDiffuse.Sample(samLinearClamp, iTex0 + d1 * 2);
  float4 cu3 = txDiffuse.Sample(samLinearClamp, iTex0 + d1 * 3);
  float4 cu4 = txDiffuse.Sample(samLinearClamp, iTex0 + d1 * 4);
  float4 cd1 = txDiffuse.Sample(samLinearClamp, iTex0 - d1);
  float4 cd2 = txDiffuse.Sample(samLinearClamp, iTex0 - d1 * 2);
  float4 cd3 = txDiffuse.Sample(samLinearClamp, iTex0 - d1 * 3);
  float4 cd4 = txDiffuse.Sample(samLinearClamp, iTex0 - d1 * 4);

  // O bien hacer un blend entre el color mixed y el color no mixed
  // usando c.a como factor de blend

  return (c*c.a 
        + cu1*cu1.a 
        + cu2*cu2.a 
        + cu3*cu3.a 
        + cu4*cu4.a
        + cd1*cd1.a
        + cd2*cd2.a
        + cd3*cd3.a
        + cd4*cd4.a
        ) / 9.;
}