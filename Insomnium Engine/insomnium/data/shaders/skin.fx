Texture2D txDiffuse : register( t0 );
Texture2D txLightmap : register( t1 );
SamplerState samLinear : register( s0 );

//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "data/shaders/constants/buffer_camera.h"
#include "data/shaders/constants/buffer_object.h"
#include "data/shaders/constants/buffer_bones.h"

//--------------------------------------------------------------------------------------
struct PS_INPUT_TEXTURED
{
    float4 Pos : SV_POSITION;
    float2 Tex : TEXCOORD0;
	float4 dbg : TEXCOORD1;
};

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS_textured( PS_INPUT_TEXTURED input) : SV_Target
{
    return txDiffuse.Sample( samLinear, input.Tex );
}

//--------------------------------------------------------------------------------------
// Vertex Shader para cal3d usando software skinning
//--------------------------------------------------------------------------------------
struct VS_INPUT_SKIN
{
    float4 Pos : POSITION;
    float3 Nor : NORMAL;
    float2 Tex : TEXCOORD0;
    float4 Weights : BLENDWEIGHTS;
    int4   Indices : BLENDINDICES;
}; 

PS_INPUT_TEXTURED VS_skin( VS_INPUT_SKIN input )
{
    PS_INPUT_TEXTURED output = (PS_INPUT_TEXTURED)0;

	matrix skin_matrix = bones[ input.Indices.x ] * input.Weights.x
	                   + bones[ input.Indices.y ] * input.Weights.y
	                   + bones[ input.Indices.z ] * input.Weights.z
	                   + bones[ input.Indices.w ] * input.Weights.w;
	float4 skin_pos = mul( float4(input.Pos.xyz,1), skin_matrix );

    output.Pos = mul( skin_pos, ViewProjection );
    output.Tex = input.Tex;
	output.dbg = input.Indices;
    return output;
}