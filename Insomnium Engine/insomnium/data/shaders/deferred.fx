
#include "data/shaders/utils.fx"

//--------------------------------------------------------------------------------------
// Defered G-Buffer
//--------------------------------------------------------------------------------------
void VS_GBuffer(    in float4 iPos     : POSITION
                  , in float3 iNormal  : NORMAL
                  , in float2 iTex     : TEXCOORD0
                  , in float4 iTangent : TANGENT

                  , out float4 oPos : SV_POSITION
                  , out float3 oWorldPos : TEXCOORD0
                  , out float2 oTex : TEXCOORD1
                  , out float3 oViewNormal : NORMAL
                  , out float3 oViewPos : TEXCOORD2
                  , out float4 oViewTangent : TEXCOORD3){

    // La posicion en mundo = local_pos * world
    float4 world_pos = mul(iPos, World);

    // Tengo que devolver la pos en projection (local_pos * w v p )
    oPos             = mul(iPos, WorldViewProj);
    oWorldPos        = world_pos.xyz;
    oViewPos         = mul(iPos, WorldView).xyz;
    oViewNormal      = mul(iNormal, (float3x3)WorldView);
    oViewTangent.xyz = mul(iTangent.xyz, (float3x3)WorldView);
    oViewTangent.w   = iTangent.w;
    oTex             = iTex;
}


void PS_GBuffer(    in float4 iPos              : SV_POSITION
                  , in float3 iWorldPos         : TEXCOORD0
                  , in float2 iTex              : TEXCOORD1
                  , in float3 iViewNormal       : NORMAL
                  , in float3 iViewPos          : TEXCOORD2
                  , in float4 iViewTangent      : TEXCOORD3

                  , out float4 oDiffuse         : SV_TARGET0
                  , out float4 oNormal          : SV_TARGET1
                  , out float  oSpecular        : SV_TARGET2
                  , out float  oDisplacement    : SV_TARGET3
                  , out float4 oExtra           : SV_TARGET4
                  , out float  oZBuffer         : SV_TARGET5 ){


    //Get the cascade index
    //float Z  = -iViewPos.z / camera_zfar;
    //int nCascade = getCascadeIndex(Z);
    
    //DBG
    //float4 visualizeCascadeColor = float4(0., 0., 0., 1.);
    //visualizeCascadeColor = cascadeColorsMultiplier[nCascade];
    
    //Calculate the shadow intensity
   // float shadow_factor = computeSoftShadowFactor(iWorldPos, nCascade);
    
    //Save the result
    float4 lDiffuse = txDiffuse.Sample(samLinear, iTex);
    oDiffuse        = lDiffuse;
    //oDiffuse      = txDiffuse.Sample(samLinear, vFinalCoords) * shadow_factor;
    //oDiffuse      = txDiffuse.Sample(samLinear, input.Tex) * shadow_factor * visualizeCascadeColor;
    oSpecular       = txSpecular.Sample(samLinear, iTex).a;
    //oDisplacement = txNormal.Sample(samLinear, iTex).a;
    oNormal         = float4(computeNormal(iViewTangent, iViewNormal, iTex) , 1.);
    oExtra.xyz      = iViewPos;

    // Clip based on the alpha value of the texture.
    if ( lDiffuse.a > 0.6f){
        oZBuffer = -iViewPos.z / camera_zfar;
    }
    else{
        discard;
    }
}


//--------------------------------------------------------------------------------------
// Composite pass. Combine light acc and gbuffer
//--------------------------------------------------------------------------------------
void VS_Composite   (    in float4 iPos  : POSITION
                       , in float2 iTex  : TEXCOORD0
                         
                       , out float4 oPos : SV_POSITION )
{
    float4 wPos = mul(iPos, World);
    oPos        = mul(wPos, ViewProjection);
}

float4 PS_Composite( in float4 iPos: SV_POSITION ) : SV_Target
{
    int3 scr_coords  = uint3(iPos.xy, 0);
    float4 albedo    = txDiffuse.Load(scr_coords);
    float4 acc_light = txAccLight.Load(scr_coords);

    return float4(albedo.rgb * (0.2 + acc_light).rgb, 1-albedo.a);
}





