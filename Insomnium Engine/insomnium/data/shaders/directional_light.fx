
#include "data/shaders/utils.fx"

//--------------------------------------------------------------------------------------
// Directional light
//--------------------------------------------------------------------------------------

void VS_DirectionalLight(   in  float4 iPos : POSITION
                          , in  float2 iTex : TEXCOORD0

                          , out float4 oPos : SV_POSITION
                          , out float2 oTex : TEXCOORD0 ){

  oPos = mul(iPos, World);
  oPos = mul(oPos, ViewProjection);
  oTex = iTex;
}


/**
 //http://rbwhitaker.wikidot.com/specular-lighting-shader
 */
void PS_DirectionalLight(     in float4 iPos : SV_POSITION
                            , in float2 iTex : TEXCOORD0

                            , out float4 oAccLight : SV_TARGET0){

    float3 N              = txNormal.Sample(samLinear, iTex);
    float3 L              = normalize(dirlight_direction);     //Light vector
    float3 viewpos = txExtra.Sample(samLinear, iTex).rgb;
    float  Shininess = 200; //Shininess or Glossinnes
    float4 SpecularColor = float4(1, 1, 1, 1);
    float  specular_power = txSpecular.Sample(samLinear, iTex) * 255; //Specular power or Specular intensity

    //Ambient color
    float4 color = saturate( ambient_color * ambient_intensity);

    //Directional light color
    float light_intensity = dot(N, L);
    color += saturate(dirlight_color * dirlight_intensity * light_intensity);

    //Specular
    float3 R = normalize(2 * dot(L, N) * N - L); //Reflection vector - Direction that light directly from the source will be reflecting
    float3 E = normalize(-viewpos);
    float dotProduct = dot(R, E);
    float4 specular = saturate(specular_power * SpecularColor * max(pow(dotProduct, Shininess), 0));
    color += specular;

    oAccLight = saturate( color );
}