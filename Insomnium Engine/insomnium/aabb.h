#ifndef INC_AABB_H_
#define INC_AABB_H_

#include "mcv_platform.h"

class CAABB
{
private:
    XMVECTOR mCenter;
    XMVECTOR mHalfSize;

public:
    CAABB(){ }

    CAABB(const CAABB &aabb, XMMATRIX aWorld)
    {
        mCenter = aabb.mCenter;
        mHalfSize = aabb.mHalfSize;

        mCenter = XMVector3TransformCoord(aabb.mCenter, aWorld);

        float hx = XMVectorGetX( aabb.mHalfSize );
        float hy = XMVectorGetY( aabb.mHalfSize );
        float hz = XMVectorGetZ( aabb.mHalfSize );

        float wxx = aWorld(0, 0);
        float wyx = aWorld(1, 0);
        float wzx = aWorld(2, 0);

        float wxy = aWorld(0, 1);
        float wyy = aWorld(1, 1);
        float wzy = aWorld(2, 1);

        float wxz = aWorld(0, 2);
        float wyz = aWorld(1, 2);
        float wzz = aWorld(2, 2);

        float new_hx = hx * fabsf(wxx)
            + hy * fabsf(wyx)
            + hz * fabsf(wzx);
        float new_hy = hx * fabsf(wxy)
            + hy * fabsf(wyy)
            + hz * fabsf(wzy);
        float new_hz = hx * fabsf(wxz)
            + hy * fabsf(wyz)
            + hz * fabsf(wzz);
        mHalfSize = XMVectorSet(new_hx, new_hy, new_hz, 0.f);
    }

    XMVECTOR GetMinCorner () const                              { return mCenter - mHalfSize; }
    XMVECTOR GetMaxCorner () const                              { return mCenter + mHalfSize; }
    XMVECTOR GetCenter    () const                              { return mCenter;             }
    XMVECTOR GetHalfSize  () const                              { return mHalfSize;           } 
    void     SetCenter    (const XMVECTOR aCenter)              { mCenter = aCenter;          }
    void     SetHalfSize  (const XMVECTOR aHalfSize)            { mHalfSize = aHalfSize;      }

};


#endif
