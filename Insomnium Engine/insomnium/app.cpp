#include "mcv_platform.h"
#include "app.h"
#include "utils.h"
#include "debug_manager.h"

CApplication App;

CApplication::CApplication() : has_focus(false) {
    //prev_ticks.
}

void CApplication::loadConfig() {
    // read from an xml...
    xres = 800;
    yres = 600;
}

// ---------------------------------------
void CApplication::destroyDevice() {

    if (depthStencil)depthStencil->Release();
    if (depthStencilView)depthStencilView->Release();

    if (immediateContext) immediateContext->ClearState();
    if (renderTargetView) renderTargetView->Release();
    if (swapChain) swapChain->Release();
    if (immediateContext) immediateContext->Release();
    if (d3dDevice) d3dDevice->Release();
}

// ---------------------------------------
bool CApplication::createDevice() {

    HRESULT hr = S_OK;

    RECT rc;
    GetClientRect(hWnd, &rc);
    UINT width = rc.right - rc.left;
    UINT height = rc.bottom - rc.top;

    UINT createDeviceFlags = 0;

    D3D_DRIVER_TYPE driverTypes[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
    };
    UINT numDriverTypes = ARRAYSIZE(driverTypes);

    D3D_FEATURE_LEVEL featureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_0,
        //D3D_FEATURE_LEVEL_10_1,
        //D3D_FEATURE_LEVEL_10_0,
    };
    UINT numFeatureLevels = ARRAYSIZE(featureLevels);

    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory(&sd, sizeof(sd));
    sd.BufferCount = 1;
    sd.BufferDesc.Width = width;
    sd.BufferDesc.Height = height;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = hWnd;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;

    for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
    {
        driverType = driverTypes[driverTypeIndex];
        hr = D3D11CreateDeviceAndSwapChain(NULL, driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
            D3D11_SDK_VERSION, &sd, &swapChain, &d3dDevice, &featureLevel, &immediateContext);
        if (SUCCEEDED(hr))
            break;
    }
    if (FAILED(hr))
        return false;

    // Create a render target view
    ID3D11Texture2D* pBackBuffer = NULL;
    hr = swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
    if (FAILED(hr))
        return false;

    hr = d3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &renderTargetView);
    pBackBuffer->Release();
    if (FAILED(hr))
        return false;

    // ------------------
    // Create depth stencil texture
    D3D11_TEXTURE2D_DESC descDepth;
    ZeroMemory(&descDepth, sizeof(descDepth));
    descDepth.Width = width;
    descDepth.Height = height;
    descDepth.MipLevels = 1;
    descDepth.ArraySize = 1;
    descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    descDepth.SampleDesc.Count = 1;
    descDepth.SampleDesc.Quality = 0;
    descDepth.Usage = D3D11_USAGE_DEFAULT;
    descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    descDepth.CPUAccessFlags = 0;
    descDepth.MiscFlags = 0;
    hr = d3dDevice->CreateTexture2D(&descDepth, NULL, &depthStencil);
    if (FAILED(hr))
        return false;

    // Create the depth stencil view
    D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
    ZeroMemory(&descDSV, sizeof(descDSV));
    descDSV.Format = descDepth.Format;
    descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    descDSV.Texture2D.MipSlice = 0;
    hr = d3dDevice->CreateDepthStencilView(depthStencil, &descDSV, &depthStencilView);
    if (FAILED(hr))
        return false;

    immediateContext->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

    // Setup the viewport
    D3D11_VIEWPORT vp;
    vp.Width = (FLOAT)width;
    vp.Height = (FLOAT)height;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;
    immediateContext->RSSetViewports(1, &vp);

    return onDeviceCreated();
}

// ---------------------------------------
void CApplication::mainLoop() {

    // Main message loop
    MSG msg = { 0 };
    while (WM_QUIT != msg.message)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else
        {
            if (has_focus) {

#ifndef NDEBUG
                PROFILE_BEGIN(UPDATE);
#endif
                updateFrame();
#ifndef NDEBUG
                PROFILE_END();
                PROFILE_BEGIN(RENDER);
#endif
                renderFrame();
#ifndef NDEBUG
                PROFILE_END();
                PROFILE_UPDATE(); // update all profiles
                debug_manager::SetDebugProfilerInfoStr( string( "FPS: " + utils::profiler::fps::GetFPS() ) + "\n" + PROFILE_GET_TREE_STRING());
#endif
            }
        }
    }
}

// ---------------------------------------
void CApplication::updateFrame() {

    // Compute elapsed real time since the last update
    LARGE_INTEGER curr_counter;
    ::QueryPerformanceCounter(&curr_counter);
    if (prev_counter.LowPart == 0)
        prev_counter = curr_counter;

    LARGE_INTEGER delta;
    delta.QuadPart = (curr_counter.QuadPart - prev_counter.QuadPart);

    LARGE_INTEGER freq;
    ::QueryPerformanceFrequency(&freq);

    float elapsed = (float)delta.LowPart / (float)freq.LowPart;

    prev_counter = curr_counter;

    onUpdate(elapsed);
}
// ---------------------------------------
void CApplication::renderFrame() {
    onRender();
    swapChain->Present(0, 0);
}