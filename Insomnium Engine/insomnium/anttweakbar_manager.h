#ifndef _ANTTWEAKBAR_MANAGER_
#define _ANTTWEAKBAR_MANAGER_

class CAntTweakBarManager{
public:
    TwBar *mMainBar;
    TwBar *mRenderBar;
    TwBar *mProfilerBar;

    bool init();
    void update( float delta );
    void render();
    void destroy();
};

extern CAntTweakBarManager anttweakbar_manager;

#endif